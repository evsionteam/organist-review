#!/bin/bash

read -p "Database Host: " DB_HOST
read -p "Database Name: " DB_NAME
read -p "Database User: " DB_USER
read -p "Database password: " DB_PASSWORD
read -p "Project Name: " PROJECT_NAME
read -p "Project URL: " PROJECT_URL
read -p "Theme slug: " THEME_SLUG
read -p "Author Name: " THEME_SLUG
read -p "Admin Name: " ADMIN_USER
read -p "Admin Password: " ADMIN_PASSWORD
read -p "Admin Email: " ADMIN_EMAIL
read -p "Repository URL: " REPO_URL

echo ">> Downloading WordPress"
wp core download
echo ">> WordPress download completed"
# ------------------------------------------------------------------------

echo ">> Configuring WordPress"
wp core config --dbhost="$DB_HOST" --dbname="$DB_NAME" --dbuser="$DB_USER" --dbpass="$DB_PASSWORD" --extra-php <<PHP
define( 'WP_DEBUG', true );
define( 'SCRIPT_DEBUG', true );
PHP
echo ">> WordPress Configured"
# ------------------------------------------------------------------------

echo ">> Installing WordPress"
wp core install --url="$PROJECT_URL" --title="$PROJECT_NAME" --admin_user="$ADMIN_USER" --admin_password="$ADMIN_PASSWORD" --admin_email="$ADMIN_EMAIL"
echo ">> Finished Installing WordPress"
# ------------------------------------------------------------------------

echo ">> Installing Underscore Theme"
wp scaffold _s $THEME_SLUG --theme_name="$PROJECT_NAME" --author=$AUTHOR_NAME
wp theme activate $THEME_SLUG
echo ">> Finished Installing Underscore Theme"
# ------------------------------------------------------------------------

echo ">> Removing Unwanted Files"
wp plugin delete akismet
wp plugin delete hello

wp theme delete twentyfourteen
wp theme delete twentyfifteen

rm -rf wp-content/themes/layouts
rm -rf wp-content/uploads
rm wp-content/themes/$THEME_SLUG/.jscsrc
rm wp-content/themes/$THEME_SLUG/.jshintignore
echo ">> Removed Unwanted Files"
# ------------------------------------------------------------------------

#echo ">> Installing plugins"
#wp plugin install woocommerce --activate
#echo ">> Removed Unwanted Files"
# ------------------------------------------------------------------------

echo ">> Sending everything to repo"
git init
git remote add origin $REPO_URL
git add --all
git commit -m "Project Initiated" --all
git push -u origin master
echo ">> Saved in repo : $REPO_URL"
# ------------------------------------------------------------------------
npm init --yes

echo ">> Setting Up Grunt"
npm install grunt-cli -g
npm install grunt --save-dev
npm install bootstrap --save-dev
npm install font-awesome --save-dev
npm install grunt-contrib-concat --save-dev
npm install grunt-contrib-copy --save-dev
npm install grunt-contrib-cssmin --save-dev
npm install grunt-contrib-jshint --save-dev
npm install grunt-contrib-sass --save-dev
npm install grunt-contrib-watch --save-dev
npm install grunt-wp-i18n --save-dev
