<?php
class Organist_Contact extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'contact_widget', // Base ID
			__( 'Contacts', 'organist' ), // Name
			array( 'description' => __( 'Widget for Footer contact', 'organist' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<div class="contact-information">

			<div class="contact-item cw-address">
				<div class="contact-item-icon"><i class="fa fa-location-arrow"></i></div>
				<div class="contact-item-content">
					<?php echo $instance[ 'address' ]; ?>
				</div>
			</div>

			<div class="contact-item cw-phone">
				<div class="contact-item-icon"><i class="fa fa-phone"></i></div>
				<?php 
					$all_phones =  $instance[ 'phone' ]; 
					$phones = explode( ',', $all_phones );
					$phone_html = array();
					foreach( $phones as $phone){
						$phone_html[] = '<a href="tel:'.trim($phone).'">'.trim($phone).'</a>';
					}
				?>

				<div class="contact-item-content"><?php echo implode(', ', $phone_html); ?></div>
			</div>

			<div class="contact-item cw-email">
				<div class="contact-item-icon"><i class="fa fa-envelope"></i></div>
				<div class="contact-item-content">
					<a href="mailto:<?php echo $instance[ 'email' ]; ?>">
						<?php echo $instance[ 'email' ]; ?>
					</a>
				</div>
			</div>

		</div>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'organist' );
		$address = ! empty( $instance['address'] ) ? $instance['address'] : '';
		$phone = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
		$email = ! empty( $instance['email'] ) ? $instance['email'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( esc_attr( 'Title:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>">
				<?php _e( esc_attr( 'Address:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" 
				value="<?php echo esc_attr( $address ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>">
				<?php _e( esc_attr( 'Phone:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" 
				value="<?php echo esc_attr( $phone ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>">
				<?php _e( esc_attr( 'Email:' ) ); ?>
			</label> 
			<input 
				class="widefat"
				type="text"
				id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" 
				value="<?php echo esc_attr( $email ); ?>" />
		</p>
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
		$instance['phone']   = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
		$instance['email']   = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
		
		return $instance;
	}
}

class Organist_Footer_Logo extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'organist_footer_logo', // Base ID
			__( 'Footer Logo', 'organist' ), // Name
			array( 'description' => __( 'Widget for Footer logo', 'organist' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<img src="<?php echo $instance['image_uri']; ?>" />
		<p><?php echo $instance['foot_description']; ?></p>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {

		$foot_description = ! empty( $instance['foot_description'] ) ? $instance['foot_description'] : '';
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : '';
		?>
		
		<p>
			<input type="text"  
				class="widefat custom_media_url_<?php echo esc_attr( $this->get_field_id( 'image_uri' ) ); ?>" 
				name="<?php echo $this->get_field_name('image_uri'); ?>" 
				id="<?php echo $this->get_field_id('image_uri'); ?>" 
				value="<?php echo $image_uri; ?>" style="margin-top:5px;">

			<input data-target=".custom_media_url_<?php echo esc_attr( $this->get_field_id( 'image_uri' ) ); ?>" 
				type="button" 
				class="button button-primary custom_media_button" 
				id="custom_media_button" 
				name="<?php echo $this->get_field_name('image_uri'); ?>" 
				value="Upload Image" style="margin-top:5px;" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'foot_description' ) ); ?>">
				<?php _e( esc_attr( 'Description:' ) ); ?>
			</label>
			<textarea
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'foot_description' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'foot_description' ) ); ?>"><?php echo esc_attr( $foot_description ); ?></textarea>
		</p>
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['foot_description']   = ( ! empty( $new_instance['foot_description'] ) ) ? $new_instance['foot_description'] : '';
		$instance['image_uri'] = ( ! empty( $new_instance['image_uri'] ) ) ? strip_tags( $new_instance['image_uri'] ) : '';
		
		return $instance;
	}
}

// register Foo_Widget widget
function organist_widgets() {
	register_widget( 'Organist_Contact' );
	register_widget( 'Organist_Footer_Logo' );
}

add_action( 'widgets_init', 'organist_widgets' );