<?php
function organist_register_post_type(){
	$labels = array(
		'name'               => _x( 'Issues', 'post type general name', 'organist' ),
		'singular_name'      => _x( 'Issue', 'post type singular name', 'organist' ),
		'menu_name'          => _x( 'Issues', 'admin menu', 'organist' ),
		'name_admin_bar'     => _x( 'Issue', 'add new on admin bar', 'organist' ),
		'add_new'            => _x( 'Add New', 'issue', 'organist' ),
		'add_new_item'       => __( 'Add New Issue', 'organist' ),
		'new_item'           => __( 'New Issue', 'organist' ),
		'edit_item'          => __( 'Edit Issue', 'organist' ),
		'view_item'          => __( 'View Issue', 'organist' ),
		'all_items'          => __( 'All Issues', 'organist' ),
		'search_items'       => __( 'Search Issues', 'organist' ),
		'parent_item_colon'  => __( 'Parent Issues:', 'organist' ),
		'not_found'          => __( 'No Issues found.', 'organist' ),
		'not_found_in_trash' => __( 'No Issues found in Trash.', 'organist' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'organist' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);
	register_post_type( 'issue', $args );
}
add_action( 'init', 'organist_register_post_type' );