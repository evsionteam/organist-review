<?php
if( !class_exists('WP_REST_Controller')){
	return;
}

set_time_limit(0);
class OR_SUBSCRIBER_ENDPOINTS extends WP_REST_Controller {
	public function register_or_routes() {
		$version = 'v1';
	    $namespace = 'or';
	    $base = '/';
	    
	    /* Webhook: invoice.payment_succeeded */
		register_rest_route( $namespace, '/invoice/succeded' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::CREATABLE,
				'callback'  => array( $this, 'proceed_success_action' )
			)
		) );
		/* Webhook: invoice.payment_failed */
		register_rest_route( $namespace, '/invoice/failed' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::CREATABLE,
				'callback'  => array( $this, 'proceed_failed_action' )
			)
		) );		

		/* Webhook: customer.subscription.deleted */
		register_rest_route( $namespace, '/invoice/cancelled' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::CREATABLE,
				'callback'  => array( $this, 'proceed_cancelled_action' )
			)
		) );

		/* Payment Reminder: Send Email to admin */
		register_rest_route( $namespace, '/invoice/reminder' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::READABLE,
				'callback'  => array( $this, 'remind_payment' )
			)
		) );

		/* Preinform Payment: Send Email to admin */
		register_rest_route( $namespace, '/invoice/preinform' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::READABLE,
				'callback'  => array( $this, 'preinform_payment' )
			)
		) );

		/* Mark user whose subscription date is expired to Passive  */
		register_rest_route( $namespace, '/invoice/check' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::READABLE,
				'callback'  => array( $this, 'check_subcription_expiry' )
			)
		) );

		/* Copy customer ID to Woocommerce compatible meta key  */
		register_rest_route( $namespace, '/migrate/customer' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::READABLE,
				'callback'  => array( $this, 'migrate_customer_key' )
			)
		) );

		/* Add renewal flag to user meta  */
		register_rest_route( $namespace, '/migrate/renewal-flag' . $base, array(
			array(
				'methods' 	=> WP_REST_Server::READABLE,
				'callback'  => array( $this, 'migrate_renewal_flag' )
			)
		) );
	}

	public function proceed_success_action($request){
		$data =  json_decode($request->get_body(),true);

		$log = "Webhooked trigged \n";

		if(isset($data['data'])){
			if(isset($data['data']['object'])){
				$customer = $data['data']['object'];
			}
		}else if(isset($data['object'])){
			$customer = $data['object'];
		}

		file_put_contents(__DIR__.'/log/log-'.$data['type'].'-'.$customer['customer'].'.txt', print_r($customer,true));

		$log .= "Grabbed customer ID: ".$customer['customer']."\n";


		/*if(!isset($data['data']['object']) && isset($data['object'])){
			$customer = $data['object'];
		}*/

		if( isset($customer['subscription']) && !empty($customer['subscription']) ){
			
			$log .= $customer['customer']." is a subscriber \n";

			$user_id = $this->get_user_id($customer['customer']);

			if($user_id){

				$log .= "Got user ID: ".$user_id." \n";

				$user = get_userdata( $user_id );
				$user->remove_role('passive_subscriber');
				$user->add_role('active_subscriber');

				$log .= "User Role is set \n";

				$userdata =  array(
			   		'user_id'	=> $user_id,
			   		'billing_email' => $user->user_email,
			   		'billing_first_name' => $user->first_name,
			   		'billing_last_name'	=> $user->last_name
			   	);

				update_user_meta( $user_id, 'subscription_id', $customer['subscription'] );
				// if(isset($customer['lines']['data'][0]['id'])){
				// 	update_user_meta( $user_id, 'subscription_id', $customer['lines']['data'][0]['id'] );
				// }

				if( isset( $customer['lines']['data'][0]['plan'] ) ){
					update_user_meta( $user_id, 'plan', $customer['lines']['data'][0]['plan'] );
				}

			   	update_user_meta( $user_id, 'current_period_start', $customer['lines']['data'][0]['period']['start'] );

				update_user_meta( $user_id, 'current_period_end', $customer['lines']['data'][0]['period']['end'] );

				$log .= "User period updated \n";

			   	//ORG_MAILCHIMP()->subscribe( $userdata,'active');
			}


			$is_recurring = get_user_meta( $user_id, 'recurring_subscription', true);
			if(!$is_recurring){
				or_log_stripe_data('subscribe-webhook', $customer, $user_id);
				or_notify_created_subscription($user_id);
				update_user_meta( $user_id, 'recurring_subscription', 1);
			}else{
				or_log_stripe_data('subscription-renew', $customer, $user_id);
				or_notify_renewed_subscription($user_id);
			}

			$log .= "User notified via email \n";
		}

		file_put_contents(__DIR__.'/log/user-'.$customer['customer'].'.txt', $log);

		return new WP_REST_Response(array());
	}

	public function proceed_failed_action($request){
		$data =  json_decode($request->get_body(),true);
		file_put_contents(__DIR__.'/log/log-'.$data['type'].'-'.$data['data']['object']['customer'].'.txt', print_r($data['data'],true));

		$customer = $data['data']['object'];
		if(!isset($data['data']['object']) && isset($data['object'])){
			$customer = $data['object'];
		}

		if( isset($customer['subscription']) && !empty($customer['subscription']) ){
			$this->failed_action($customer);
		}
		
		return new WP_REST_Response(array());
	}

	public function proceed_cancelled_action($request){
		$data =  json_decode($request->get_body(),true);
		
		$customer = $data['data']['object'];
		if(!isset($data['data']['object']) && isset($data['object'])){
			$customer = $data['object'];
		}

		$this->failed_action($customer);
		return new WP_REST_Response(array());
	}
	
	/* Cron URL: http://localhost/organist-review/wp-json/or/invoice/reminder */
	
	/* 
	 * 1. Send admin an email with list of manual subscriber whose subscription is going to expire after threshold period
	 * 2. Send admin an email with an attachment of list of user who has to be renewed with paper whose subscription is going to expire after threshold period
	*/

	public function remind_payment($request){
		$user_list = array();		

		$organist_opt = get_option('organist_opt');
		
		$threshold = 30;
		if( isset($organist_opt['email_threshold_days_manual']) && !empty($organist_opt['email_threshold_days_manual']) ){
			$threshold = $organist_opt['email_threshold_days_manual'];
		}

		$users = get_users( array( 'role__in' => array('manual_subscriber') ) );
		if($users){
			foreach( $users as $user ){
				$end_time_unix = get_user_meta( $user->ID, 'current_period_end', true );

				if( $end_time_unix && (date('d M Y',strtotime('-'.$threshold.' days', $end_time_unix) ) == date('d M Y')) ){
					$title = get_user_meta( $user->ID, 'title', true );
					$first_name = get_user_meta( $user->ID, 'first_name', true );
					$last_name = get_user_meta( $user->ID, 'last_name', true );

					$email = $user->user_email;
					$user_list[] = $title.'. '.$first_name.' '.$last_name.' ( '.$email.' )';
				}
			}
		}
		
		or_notify_admin_subscription($user_list,$threshold);

		
		$user_list = array();
		$users = get_users( array( 
			'meta_key'     => 'paper_renew',
			'meta_value'   => true,
		) );

		$filepath = __DIR__.'/paper-renewal.csv';
		$output = fopen($filepath, 'w');
		
		fputcsv(
			$output, 
			array( 'User ID','Title', 'First Name', 'Last Name', 'Address 1', 'Address 2', 'Town', 'County', 'Post Code', 'Country', 'Status', 'Package', 'Association' )
		);

		$threshold = $organist_opt['email_threshold_days_paper'];

		if($users){
			$all_plans = organists_get_plans();
			
			foreach( $users as $user ){
				$end_time_unix = get_user_meta( $user->ID, 'current_period_end', true );
				if( date('d M Y',strtotime('-'.$threshold.' days', $end_time_unix) ) == date('d M Y') ){
					$user_meta = array_map(function($v){
						return $v[0];
					},get_user_meta( $user->ID ));
					
					$email = $user->user_email;
					$user_list[] = $user_meta['billing_title'].'. '.$user_meta['billing_first_name'].' '.$user_meta['billing_last_name'].' ( '.$email.' )';
					
					if( isset($user_meta['manual_plan']) && !empty($user_meta['manual_plan']) ){
						$index = array_search($user_meta['manual_plan'], array_column($all_plans, 'id'));
						$plan = isset($all_plans[$index]['nickname'])?$all_plans[$index]['nickname']:$user_meta['manual_plan'];
						}
					
					$body = array( $user->ID,
						isset($user_meta['billing_title'])? $user_meta['billing_title']:'N/A',
						isset($user_meta['billing_first_name'])? $user_meta['billing_first_name']:'N/A',
						isset($user_meta['billing_last_name'])? $user_meta['billing_last_name']:'N/A',
						isset($user_meta['billing_address_1'])? $user_meta['billing_address_1']:'N/A',
						isset($user_meta['billing_address_2'])? $user_meta['billing_address_2']:'N/A',
						isset($user_meta['billing_city'])? $user_meta['billing_city']:'N/A',
						isset($user_meta['billing_state'])? $user_meta['billing_state']:'N/A',
						isset($user_meta['billing_postcode'])? $user_meta['billing_postcode']:'N/A',
						isset($user_meta['billing_country'])?  ( isset(WC()->countries->countries[$user_meta['billing_country']])?WC()->countries->countries[$user_meta['billing_country']]: $user_meta['billing_country'] ) :'N/A',
						isset($user_meta['current_period_end'])? __(sprintf('Renew %s', date('M Y',$user_meta['current_period_end']) ),'organist'):'N/A',
						$plan,
						isset($user_meta['local_association'])? $user_meta['local_association']:'N/A'			
					);


					fputcsv($output, $body);
				}
			}
		}

		fclose($output);
		
		or_notify_admin_paper_subscription($user_list, $filepath, $threshold);

		return new WP_REST_Response( array() );
	}


	/* Cron URL: http://localhost/organist-review/wp-json/or/invoice/preinform */
	/* 
	 * Inform active subscriber that their subscription is about to expire prior to threshold time
	*/
	
	public function preinform_payment($request){
		$users = get_users( array( 'role__in' => array('active_subscriber') ) );		
		$organist_opt = get_option('organist_opt');
		
		$threshold = 60;
		if( isset($organist_opt['email_threshold_days']) && !empty($organist_opt['email_threshold_days']) ){
			$threshold = $organist_opt['email_threshold_days'];
		}

		if($users){
			foreach( $users as $user ){
				$end_time_unix = get_user_meta( $user->ID, 'current_period_end', true );
		
				if( date('d M Y',strtotime('-'.($threshold+1).' days', $end_time_unix) ) == date('d M Y') ){

					or_remind_subscription_renew($user->ID);
				}
			}	
		}

		return new WP_REST_Response(array());		
	}

	public function check_subcription_expiry($request){
		$users = get_users( array( 'role__in' => array('active_subscriber') ) );
		
		$organist_opt = get_option('organist_opt');
		
		$threshold = 0;
		if($users){
			foreach( $users as $user ){
				$end_peroid = get_user_meta( $user->ID, 'current_period_end', true );
				
				if( $end_peroid <= time() ){
					$user->remove_role('active_subscriber');
					$user->add_role('passive_subscriber');
				}
			}	
		}
		
		return new WP_REST_Response(array());	
	}

	public function migrate_customer_key($request){
		$users_updated = array();
		$users = get_users(array('fields' => 'ids'));

		if($users){
			foreach($users as $user_id){
				$woo_stripe_id = get_user_meta( $user_id, '_stripe_customer_id', true );
				
				if(!$woo_stripe_id){
					
					$org_stripe_id = get_user_meta( $user_id, 'customer_id', true );
					
					if($org_stripe_id){
						update_user_meta( $user_id, '_stripe_customer_id', $org_stripe_id );
						$users_updated[] = array('user' => $user_id, 'customer_id' => $org_stripe_id );	
					}
				}
			}
		}

		return new WP_REST_Response($users_updated);
	}

	public function migrate_renewal_flag($request){
		$users_updated = array();
		$users = get_users(array('fields' => 'ids'));

		if($users){
			foreach($users as $user_id){
				$is_recurring = get_user_meta( $user_id, 'recurring_subscription', true );
				
				if(!$is_recurring){
					update_user_meta( $user_id, 'recurring_subscription', 1 );
					$users_updated[] = $user_id;
				}
			}
		}
		return new WP_REST_Response($users_updated);
	}

	private function failed_action($customer){
		$user_id = $this->get_user_id($customer['customer']);
		if($user_id){
			$user = get_userdata( $user_id );
			$user->remove_role('active_subscriber');
			$user->add_role('passive_subscriber');

			delete_user_meta( $user_id, 'proceeded_cancellation' );
			
			$userdata =  array(
		   		'user_id'	=> $user_id,
		   		'billing_email' => $user->user_email,
		   		'billing_first_name' => $user->first_name,
		   		'billing_last_name'	=> $user->last_name
		   	);

		   	//ORG_MAILCHIMP()->subscribe( $userdata,'passive');

			/* Send user an email */
			or_notify_failed_subscription($user_id);
		}
	}

	private function get_user_id($customer_id){
		global $wpdb;
		$user_id = $wpdb->get_var( 'SELECT `user_id` FROM '.$wpdb->usermeta.' WHERE `meta_key` = "customer_id" AND `meta_value` = "'.$customer_id.'"' );
		return $user_id;
	}
}


add_action( 'rest_api_init', 'register_or_webhook_route' );
function register_or_webhook_route(){
	$stripe_hook = new OR_SUBSCRIBER_ENDPOINTS();
	$stripe_hook->register_or_routes();
}