<?php
/* Export to CSV Module */
add_action('restrict_manage_users','organist_add_export_button');
function organist_add_export_button(){
	$user_type 	= 'all';
	$role 		= !empty($_GET['role'])?$_GET['role']:'';

	$url = esc_url( add_query_arg( 'type', 'paper-renew', 'users.php' ) );
	$text = __( 'View Paper Renewal Users','organist' );

	if( isset($_GET['type']) && $_GET['type'] == 'paper-renew' ){
		$url = 'users.php';
		$text = __( 'View All Users','organist' );
	}

	echo '<a href="'.$url.'" class="button" style="display: inline-block;margin: 0;">'.$text.'</a>';

	if( $role ){
		$user_type 	= str_replace('_', ' ', $role);
	}
	echo '<input type="hidden" name="exportit_type" value="'.$role.'" />';
	echo '<input type="submit" name="exportit" id="exportit" class="button" value="'.__( sprintf('Export %s to CSV',$user_type),'organist' ).'">';

	echo '<input type="hidden" name="printit_type" value="'.$role.'" />';
	echo '<input type="submit" name="printit" id="exportit" class="button" value="'.__( 'Export Printing List','organist' ).'">';


	echo '<button type="button" name="notify_user" id="notify_user" class="button" >'.__( 'Notify User','organist' ).' <span class="spinner" id="or-spinner"></span></button>';

	echo '<input type="submit" name="donation_user" id="donation_user" class="button" value="'.__( 'Donation Report','organist' ).'" >';
}

add_action( 'admin_init', 'organist_export_donation_report' );
function organist_export_donation_report(){
	if( count($_GET) > 0 && isset( $_GET[ 'donation_user' ] ) ){

		$users = get_users( array(
			'meta_query' => array(
				array(
					'key'     => 'gift_aid_donation',
					'value'   => 'yes',
					'compare' => '='
				)
			)
		));

		if( $users ){

			$filename = 'donation-report';
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename='.$filename.'.csv');

			$header = array( 'User ID', 'Username', 'Title', 'First Name', 'Last Name', 'Address 1', 
				'Address 2', 'City', 'State', 'Post Code', 'Country', 'Additional Donation', 'Gift Aid' );

			$output = fopen('php://output', 'w');

			fputcsv( $output, $header );

			$field_type = 'billing_';
			foreach( $users as $user ){

				$user_meta = array_map(function($v){
					return $v[0];
				},get_user_meta( $user->ID ));

				$row = array( 
					$user->ID,
					$user->display_name,
					isset($user_meta[ $field_type.'title'])? $user_meta[ $field_type.'title']:'N/A',
					isset($user_meta[ $field_type.'first_name'])? $user_meta[ $field_type.'first_name']:'N/A',
					isset($user_meta[ $field_type.'last_name'])? $user_meta[ $field_type.'last_name']:'N/A',
					isset($user_meta[ $field_type.'address_1' ])? $user_meta[ $field_type.'address_1' ]:'N/A',
					isset($user_meta[ $field_type.'address_2' ])? $user_meta[ $field_type.'address_2' ]:'N/A',
					isset($user_meta[ $field_type.'city' ])? $user_meta[ $field_type.'city' ]:'N/A',
					isset($user_meta[ $field_type.'state' ])? $user_meta[ $field_type.'state' ]:'N/A',
					isset($user_meta[ $field_type.'postcode' ])? $user_meta[ $field_type.'postcode' ]:'N/A',
					( isset( $user_meta[ $field_type.'country' ] ) && !empty($user_meta[ $field_type.'country' ]) )? WC()->countries->countries[$user_meta[ $field_type.'country' ]]:'N/A',
					isset($user_meta[ 'additional_donation'])? $user_meta[ 'additional_donation']:'N/A',
					isset($user_meta[ 'gift_aid_donation'])? $user_meta[ 'gift_aid_donation']:'No',

				);

				fputcsv( $output, $row );
			}

			exit();
		}

	}
}

add_action('admin_init','organist_export_to_csv');
function organist_export_to_csv(){
   
	global $all_plans;

	$all_plans = organists_get_plans();
	$field_type ='billing_';
	
	if( isset($_GET['printit']) ){
		$field_type = 'shipping_';
	}

	if( isset($_GET['exportit']) || isset($_GET['printit']) ){

		$current_time = time();
		
		$filename = ( ( isset($_GET['exportit_type']) && !empty($_GET['exportit_type']) )? $_GET['exportit_type'] :'all-users' ).'-'.$current_time;
		
		if( isset($_GET['printit']) ){
			$filename = ( ( isset($_GET['printit_type']) && !empty($_GET['printit_type']) )? $_GET['printit_type'] :'all-users' ).'-'.$current_time;
		}

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$filename.'.csv');

		$args = array();
		if( isset($_GET['exportit_type']) && ($_GET['exportit_type'] !="all") ){
			$args['role'] = $_GET['exportit_type'];
		}

		$header = array( 'User ID','Title', 'First Name', 'Last Name', 'Address 1', 'Address 2', 'city', 'County', 'Post Code', 'Country', 'Status', 'Association', 'Package' );
		
		$output = fopen('php://output', 'w');
		fputcsv($output, $header);

		$users = get_users($args);
		foreach($users as $user){
			$user_meta = array_map(function($v){
				return $v[0];
			},get_user_meta( $user->ID ));
			
			if( isset($user_meta['plan']) && !empty($user_meta['plan']) ){
				
				$user_plan = maybe_unserialize( $user_meta['plan'] );
				$plan = isset($user_plan['nickname'])?$user_plan['nickname']:$user_plan;

			}else if( isset($user_meta['manual_plan']) && !empty($user_meta['manual_plan']) ){
				
				$index = array_search($user_meta['manual_plan'], array_column($all_plans, 'id'));
				$plan = isset($all_plans[$index]['nickname'])?$all_plans[$index]['nickname']:$user_meta['manual_plan'];
				
			}else{
				
				$plan = 'N/A';
			}
			
			$body = array( $user->ID,
				isset($user_meta[ $field_type.'title'])? $user_meta[ $field_type.'title']:'N/A',
				isset($user_meta[ $field_type.'first_name'])? $user_meta[ $field_type.'first_name']:'N/A',
				isset($user_meta[ $field_type.'last_name'])? $user_meta[ $field_type.'last_name']:'N/A',
				isset($user_meta[ $field_type.'address_1' ])? $user_meta[ $field_type.'address_1' ]:'N/A',
				isset($user_meta[ $field_type.'address_2' ])? $user_meta[ $field_type.'address_2' ]:'N/A',
				isset($user_meta[ $field_type.'city' ])? $user_meta[ $field_type.'city' ]:'N/A',
				isset($user_meta[ $field_type.'state' ])? $user_meta[ $field_type.'state' ]:'N/A',
				isset($user_meta[ $field_type.'postcode' ])? $user_meta[ $field_type.'postcode' ]:'N/A',
				( isset( $user_meta[ $field_type.'country' ] ) && !empty($user_meta[ $field_type.'country' ]) )? WC()->countries->countries[$user_meta[ $field_type.'country' ]]:'N/A',
				isset($user_meta['current_period_end'])?__(sprintf('Renew %s', date('d-M-Y',$user_meta['current_period_end']) ),'organist'):'N/A',
				isset($user_meta['local_association'])? $user_meta['local_association']:'N/A',
				is_array( $plan ) ? $plan[ 'name' ] : $plan		
			);

			fputcsv($output, $body);
		}
		exit();	
	}
}


/* Add/Remove Column in Users table */
add_filter( 'manage_users_columns', 'or_add_users_table_column',11 );
function or_add_users_table_column( $column ) {
    
    if(isset($column['posts'])){
    	unset($column['posts']);
    }

    $column['username'] = __('Username / Email','organist');
    $column['next_issue'] = __('Next Issue','organist');
    $column['renew_date'] = __('Renew Date','organist');
    $column['subscription_plan'] = __('Plan','organist');
    $column['gift_add_donation'] = __( 'Gift Aid Donation', 'organist' );

    return $column;
}



add_filter( 'manage_users_custom_column', 'or_add_users_table_column_contents', 11, 3 );
function or_add_users_table_column_contents( $val, $column_name, $user_id ) {
    global $all_plans;

    switch ($column_name) {
        
        case 'gift_add_donation':
        	$val = get_user_meta( $user_id, 'gift_aid_donation', true );
        	if( $val && $val == 'yes' ){
        		$val = esc_html__( 'Yes','organist' );
        	}else{
        		$val = esc_html__( 'No','organist' );

        	}
            return $val;
        break;

        case 'renew_date' :
            $val = get_user_meta($user_id, 'current_period_end', true);
            
            $paper_renew = get_user_meta($user_id, 'paper_renew', true);

            if($val){
            	$val = date("d-M-Y", $val );
            }

            if($paper_renew){
            	$val .= ' <strong>[PR]</strong>';	
            }


        break;

        case 'next_issue' :
            $val = get_next_issue($user_id);
        break;

        case 'subscription_plan' :
            $plan = get_user_meta($user_id, 'plan', true);
            
            $manual_plan = get_user_meta($user_id, 'manual_plan', true);
            
			if($plan){
            	$val = isset( $plan['name'] ) ? $plan['name']: $plan['nickname'];
			}else if($manual_plan){

				$index = array_search($manual_plan, array_column($all_plans, 'id'));
				$val = $all_plans[$index]['nickname'];

			}else{
				$val = 'N/A';
			}

        break;
    
    }

    return $val;
}

/* User Meta Informations */
add_action( 'show_user_profile', 'or_subscriber_extra_fields' );
add_action( 'edit_user_profile', 'or_subscriber_extra_fields' );
 
function or_subscriber_extra_fields( $user ) {
	global $organist_opt;
	$users_title 		= $organist_opt['users_title'];
	$ternary_role		= get_user_meta( $user->ID, 'ternary_role', true );
	$user_plan			= get_user_meta( $user->ID, 'plan', true );
	$manual_plan		= get_user_meta( $user->ID, 'manual_plan', true );
	$title				= get_user_meta( $user->ID, 'title', true );
	$subscription_id	= get_user_meta( $user->ID, 'subscription_id', true );
	$customer_id	= get_user_meta( $user->ID, 'customer_id', true );
	$regular_subscriber = ( in_array('passive_subscriber', $user->roles) || in_array('active_subscriber', $user->roles) );
	$subscription_plans = organists_get_plans();
	
	$gift_aid_donation = get_user_meta( $user->ID, 'gift_aid_donation', true );
	if( !$gift_aid_donation ){
		$gift_aid_donation = 'no';
	}
	
?>
    <h3><?php _e( 'Subscription Information', 'organist' ); ?></h3>
    <table class="form-table">
       	<tr>
            <th><label for="git-aid-donation"><?php _e('Gift Aid Donation','organists') ?></label></th>
            <td>
            	<label>
            		<input type="checkbox" name="gift_aid_donation" value="yes" <?php echo $gift_aid_donation == 'yes' ? esc_html__( 'checked','organist' ) : ''; ?>/>
            		<?php echo esc_html__( 'Yes', 'organist' ); ?>
            	</label>
            </td>
        </tr>

        <tr>
            <th><label for="secondary_role"><?php _e('Secondary Role','organists') ?></label></th>
            <td>
            	<?php $all_roles = $user->roles; ?>
                <select name="secondary_role" id="secondary_role">
                	<option value=""><?php _e('--- No role specified ---','organist'); ?></option>
                	<option value="active_subscriber" <?php echo in_array( "active_subscriber", $all_roles ) ? 'selected' : ''; ?>>
                		<?php _e( 'Active Subscriber', 'organists' ); ?></option>
                	<option value="passive_subscriber" <?php echo in_array( "passive_subscriber", $all_roles ) ? 'selected' : ''; ?>>
                		<?php _e( 'Passive Subscriber', 'organists' ); ?>
               		</option>
                </select>
            </td>
        </tr>
        <?php if($customer_id): ?>
	        <tr>
	            <th>
	            	<label for="customer_id"><?php _e( 'Customer ID', 'organists' ) ?></label>
	            </th>
	            <td>
	                <input type="text" readonly disabled id="customer_id" value="<?php echo $customer_id; ?>" class="regular-text" />
	            </td>
	        </tr>
        <?php endif; ?>

        <tr>
            <th><label for="ternary_role"><?php _e('Enable Manual Subscription','organists') ?></label></th>
            <td>
                <input type="checkbox" name="ternary_role" id="ternary_role" <?php checked($ternary_role,'on'); ?> data-dependent="#manual_plan,#current_period_start,#current_period_end,#subscription_id" />
            </td>
        </tr>

        <tr>
            <th><label for="subscription_id"><?php _e('Subscription ID','organists') ?></label></th>
            <td>
                <input type="text" name="subscription_id" class="regular-text" id="subscription_id" <?php echo (!$ternary_role)? '':'disabled' ?> value="<?php echo $subscription_id; ?>" /><button class="page-title-action" id="sync-stripe" data-loading="<?php _e('Please wait','organist'); ?>" style="padding: 4px 10px;vertical-align: middle;top: -2px;"><?php _e('Sync with Stripe','organist'); ?></button>
            </td>
        </tr>
		
		<tr>
            <th><label for="manual_plan" ><?php _e('Select Subscription Type','organists') ?></label></th>
            <td>
                
                <?php if($subscription_plans){ ?>

                <select name="manual_plan" id="manual_plan" <?php echo (!$ternary_role)? 'disabled':'' ?> >
                	<option value=""><?php _e('---  Select plan  ---','organist'); ?></option>
                	<?php 
                		foreach($subscription_plans as $plan):
                			$price = wc_price($plan['amount']/100);
                    		$text = $plan['nickname'].'( '.$price.' )';
                    		$id = $plan['id']; 
                    		
                    		$selected_plan = '';
                    		if( $ternary_role ){
                    		    $selected_plan = $manual_plan;
                    		}elseif( $user_plan ){
                    		    $selected_plan = $user_plan[ 'id' ];
                    		}
                    		
                    		echo '<option value="'.$id.'" '.selected( $selected_plan, $id  ).'>'.$text.'</option>';
                    	endforeach;
                    ?>
                </select>
                <?php 
            		}else{
                		_e(sprintf('%1sNo plan entered in Stripe%2s','<p>','</p>'),'organist');
                	} 
                ?>
            </td>
        </tr>

       	<?php 
       		$current_period_start 	= get_user_meta( $user->ID, 'current_period_start', true );
       		if($current_period_start){
       			$current_period_start = date('Y-m-d', $current_period_start);
       		}

			$current_period_end   	= get_user_meta( $user->ID, 'current_period_end', true );
			if($current_period_end){
       			$current_period_end = date('Y-m-d', $current_period_end);
       		}
       		
       		$local_association    	= get_user_meta( $user->ID, 'local_association', true );

       		#if( $regular_subscriber ):
				
				if(!$ternary_role):
					$additional_donation = get_user_meta( $user->ID, 'additional_donation', true );
					$society_membership  = get_user_meta( $user->ID, 'society_membership', true );
					$issue_number        = get_user_meta( $user->ID, 'issue_number', true );
					$special_comment     = get_user_meta( $user->ID, 'special_comment', true );
					$subscription_id     = get_user_meta( $user->ID, 'subscription_id', true );
			?>
					<?php if($user_plan): ?>		
						<tr>
				            <th>
				            	<label for="subscription_package"><?php _e( 'Package Subscribed', 'organists' ) ?></label>
				            </th>
				            <td>
				                <input type="text" name="subscription_package" readonly disabled id="subscription_package" value="<?php echo $user_plan['name']; ?>" class="regular-text" />
				            </td>
				        </tr>
			    	<?php endif; ?>

			        <tr>
			            <th>
			            	<label for="additional_donation"><?php _e( 'Additional Donation', 'organists' ) ?></label>
			            </th>
			            <td>
			                <input type="text" name="additional_donation" readonly disabled id="additional_donation" value="<?php echo $additional_donation; ?>" class="regular-text" />
			            </td>
			        </tr>

			        <tr>
			            <th>
			            	<label for="society_membership"><?php _e( 'Society Membership', 'organists' ) ?></label>
			            </th>
			            <td>
			                <input type="text" name="society_membership" readonly disabled id="society_membership" value="<?php echo $society_membership; ?>" class="regular-text" />
			            </td>
			        </tr>

			        <tr>
			            <th>
			            	<label for="issue_number"><?php _e( 'Issue Number', 'organists' ) ?></label>
			            </th>
			            <td>
			                <input type="text" name="issue_number" readonly disabled id="issue_number" value="<?php echo $issue_number; ?>" class="regular-text" />
			            </td>
			        </tr>

			        <tr>
			            <th>
			            	<label for="special_comment"><?php _e( 'Special Comment', 'organists' ) ?></label>
			            </th>
			            <td>
			                <textarea rows = '10' name="special_comment" id="special_comment" class="regular-text"><?php echo $special_comment; ?></textarea>
			            </td>
			        </tr>

			        <tr>
			            <th>
			            	<label for="subscription_id"><?php _e( 'Subscription', 'organists' ) ?></label>
			            </th>
			            <td>
			                <input type="text" readonly disabled temp-id="subscription_id" value="<?php echo $subscription_id; ?>" class="regular-text" />
			            </td>
			        </tr>

		<?php 	endif; ?>

				<tr>
		            <th>
		            	<label for="local_association"><?php _e( 'Local Association', 'organists' ) ?></label>
		            </th>
		            <td>
		                <input type="text" name="local_association" id="local_association" value="<?php echo $local_association; ?>" class="regular-text" />
		            </td>
		        </tr>
				
		        <tr>
		            <th>
		            	<label for="current_period_start"><?php _e( 'Subscription Start', 'organists' ) ?></label>
		            </th>
		            <td>
		                <input type="date" name="current_period_start" <?php echo (!$ternary_role)? 'disabled':'' ?> id="current_period_start" value="<?php echo $current_period_start; ?>" class="regular-text" />
		            </td>
		        </tr>

		        <tr>
		            <th>
		            	<label for="current_period_end"><?php _e( 'Subscription End', 'organists' ) ?></label>
		            </th>
		            <td>
		                <input type="date" name="current_period_end" <?php echo (!$ternary_role)? 'disabled':'' ?> id="current_period_end" value="<?php echo $current_period_end; ?>" class="regular-text" />
		            </td>
		        </tr>
		<?php #endif; ?>
    </table>
    <div class="hidden" style="padding: 10px; background-color: #ececec;" >
	    <h3><?php _e( 'For Debugging only', 'organist' ); ?></h3>
	    <?php
	    	$de_billing_state = get_user_meta( $user->ID, 'billing_state', true );
	    	$de_shipping_state = get_user_meta( $user->ID, 'shipping_state', true );
	    ?>
	    <table class="form-table">
	       	<tr>
	            <th><label for="debug_billing_county"><?php _e('Billing County','organists') ?></label></th>
	            <td>
	            	<input type="text" id="debug_billing_county" readonly value="<?php echo $de_billing_state ?>" class="regular-text" />
	            </td>
	        </tr>
	        
	        <tr>
	            <th><label for="debug_shipping_county"><?php _e('Shipping County','organists') ?></label></th>
	            <td>
	            	<input type="text" id="debug_shipping_county" readonly value="<?php echo $de_shipping_state ?>" class="regular-text" />
	            </td>
	        </tr>
	        
	    </table>
    </div>

<?php }

add_action( 'personal_options_update', 'save_subscribers_extra_fields' );
add_action( 'edit_user_profile_update', 'save_subscribers_extra_fields' );
 
function save_subscribers_extra_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) ){
        return false;
    }
 
    update_user_meta( $user_id, 'local_association', $_POST['local_association'] );
    update_user_meta( $user_id, 'special_comment', $_POST['special_comment'] );
    update_user_meta( $user_id, 'title', $_POST['title'] );

    update_user_meta( $user_id, 'gift_aid_donation', $_POST[ 'gift_aid_donation'] );
}



add_action( 'profile_update', 'or_profile_update',  10, 2 ); 
function or_profile_update($user_id = 0, $old_user_data = array()){
	$user = get_userdata($user_id);
	$log = array();

 	if(!empty($_POST['secondary_role'])){
 		$log[] = 'Added secondary role';
 		$user->add_role($_POST['secondary_role']);
 	}

 	if(!empty($_POST['ternary_role'])){
 		$user->add_role('manual_subscriber');
 		$log[] = 'Added ternary role';

 		if( !empty($_POST['manual_plan']) ){
 			$plan_details = organists_get_plans($_POST['manual_plan']);
 			
			$manual_plan = get_user_meta( $user_id, 'manual_plan', true );
			
			if( $manual_plan != $_POST['manual_plan'] ){
				$plan_detail = organists_get_plans($_POST['manual_plan']);
				$period = get_manual_subscription_period($plan_detail['interval']);

				update_user_meta( $user->ID, 'plan', $plan_detail );
				update_user_meta( $user->ID, 'current_period_start', strtotime($period['current_period_start']) );
				update_user_meta( $user->ID, 'current_period_end', strtotime($period['current_period_end']) );

				$log[] = 'Updated time and plan';
			}
			
			update_user_meta( $user_id, 'manual_plan', $_POST['manual_plan'] );
 		}

 		if(isset($_POST['current_period_start']) && !empty($_POST['current_period_start'])){
 			update_user_meta( $user->ID, 'current_period_start', strtotime($_POST['current_period_start']) );			
 		}

 		if(isset($_POST['current_period_end']) && !empty($_POST['current_period_end'])){
			update_user_meta( $user->ID, 'current_period_end', strtotime($_POST['current_period_end']) );
		}

 	}else{
 		$log[] = 'Not a manual subscriber';
 		$user->remove_role('manual_subscriber');
 		//delete_user_meta( $user->ID, 'current_period_start' );
		//delete_user_meta( $user->ID, 'current_period_end' );
		delete_user_meta( $user_id, 'manual_plan' );

		$log[] = 'Deleted time and plan';
 	}

 	if(isset($_POST['ternary_role'])){
 		update_user_meta( $user_id, 'ternary_role', $_POST['ternary_role'] );
 	}

 	or_log_stripe_data('update-profile', $log, $user_id);
}



add_filter( 'users_list_table_query_args', 'org_intercept_admin_user_listing' );
function org_intercept_admin_user_listing($args){
    if( isset($_GET['type']) && $_GET['type'] == 'paper-renew' ){
    	$args = array(
            'meta_key' => 'paper_renew',
            'meta_value' => true
        );
    }
   
    return $args;
}


add_filter('woocommerce_customer_meta_fields', 'or_add_wc_custom_field');
function or_add_wc_custom_field($fields){
	global $organist_opt;
	$users_title = $organist_opt['users_title'];
	$titles = array();

	foreach($users_title as $user_title):
    	$titles[$user_title] = $user_title;
    endforeach;

    $new_fields['billing']['fields'] = array( 'billing_title' => array(
		'label'       => __( 'Billing Title', 'woocommerce' ),
		'description' => '',
		'type'        => 'select',
		'class'       => 'select',
		'options'     => array( '' => __( 'Select a title', 'woocommerce' ) ) + $titles
	)) + $fields['billing']['fields'];

    $new_fields['shipping']['fields'] = array( 'shipping_title' => array(
		'label'       => __( 'Delivery Title', 'woocommerce' ),
		'description' => '',
		'type'        => 'select',
		'class'       => 'select',
		'options'     => array( '' => __( 'Select a title', 'woocommerce' ) ) + $titles
	)) + $fields['shipping']['fields'];

	$new_fields['billing']['title'] = $fields['billing']['title'];
	$new_fields['shipping']['title'] = $fields['shipping']['title'];

	return $new_fields;
}