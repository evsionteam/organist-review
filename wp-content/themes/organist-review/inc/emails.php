<?php

add_action( 'phpmailer_init', 'or_set_smtp_details' );
function or_set_smtp_details( $phpmailer ) {
    $organist_opt = get_option('organist_opt');

    $phpmailer->Mailer = 'smtp';
    $phpmailer->Host = $organist_opt['smtp_host'];
    $phpmailer->Port = $organist_opt['smtp_port'];
    $phpmailer->SMTPAuth = true;
    $phpmailer->Username = $organist_opt['smtp_username'];
    $phpmailer->Password = $organist_opt['smtp_password'];

    $phpmailer->SMTPSecure = 'none';
  /*  $phpmailer->Debugoutput = 'mg_smtp_debug_output';
    $phpmailer->SMTPDebug = 2;*/

    $phpmailer->From = $organist_opt['smtp_from_email'];
    $phpmailer->FromName = $organist_opt['smtp_from_name'];
}

/* Email sent when subscription is created */
function or_notify_created_subscription($user_id){
	$organist_opt = get_option('organist_opt');
	$user = get_userdata( $user_id );
	
	$subject = $organist_opt['payment_created_subject'];
	$title = $organist_opt['payment_created_title'];
	$message = $organist_opt['payment_created_body'];	
	$message = prepare_email($message,$user);
	//die($user->user_email);
	or_send_mail($user->user_email, $subject, array('title'=>$title, 'body' => $message)  );
}

/* Email sent when subscription is created */
function or_notify_renewed_subscription($user_id){
	$organist_opt = get_option('organist_opt');
	$user = get_userdata( $user_id );
	
	$subject = $organist_opt['payment_renewed_subject'];
	$title = $organist_opt['payment_renewed_title'];
	$message = $organist_opt['payment_renewed_body'];

	$message = prepare_email($message,$user);

	or_send_mail($user->user_email, $subject, array('title'=>$title, 'body' => $message)  );
}

/* Email sent prior to subscription */
function or_remind_subscription_renew($user_id){
	$organist_opt = get_option('organist_opt');
	$user = get_userdata( $user_id );
	
	$subject = $organist_opt['payment_renew_subject'];
	$title = $organist_opt['payment_renew_title'];
	$message = $organist_opt['payment_renew_body'];

	$message = prepare_email($message,$user);

	or_send_mail($user->user_email, $subject, array('title'=>$title, 'body' => $message)  );
}

/* Email sent when subscription is cancelled or failed */
function or_notify_failed_subscription($user_id){
	$organist_opt = get_option('organist_opt');
	$user = get_userdata( $user_id );
	
	$subject = $organist_opt['payment_failed_subject'];
	$title = $organist_opt['payment_failed_title'];
	$message = $organist_opt['payment_failed_body'];

	$message = prepare_email($message,$user);

	or_send_mail($user->user_email, $subject, array('title'=>$title, 'body' => $message)  );
}

/* Email sent when admin manual try to notify user from admin end */
function or_notify_passive_subscriber($user){
	$organist_opt = get_option('organist_opt');
	
	if(!is_object($user)){
		$user = get_userdata( $user );
	}
	
	$subject = $organist_opt['passive_user_subject'];
	$title = $organist_opt['passive_user_title'];
	$message = $organist_opt['passive_user_body'];

	$message = prepare_email($message,$user);

	or_send_mail($user->user_email, $subject, array('title'=>$title, 'body' => $message)  );
}

/* Email sent prior to expiration of subscription */
function or_notify_admin_subscription($users, $threshold=30){
	if(count($users) < 1 ){
		return;
	}

	$organist_opt = get_option('organist_opt');
	$to = get_bloginfo('admin_email');
	//$to = 'cloudabiral@gmail.com';
	$subject = __('Users\' subscription about to expire','organist');
	$title =  __('About to expire','organist');

	$exp_date = date('d M Y',strtotime('+'.$threshold.' days' ) );

	$message = '<h1>Dear admin,</h1><p>Following users\' subscription are about to expire on '.$exp_date.'.</p>';
	$message .= '<ul>';
		foreach ( $users as $user ){
			$message .= '<li>'.$user.'</li>';
		}
	$message .= '</ul>';

	or_send_mail($to, $subject, array('title'=>$title, 'body' => $message)  );
}


/* Email sent prior to expiration of subscription */
function or_notify_admin_paper_subscription($users, $csv, $threshold=60){
	if(count($users) < 1 ){
		return;
	}

	$organist_opt = get_option('organist_opt');
	$to = get_bloginfo('admin_email');
	//$to = 'cloudabiral@gmail.com';

	$subject = __('Users\' subscription about to expire','organist');
	$title =  __('About to expire','organist');
	
	$exp_date = date('d M Y',strtotime('+'.$threshold.' days' ) );

	$message = '<h1>Dear admin,</h1><p>Following users\' subscription are about to expire on '.$exp_date.'.</p>';
	$message .= '<ul>';
		foreach ( $users as $user ){
			$message .= '<li>'.$user.'</li>';
		}
	$message .= '</ul>';

	or_send_mail($to, $subject, array('title'=>$title, 'body' => $message), $csv  );
	if($users){
		unlink($csv);
	}
}

function or_send_mail($to, $subject, $message,$attachments=""){
	
	if( strpos($to,'@or.post') ){
		return false;
	}

	$mailer = WC()->mailer();
	$email  = new WC_Email();

	$content =  $email->style_inline($mailer->wrap_message($message['title'],$message['body']));
	$mailer->send( $to, $subject, $content, "Content-Type: text/html\r\n", $attachments );
}

function prepare_email($content, $user){
	$organist_opt = get_option('organist_opt');
	$template = $content;

	$userdata = get_user_meta( $user->ID );
	$userdata = array_map(function($val){
		return $val[0];
	}, $userdata);

	$charge_amount 	= isset($userdata['charge_amount'])?(intval($userdata['charge_amount'])/100):0;
	//$donation 		= isset($userdata['additional_donation'])?intval($userdata['additional_donation']):0;
	//$membership 	= isset($userdata['society_membership'])?intval($userdata['society_membership']):0;

	//$additional_charge = wc_price(($charge_amount+$donation+$membership)/100);
	//$additional_charge = wc_price($charge_amount+$membership);
	$additional_charge = wc_price($charge_amount);

	$plan = maybe_unserialize($userdata['plan']);
	$plan_amount = wc_price($plan['amount']/100);

	file_put_contents(__DIR__.'/log/users-'.$user->ID.'.txt', print_r($userdata, true ));
	//file_put_contents(__DIR__.'/date.txt', print_r($userdata['current_period_end'], true ));
	
	$params = array(
		array('key' => '{user}', 'value' => $user->display_name ),
		array('key' => '{user.title}', 'value' => $userdata['billing_title'] ),
		array('key' => '{user.firstname}', 'value' => $userdata['billing_first_name'] ),
		array('key' => '{user.surname}', 'value' => $userdata['billing_last_name'] ),
		array('key' => '{user.email}', 'value' => $user->user_email ),
		array('key' => '{user.login}', 'value' => $user->user_login ),
		array('key' => '{plan.name}', 'value' => $plan['nickname'] ),
		array('key' => '{plan.start}', 'value' => date("d M, Y", $userdata['current_period_start']) ),
		array('key' => '{plan.end}', 'value' => date("d M, Y", $userdata['current_period_end']) ),
		array('key' => '{plan.charge}', 'value' => $additional_charge ),
		array('key' => '{plan.amount}', 'value' => $plan_amount ),
		array('key' => '{site.name}', 'value' => get_bloginfo('name') )
	);

	foreach( $params as $param ):
		$template = str_replace($param['key'], $param['value'], $template );
	endforeach;

	return wpautop($template);
}

/*or_notify_renewed_subscription(41);
die('dead');*/