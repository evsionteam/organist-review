<?php

class ORGANIST_MAILCHIMP{
	
	private $base_url;

	private $api_key;	

	private $lists;

	private $secret_key;

	protected static $instance = null;

	public function __construct($api_key, $list_id){
		if(!$api_key){
			return new WP_Error( 'invalid_api_key', __('Invalid API key supplied','organist') );
		}

		$key_parts = explode('-', $api_key);

		if(count($key_parts) != 2 ){
			return new WP_Error( 'invalid_api_key', __('Invalid API key supplied','organist') );
		}

		$this->api_key = $api_key;
		$this->base_url = 'https://'.$key_parts[1].'.api.mailchimp.com/3.0/';
		$this->secret_key = 'iamtesting';

		$this->list_id = $list_id;

		/*$this->lists = array(
			'active'	=> 'dfcc792200',
			'passive'	=> 'dfcc792200'
		);*/
	}

	public function subscribe( $user_data ){
		if( !isset($user_data['user_email']) ){
			return new WP_Error( 'user_email_missing', __('User email is missing','organist') );
		}

		$final_data = array(
			"email_address" => $user_data['user_email'],
			"status" =>  "subscribed",
			"merge_fields" =>  array(
		        "FNAME" =>  $user_data["FNAME"],
		        "LNAME" =>  $user_data["LNAME"],
		        "TIMESTAMP" =>  time()
	 		)
		);
		
		$endpoint = 'lists/'.$this->list_id.'/members';
		$response = $this->__make_request($endpoint,json_encode($final_data),'POST');
		
		return $response;
	}

	public function get_campaign($campaign_id=false){
		$endpoint = 'campaigns/';

		if($campaign_id){
			$endpoint .= $campaign_id;
			file_put_contents(__DIR__.'/campaign_'.time().'.txt',print_r($endpoint,true));
		}
		$response = $this->__make_request($endpoint);
		
		if( $campaign_id && !is_wp_error( $response ) ){
			return $response;
			file_put_contents(__DIR__.'/campaign_response_'.time().'.txt',print_r($response,true));
		}

		if( is_wp_error( $response ) ){
			return $response;
		}

		if( $response['total_items'] < 1 ){
			return new WP_Error( 'campaigns_not_found', __('Campaigns not found','organist') );
		}

		return $response['campaigns'];
	}

	public function get_list($list_id=false){
		$endpoint = 'lists/';

		if($list_id){
			$endpoint .= $list_id;
		}

		$response = $this->__make_request($endpoint);
		
		if( is_wp_error($response) ){
			return $response;
		}else if( $response['total_items'] < 1 ){
			return new WP_Error( 'list_not_found', __('Lists not found','organist') );
		}

		return $response['lists'];
	}

	public function send_campaign($campaign_id){
		if(!$campaign_id){
			return new WP_Error( 'no_campaign_id', __('No campaign ID provided','organist') );	
		}

		$endpoint = 'campaigns/'.$campaign_id.'/actions/send/';
		$response = $this->__make_request($endpoint,'','POST');

		return $response;
	}

	public function delete( $user_id ){
		$subscriber_hash = get_user_meta( $user_id, 'subscriber_hash', true );
		$list_id = get_user_meta( $user_id, 'list_id', true );
		
		if(!isset($list_id,$subscriber_hash)){
			return new WP_Error( 'subscriber_and_list_not_found', __('Subscriber hash and list not found','organist') );	
		}

		$endpoint = 'lists/'.$list_id.'/members/'.$subscriber_hash;
		$response = $this->__make_request($endpoint,'','DELETE');

		delete_user_meta( $user_id, 'subscriber_hash');
		delete_user_meta( $user_id, 'list_id');

		return $response;
	}
	
	public function replicate_campaign($campaign_id){
		if(!$campaign_id){
			return new WP_Error( 'campaign_id_not_found', __('Campaign ID is missing','organist') );	
		}

		$endpoint = 'campaigns/'.$campaign_id.'/actions/replicate/';
		$response = $this->__make_request($endpoint,'','POST');

		return $response;
	}

	public function update_campaign($campaign_id, $body){
		if(!$campaign_id){
			return new WP_Error( 'campaign_id_not_found', __('Campaign ID is missing','organist') );	
		}

		$endpoint = 'campaigns/'.$campaign_id;
		$response = $this->__make_request($endpoint,json_encode($body),'PATCH');

		return $response;
	}

	public function delete_campaign($campaign_id){
		if(!$campaign_id){
			return new WP_Error( 'campaign_id_not_found', __('Campaign ID is missing','organist') );	
		}

		$endpoint = 'campaigns/'.$campaign_id;
		$response = $this->__make_request($endpoint,'','DELETE');

		return $response;
	}

	private function __make_request($endpoint, $data='', $type='GET' ){
		$args = array(
			'method'			=> $type,
			'headers' 			=> array(
				'content-type' 	=> 'application/json',
				/*'content-type' 	=> 'application/x-www-form-urlencoded',*/
			    'Authorization' => 'Basic ' . base64_encode( $this->secret_key.':'.$this->api_key )
			),
			'body'		=>  $data
		);

		$response = wp_remote_request( $this->base_url.$endpoint, $args );

		if(is_wp_error($response)){
			return $response;
		}else if( ($response['response']['code'] >= 200) && ($response['response']['code'] <= 208) ){
			$response_data = json_decode(wp_remote_retrieve_body($response),true);
			return $response_data;
		}else{
			$response_data = json_decode(wp_remote_retrieve_body($response),true);
			if( isset($response_data['title'],$response_data['detail']) ){
				return new WP_Error( str_replace(' ','_', strtolower($response_data['title'])), $response_data['detail'] );
			}

			return new WP_Error('unknown_error',__('Unknown Error of Mailchimp occured','organist'));
		}
	}

	public static function get_instance($key, $list_id) {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self($key, $list_id);
		}
		return self::$instance;
	}
}


function ORG_MAILCHIMP(){
  return ORGANIST_MAILCHIMP::get_instance( '51fa43fd753a8824e45c46a3fa38aaf2-us3', 'df287c1fa0' );
}

/*$data = array(
	'user_id' => 34,
	'billing_email' => 'harrbaha@gmail.com',
	'billing_first_name' => 'Abiral',
	'billing_last_name'	=> 'Neupane'
);

$response = ORG_MAILCHIMP()->subscribe($data,'active');
print_r($response);
die();*/