<?php

/* custom product image size */
//add_filter( 'single_product_archive_thumbnail_size', 'orgainist_product_image_size' );
function orgainist_product_image_size( $size ){
  return 'product-image-size';
}

/* Add view link after cart button in product loop*/
add_action('woocommerce_after_shop_loop_item','organist_add_block_after_cart',15);
function organist_add_block_after_cart(){
	global $product;
	$url = get_permalink($product->get_id());
	$output = "<a href='".$url."'><i class='fa fa-eye'></i></a>";
	echo $output;
}

/* Alter layout of breadcrumb */
#add_filter( 'woocommerce_breadcrumb_defaults','organist_alter_breadcrumb_default_args');
function organist_alter_breadcrumb_default_args($args){
	$args['wrap_before'] = '<div class="woocommerce-breadcrumb" ><nav ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';
	$args['wrap_after'] = '</nav></div>';
	
	return $args;
}


/* Remove default sidebar from single page, and add our own sidebar */
remove_action( 'woocommerce_sidebar','woocommerce_get_sidebar',10 );
//add_action( 'woocommerce_sidebar','organist_woocommerce_get_product_sidebar' );
function organist_woocommerce_get_product_sidebar(){
	if( ! is_shop() ){
		dynamic_sidebar('woo-sidebar');
	}
}

remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb',20 );
#add_action( 'woocommerce_breadcrumb','woocommerce_breadcrumb' );

add_filter( 'woocommerce_before_widget_product_list', 'organist_wc_before_widget_product_list' );
function organist_wc_before_widget_product_list($html){
	echo '<ul class="product_list_widget list-unstyled">';
}

add_action( 'woocommerce_before_shop_loop_item_title', 'add_open_wrapper_to_product_img',9 );
function add_open_wrapper_to_product_img(){
	echo "<div class='product-image-wrapper'>";
}

add_action( 'woocommerce_before_shop_loop_item_title', 'add_close_wrapper_to_product_img',11 );
function add_close_wrapper_to_product_img(){
	echo "</div>"; // .product-image-wrapper
}

add_action( 'woocommerce_before_main_content', 'add_container_shop_page', 1 );
function add_container_shop_page(){
	#echo "<div class='inner-page-wrapper'><div class='container'>";
	echo "<div class='inner-page-wrapper'>";
}

add_action( 'woocommerce_after_main_content', 'close_container_shop_page', 99);
function close_container_shop_page(){
	#echo "</div></div>";  // ./inner-page-wrapper
	echo "</div>";  // ./inner-page-wrapper
}

add_action( 'woocommerce_before_single_product_summary','organist_before_single_product_summary_9',9 );
function organist_before_single_product_summary_9(){
	echo '<div class="organist-product"><div class="row"><div class="col-sm-6 sp-lt">';
}

add_action( 'woocommerce_before_single_product_summary','organist_before_single_product_summary_99',99 );
function organist_before_single_product_summary_99(){
	echo '</div><div class="col-sm-6 sp-rt">';
}

add_action( 'woocommerce_after_single_product_summary', 'organist_after_single_product_summary', 1);
function organist_after_single_product_summary(){
	echo "</div>"; // ./sp-rt
	echo "</div>"; // ./row
	echo "</div>"; // ./row
}

add_action( 'woocommerce_before_shop_loop', 'organist_before_shop_loop_1', 1 );
function organist_before_shop_loop_1(){
	echo '<div class="woo-breadcrumb-filter clearfix">';
}

add_action( 'woocommerce_before_shop_loop', 'organist_before_shop_loop_99', 99 );
function organist_before_shop_loop_99(){
	echo '</div>'; // ./woo-breadcrumb-filter
}

add_filter( 'woocommerce_product_review_comment_form_args', 'change_woocommerce_review_form' );
function change_woocommerce_review_form( $comment_form ){

	$commenter = wp_get_current_commenter();

	$comment_form[ 'fields' ] = array(
		'author' => '<p class="comment-form-author"><input placeholder="Name" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
		'email'  => '<p class="comment-form-email"><label for="email"><input placeholder="Email" id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>'
	);

	return $comment_form ;
}


/* Add Cart Collaterals */

add_action('woocommerce_cart_collaterals', 'add_woocommerce_collaterals',8);
function add_woocommerce_collaterals(){
	if ( wc_coupons_enabled() ) { ?>
		<div class="col-sm-6">
			<div class="coupon">
				<h2><?php _e('Discount Codes','woocommerce'); ?></h2>
				<p><?php _e('Enter your coupon code if you have one.','woocommerce'); ?></p>
				<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			</div>
		</div>
	<?php } 
}

add_action('woocommerce_cart_collaterals', 'add_woocommerce_collaterals_9', 9);
function add_woocommerce_collaterals_9(){
	echo "<div class='col-sm-6'>";
}

add_action('woocommerce_cart_collaterals', 'add_woocommerce_collaterals_11', 11);
function add_woocommerce_collaterals_11(){
	echo "</div>";
}

// add_filter( 'wc_add_to_cart_params', 'modidify_view_cart_text' );
add_filter( 'woocommerce_get_script_data', 'modidify_view_cart_text' );
function modidify_view_cart_text( $data ){
	$data['i18n_view_cart'] = "<i class='fa fa-check'></i>";
	return $data;
}

add_filter( 'woocommerce_cart_totals_coupon_html', 'modifiy_cart_totals_coupon', 10, 2 );
function modifiy_cart_totals_coupon( $value, $coupon ){

	// $value = implode( ', ', $value ) . ' <a href="' . esc_url( add_query_arg( 'remove_coupon', urlencode( $coupon->code ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" class="woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->code ) . '">' . __( 'Remove', 'woocommerce' ) . '</a>';
	$value = str_replace( '[', '', $value );
	$value = str_replace( ']', '', $value );
	return $value;
}

function display_user_address($order, $type="billing"){

	$det = $order->get_address($type);
	
	$output  ='<div class="table-responsive">';
	$output .='	<table class="shop_table billing_details">';
	
	$output .='		<tr>';
	$output .='			<th>'.__( 'Name', 'woocommerce' ).'</th>';
	$output .='			<td>'.$det['first_name'].' '.$det['last_name'].'</td>';
	$output .='		</tr>';
	
	if( $det['address_1'] ):
		$output .='		<tr>';
		$output .='			<th>'.__( 'Address 1', 'woocommerce' ).'</th>';
		$output .='			<td>'.$det['address_1'].'</td>';
		$output .='		</tr>';
	endif;
	
	if( $det['address_2'] ):
		$output .='		<tr>';
		$output .='			<th>'.__( 'Address 2', 'woocommerce' ).'</th>';
		$output .='			<td>'.$det['address_2'].'</td>';
		$output .='		</tr>';
	endif;

	if( $det['city'] ):
		$output .='		<tr>';
		$output .='			<th>'.__( 'City', 'woocommerce' ).'</th>';
		$output .='			<td>'.$det['city'].'</td>';
		$output .='		</tr>';
	endif;
				
	if( $det['state'] ):
		$full_state = ( $det['country'] && $det['state'] && isset( WC()->countries->states[ $det['country'] ][ $det['state'] ] ) ) ? WC()->countries->states[ $det['country'] ][ $det['state'] ] : $det['state'];

		$output .='		<tr>';
		$output .='			<th>'.__( 'State', 'woocommerce' ).'</th>';
		$output .='			<td>'. $full_state.'</td>';
		$output .='		</tr>';
	endif;

	if( $det['country'] ):
		$full_country = ( isset( WC()->countries->countries[ $det['country'] ] ) ) ? WC()->countries->countries[ $det['country'] ] : $det['country'];

		$output .='		<tr>';
		$output .='			<th>'.__( 'Country', 'woocommerce' ).'</th>';
		$output .='			<td>'. $full_country.'</td>';
		$output .='		</tr>';
	endif;

		$output .='		<tr>';
		$output .='			<th>'.__( 'Post Code', 'woocommerce' ).'</th>';
		$output .='			<td>'.$det['postcode'].'</td>';
		$output .='		</tr>';

	$output .='	</table>';
	$output .='</div>';

	echo $output;
}

add_filter( 'woocommerce_placeholder_img', 'change_default_woocommerce_placeholder' );
function change_default_woocommerce_placeholder( $img ){

	return  '<img src="//placeholdit.imgix.net/~text?txtsize=33&txt=&w=214&h=300" alt="' . esc_attr__( 'Placeholder', 'woocommerce' ) . '" width="214" class="woocommerce-placeholder wp-post-image" height="300" />';
}

add_action( 'woocommerce_after_checkout_validation', 'organist_after_checkout_validation' );
function organist_after_checkout_validation( $posted ){

	if( ! is_user_logged_in() ){

		if ( ! isset( $_POST['woocommerce_checkout_update_totals'] ) && wc_notice_count( 'error' ) == 0 ) {

			$new_customer = wc_create_new_customer( $posted['billing_email'], '', '' );

			if ( is_wp_error( $new_customer ) ) {
				throw new Exception( $new_customer->get_error_message() );
			} else {
				$customer_id = absint( $new_customer );

				$creds = array();
				$creds['user_login'] = $username;
				$creds['user_password'] = $password;

				$user = wp_signon( $creds, false );

				wc_set_customer_auth_cookie( $customer_id );
				WC()->cart->calculate_totals();
			}
		}
	}
}

function organist_product_loop( $query_args, $atts, $loop_name ) {

	global $woocommerce_loop;

	$products                    = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, $loop_name ) );
	$columns                     = absint( $atts['columns'] );
	$woocommerce_loop['columns'] = $columns;
	$woocommerce_loop['name']    = $loop_name;

	ob_start();

	if ( $products->have_posts() ) {
		?>

		<?php do_action( "woocommerce_shortcode_before_{$loop_name}_loop" ); ?>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php
					global $filter_class; $filter_class = $loop_name;
					wc_get_template_part( 'content', 'product' );
				?>

			<?php endwhile; // end of the loop. ?>


		<?php do_action( "woocommerce_shortcode_after_{$loop_name}_loop" ); ?>

		<?php
	} else {
		do_action( "woocommerce_shortcode_{$loop_name}_loop_no_results" );
	}

	woocommerce_reset_loop();
	wp_reset_postdata();

	return ob_get_clean();
}

add_filter( 'woocommerce_my_account_get_addresses', 'or_alter_account_address_text' );
function or_alter_account_address_text($labels){
	if( isset($labels['shipping']) ){
		$labels['shipping'] = __( 'Delivery Address', 'woocommerce' );
	}

	return $labels;
}


add_filter( 'product_cat_class', 'add_class_in_category_loop' );
function add_class_in_category_loop($classes){
	$classes[] = 'col-sm-6';
	$classes[] = 'col-md-3';
	$classes[] = 'col-xs-12';

	return $classes;
}