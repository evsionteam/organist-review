<?php
use Cartalyst\Stripe\Stripe;

function organist_stripe(){
	//global $organist_opt;
	//$organist_opt['stripe_secret_key'];
	
	//Added static scret key because sometimes dynamic value becomes null for some reason. Don't know why
	return Stripe::make( 'sk_live_6CmZvCnEw1JASWcignLC80mK', '2019-03-14' );
}

function organist_get_exceptions( $e ){
	return array(
		'is_exception' => true,
		'code'    => $e->getCode(),
		'message' => $e->getMessage(),
		'type'    => $e->getErrorType()
	);
}

add_action( 'wp_ajax_handle_organists_subscription', 'handle_organists_subscription' );
add_action( 'wp_ajax_nopriv_handle_organists_subscription', 'handle_organists_subscription' );
function handle_organists_subscription(){
	$ignore_list = array('card_number','card_token','cvc','subscription_package');
	$data = $_POST['data'];

	if( $data[ 'billing_email' ] == $data[ 'billing_confirm_email'] ){
		$userdata = array(
		    'user_login'  	=> $data['billing_email'],
		    'user_email'	=> $data['billing_email'],
		    'role'			=> 'customer', /* Need to change to separate role to make ease in filtering in admin end */
		    'user_pass'   	=> wp_generate_password()
		);

		if( is_user_logged_in() ){
			$user = wp_get_current_user();
			$user_id = $user->ID;
		}else{
			$user_id = wp_insert_user( $userdata );
			$user = get_user_by('id', $user_id);
		}

		if ( ! is_wp_error( $user_id ) ) {
		    $user->add_role('passive_subscriber');
		    foreach($data as $key => $value ){
		    	if(in_array($key, $ignore_list)){
		    		continue;
		    	}

		    	if( $key == 'billing_first_name' ){
		    		update_user_meta( $user_id, 'first_name', $value );
		    	}

		    	if( $key == 'billing_last_name' ){
		    		update_user_meta( $user_id, 'last_name', $value );
		    	}

		    	update_user_meta( $user_id, $key, $value );

		    	/*$key = str_replace('billing_', 'shipping_', $key);
		    	update_user_meta( $user_id, $key, $value );*/
		    }

		    $userdata['source'] = $data['card_token'];
		    $userdata['coupon'] = $_POST['coupon'];

		    $response = organists_subscribe_to_stripe($user_id, $userdata, $data);
		   
		    if( !isset( $response[ 'is_exception' ] ) ){
		    	
		    	wp_signon( array(
					'user_login'	=> $userdata['user_login'],
					'user_password'	=> $userdata['user_pass']
				), false );

		    	$redirect_to = get_permalink(get_option('woocommerce_myaccount_page_id'));
		    		    	
		    	WC()->mailer()->customer_new_account( $user_id, $userdata,true );

		    	$final_response = array(
					'status' 	=> 200,
					'message' 	=> __( sprintf('Your order has been accepted. Please check your email for account information and password. Wait for @time seconds while we prepare to redirect you to your account page or %1sclick here%2s','<a href="'.$redirect_to.'">','</a>'),'organist-review' ),
					'data' 		=> $response,
					'redirect'	=> $redirect_to
				);

				subscribe_to_mailchimp( array(
					'user_email'    => $data[ 'billing_email' ],
					'FNAME' => $data[ 'billing_first_name' ],
					'LNAME'  => $data[ 'billing_last_name' ]
				));
		    }else{
		    	
		    	$final_response = array(
					'status' => $response[ 'code' ],
					'data' => array( 
						'message' => $response[ 'message' ], 
						'code'=> $response[ 'type' ] 
					)
				);	
		    }
		    
		}else{
			$error_messages = $user_id->get_error_messages();

			$final_response = array(
				'status' => 500,
				'data' => array( 'message' => implode('<br>',$error_messages), 'code'=>'user-not-created'  )
			);	
		}
	}else{
		$final_response = array(
			'status' => 500,
			'data' => array(
				'message' => __( 'Confirmation Email Mismatched.', 'organist-review' ),
				'code' => 'user-not-created'
			)
		);
	}
	
	wp_send_json($final_response);
}

function organists_subscribe_to_stripe($user_id,$userdata,$data){

	$user = get_user_by('id', $user_id);
	
	try{

		$response_data = organist_stripe()->customers()->create([
		    'email'			=> $userdata['user_email'],
		    'description' 	=> 'User #'.$user_id,
		    'source'		=> $userdata['source'],
		    'metadata'		=> array( 'user_id' => $user_id )
		]);

		$data['user_id'] = $user_id;
		//$mailchimp_response = ORG_MAILCHIMP()->subscribe($data,'active');		

		/* Customer Created */

		$customer_id = $response_data['id'];
		update_user_meta( $user_id, 'customer_id', $customer_id );
		update_user_meta( $user_id, '_stripe_customer_id', $customer_id );

		/* Add card to user meta */
		$customer_source = $response_data['sources']['data'][0];

		$card = array(
			'id' => $customer_source['id'],
			'brand' => $customer_source['brand'],
			'exp_month' => $customer_source['exp_month'],
			'exp_year' => $customer_source['exp_year'],
			'last4' => $customer_source['last4']
		);

		update_user_meta( $user_id, 'subscription_card', $card );

		/* Additional Charge */
		$extra_charge = 0;
		$charges = array();
		if( isset($data['additional_donation']) && !empty($data['additional_donation']) ){
			$charges[] = __('Additional Donation','organist');
			$extra_charge += $data['additional_donation'];
		}

		if( isset($data['society_membership']) && !empty($data['society_membership']) ){
			$charges[] = __('Society Membership','organist');
			$extra_charge += $data['society_membership'];
		}

		if($extra_charge > 0 ){
			
			try{

				$response_data = organist_stripe()->charges()->create([
					'amount' 	  => $extra_charge,
					'currency'	  => get_woocommerce_currency(),
					'customer'	  => $customer_id,
					'metadata'	  => array( 'user_id' => $user_id ),
					'description' => __( sprintf('Payment for %s ', implode(' + ', $charges)),'organist' )
				]);

				or_log_stripe_data('extra-charge', $response_data, $user_id);
				
				update_user_meta( $user_id, 'charge_id', $response_data['id'] );
				update_user_meta( $user_id, 'charge_amount', $response_data['amount'] );
				update_user_meta( $user_id, 'charge_created', $response_data['created'] );

			} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
				$response_data = organist_get_exceptions( $e );
			} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
				$response_data = organist_get_exceptions( $e );
			}

		}

		/* Subscribe to plan */
		$args = array(
			'customer'		=> $customer_id,
			'plan'			=> $data['subscription_package'],
			'metadata'		=> array( 'user_id' => $user_id )
		);

		if(!empty($userdata['coupon'])){
			$args['coupon'] = $userdata['coupon'];
		}

		$response_data = subscribe_to_stripe($user_id, $args);

		/*or_notify_created_subscription($user_id);*/
		if( !isset( $response_data[ 'is_exception' ] ) ){
			
			or_log_stripe_data('subscribe', $response_data, $user_id);

			$user->remove_role('passive_subscriber');
    		$user->add_role('active_subscriber');
		}
	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	}

	return $response_data;
}

add_action( 'wp_ajax_apply_organists_coupon', 'apply_organists_coupon' );
add_action( 'wp_ajax_nopriv_apply_organists_coupon', 'apply_organists_coupon' );
function apply_organists_coupon(){

	$response_data = array(
		'status' => 400,
		'message' => __('No coupon code provided','organist')
	);

	if( !isset($_POST['coupon_code']) || empty($_POST['coupon_code']) ){
		wp_send_json($response_data);
	}

	$coupon_code = $_POST['coupon_code'];

	try{

		$response = organist_stripe()->coupons()->find( $coupon_code );
		$response_data = array(
			'status' => 200,
			'data' => $response
		);

	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		
		$exception = organist_get_exceptions( $e );
    	$response_data = array(
			'status' => $exception[ 'code' ],
			'data' => array( 
				'message' => $exception[ 'message' ], 
				'code'=> $exception[ 'type' ] 
			)
		);
	}

	wp_send_json($response_data);
}

add_action( 'wp_ajax_or_get_state', 'or_get_state_options' );
add_action( 'wp_ajax_nopriv_or_get_state', 'or_get_state_options' );
function or_get_state_options(){
	$response = array('status' => 500, 'data' => '' );

	$billing_state = '';
	if(is_user_logged_in()){
		$billing_state = get_user_meta( get_current_user_id(), 'billing_state', true );
	}

	if( isset($_POST['country'],$_POST['type']) && !empty($_POST['country']) && !empty($_POST['type']) ){
		$field = '';
		$prefix = $_POST['type'];
		$states  = WC()->countries->get_states( $_POST['country'] );
		
		$response['status'] = 404;
		if($states){
			$response['status'] = 200;
			$field .= '<select name="'.$prefix.'_state" class="or-form-control" id="or-state" > <option value="">'.__( 'Select State', 'woocommerce' ) .'</option>';
			foreach ( $states as $ckey => $cvalue ) {
				$field .= '<option value="' . esc_attr( $ckey ) . '"'.selected( $billing_state, $ckey,false ).'>'.__( $cvalue, 'woocommerce' ) .'</option>';
			}

			$field .= '</select>';

			$response['data'] = $field;	
		}else{
			$response['data'] = '<input type="text" name="'.$prefix.'_state" class="or-form-control" value="'.$billing_state.'" />';
		}
	}
	
	wp_send_json($response);
}

add_action( 'wp_ajax_or_renew_subscription', 'renew_or_user_subscription' );
add_action( 'wp_ajax_nopriv_or_renew_subscription', 'renew_or_user_subscription' );
function renew_or_user_subscription(){

	$user_id = $_POST['user_id'];
	$plan = get_user_meta( $user_id, 'plan', true );
	$customer_id = get_user_meta( $user_id, 'customer_id', true );

	$args = array(
		'customer' => $customer_id,
		'plan'	   => $plan['id'],
		'metadata' => array( 'user_id' => $user_id )
	);

	$response = subscribe_to_stripe($user_id, $args);

	if( !isset( $response[ 'is_exception' ] ) ){
	   	$final_response = array( 'status' => 200, 'data' => $response );
	}else{
		or_log_stripe_data('subscribe',$response, $user_id);
    	$final_response = array(
			'status' => $response[ 'code' ],
			'data' => array( 
				'message' => $response[ 'message' ], 
				'code'=> $response[ 'type' ] 
			)
		);	
    }

    wp_send_json($final_response);
}

add_action( 'wp_ajax_or_cancel_subscription', 'cancel_or_user_subscription' );
add_action( 'wp_ajax_nopriv_or_cancel_subscription', 'cancel_or_user_subscription' );
function cancel_or_user_subscription(){

	$user_id = $_POST['user_id'];
	$subscription_id = get_user_meta( $user_id, 'subscription_id', true );
	$customer_id 	= get_user_meta( $user_id, 'customer_id', true );
	$current_period_end = get_user_meta( $user_id, 'current_period_end', true );

	try{
		// $response_data = organist_stripe()->subscriptions()->cancel( $customer_id, $subscription_id, true );
		$response_data = organist_stripe()->subscriptions()->update( $customer_id, $subscription_id, array(
			'cancel_at_period_end' => true
		));

	   	or_log_stripe_data('cancel-subscription',$response_data, $user_id);
	   	$final_response = array(
	   		'status' 	=> 200, 
	   		'data' => $response_data, 
	   		'message' => __( sprintf('Auto-renew has been disabled, you will continue to receive magazines up until your renewal date (%s), after which you will not receive future issues.',date("d M, Y",$current_period_end) ),'organist'));

	   	$user = get_userdata($user_id);

	   	update_user_meta( $user_id, 'proceeded_cancellation', true );
	   	
	   	//$user->remove_role('active_subscriber');
		//$user->add_role('passive_subscriber');

		$userdata =  array(
	   		'user_id'	=> $user_id,
	   		'billing_email' => $user->user_email,
	   		'billing_first_name' => $user->first_name,
	   		'billing_last_name'	=> $user->last_name
	   	);

	   	//ORG_MAILCHIMP()->subscribe( $userdata,'passive');
	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	}

	if( isset( $response_data[ 'is_exception'] ) ){

    	$final_response = array(
			'status' => $response_data[ 'code' ],
			'data' => array( 'message' =>  $response_data[ 'message' ], 'code' => $response_data[ 'type' ]  )
		);	
	   	
	}

    wp_send_json($final_response);
}

add_action( 'wp_ajax_organist_update_subcription_card', 'organist_update_subcription_card' );
add_action( 'wp_ajax_nopriv_organist_update_subcription_card', 'organist_update_subcription_card' );
function organist_update_subcription_card(){
	$token = $_POST['token'];
	$user_id = $_POST['user_id'];
	
	$response = organists_change_card($user_id, $token);
	
	wp_send_json($response);
}

add_action( 'wp_ajax_notify_passive_users', 'handle_newsletter_service' );
function handle_newsletter_service(){
	global $organist_opt;
	$count = 0;
	$users = get_users(array(
		'role__in' => array('passive_subscriber')
	));

	$users = array_filter($users, function($user){
		return !strpos($user->user_email,'@or.post');
	});

	if(count($users) > 0 ){
		foreach($users as $user ){
			or_notify_passive_subscriber($user);
			$count++;
		}
		$response = array(
			'status' => 200,
			'data' => array('code' => 'mail_sent', 'message'=> __( sprintf('%s users are notified through email',$count),'organist') ),
		);
	}else{
		$response = array(
			'status' => 404,
			'data' => array('code'=>'server_error','message'=>__('No relevant user found to send the newsletter','organist')),
		);
	}

	wp_send_json( $response );
}

add_action( 'wp_ajax_sync_with_stripe', 'organist_sync_with_stripe' );
function organist_sync_with_stripe(){
    

	$user_id 		 = $_POST['user_id'];
	$subscription_id = $_POST['subscription_id'];
	$customer_id 	 = get_user_meta( $user_id, 'customer_id', true );

	try{
        file_put_contents( __DIR__ . '/te.txt', print_r( $customer_id, true ), FILE_APPEND );
		$response_data = organist_stripe()->subscriptions()->find( $customer_id, $subscription_id );
	   	or_log_stripe_data('manual-sync',$response_data, $user_id);

	   	$user = get_userdata( $user_id );
	   	$user->remove_role('passive_subscriber');
    	$user->add_role('active_subscriber');

	   	update_user_meta( $user_id, 'customer_id', $response_data['customer'] );
	   	update_user_meta( $user_id, '_stripe_customer_id', $response_data['customer'] );
	   	update_user_meta( $user_id, 'subscription_id', $response_data['id'] );
		update_user_meta( $user_id, 'current_period_start', $response_data['current_period_start'] );
		update_user_meta( $user_id, 'current_period_end', $response_data['current_period_end'] );
		update_user_meta( $user_id, 'current_period_end', $response_data['current_period_end'] );
		update_user_meta( $user_id, 'plan', $response_data['plan'] );
		delete_user_meta( $user_id, 'manual_plan' );
		delete_user_meta( $user_id, 'ternary_role' );
		
		$final_response = array( 
			'status' 	=> 200, 
			'data' => array(
				'code'=>'synced_with_stripe',
				'message'=> __('Synced with stripe successfully','organist'), 
				'stripe' => $response_data 
			) 
		);
		

	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response_data = organist_get_exceptions( $e );
	}

	if( isset( $response_data[ 'is_exception' ] ) ){
		$final_response = array(
			'status' => $response_data[ 'code' ],
			'data' => array( 
				'message' => $response_data[ 'message' ], 
				'code'=> $response_data[ 'type' ]  
			)
		);	
	}
    file_put_contents( __DIR__ . '/styncss.txt', print_r( $final_response, true ), FILE_APPEND );
    wp_send_json($final_response);
    die;
}

add_action('init','add_organist_role');
function add_organist_role(){
	add_role(
	    'active_subscriber',
	    __( 'Active Subscriber' ),
	    array( 'read' => true )
	);

	add_role(
	    'passive_subscriber',
	    __( 'Passive Subscriber' ),
	    array( 'read' => true )
	);

	add_role(
	    'manual_subscriber',
	    __( 'Manual Subscriber' ),
	    array( 'read' => true )
	);
}

function organists_get_plans($plan_id=false){

	try{

		if( $plan_id ){
			$response = organist_stripe()->plans()->find( $plan_id );
		}else{
			$response = organist_stripe()->plans()->all();
		}

	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response = organist_get_exceptions( $e );
	}

	if( isset( $response[ 'is_exception' ] ) ){
		die( $response[ 'message' ] );
	}

	if($plan_id){
		return $response;
	}

	return $response['data'];
}

function get_next_issue($user_id=0, $validate=false){
	$issue = "";
	if(!$user_id){
		return _get_issue();
	}

	if($validate){
		$user =	get_userdata($user_id);
		if( !in_array( 'active_subscriber', $user->roles ) && !in_array( 'passive_subscriber', $user->roles )){
			$issue = "";
		}else if( in_array( 'active_subscriber', $user->roles ) ){
			$issue = _get_issue();
		}else{
			$issue = __("Subscription Expired","organist");
		}
	}else{
		$issue = _get_issue();
	}

	
	return $issue;
}

function _get_issue(){
    $current_date = strtolower(date('M Y'));
    $prev_year = date("Y",strtotime("-1 year"));
    $this_year = date("Y");
    $next_year = date("Y",strtotime("+1 year"));
    
    switch($current_date){
	
	case "nov ".$prev_year:
	case "dec ".$this_year:
	case "jan ".$this_year:
	case "nov ".$this_year:
	case "jan ".$next_year:
		$issue = date_i18n("F", strtotime("01-03-2018"));
	break;

	case "feb ".$this_year:
	case "mar ".$this_year:
	case "apr ".$this_year:
		$issue = date_i18n("F", strtotime("01-06-2018"));
	break;

	case "may ".$this_year:
	case "jun ".$this_year:
	case "jul ".$this_year:
		$issue = date_i18n("F", strtotime("01-09-2016"));
	break;


	case "aug ".$this_year:
	case "sep ".$this_year:
	case "oct ".$this_year:
		$issue = date_i18n("F", strtotime("01-12-2016"));
	break;

	default:
	    $issue = '';
    }

	return $issue;
}

function get_stripe_info(){
	global $organist_opt;
	$args = array(
		'url'	=> 'https://api.stripe.com/v1',
		'headers' 	=> array(
			'content-type' 	=> 'application/x-www-form-urlencoded',
		    'Authorization' => 'Basic ' . base64_encode( $organist_opt['stripe_secret_key'].': ' )
		)
	);
	return $args;
}

function subscribe_to_stripe($user_id, $args){

	$user = get_userdata($user_id);
	$customer_id = $args[ 'customer' ];

	unset( $args[ 'customer' ] );

	try{

		$response = organist_stripe()->subscriptions()->create( $customer_id, $args );
		$user->remove_role('passive_subscriber');
		$user->add_role('active_subscriber');
		
		update_user_meta( $user_id, 'subscription_id', $response['id'] );
		update_user_meta( $user_id, 'current_period_start', $response['current_period_start'] );
		update_user_meta( $user_id, 'current_period_end', $response['current_period_end'] );
		update_user_meta( $user_id, 'plan', $response['plan'] );

	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response = organist_get_exceptions( $e );
	}

	return $response;		
}

function organists_change_card($user_id, $token ){

	$card 			= get_user_meta( $user_id, 'subscription_card', true );
	$customer_id 	= get_user_meta( $user_id, 'customer_id', true );

	try{

		$response = organist_stripe()->cards()->delete( $customer_id, $card['id'] );

	   	// $_POST['stripe_token']
	   	$response_data = organist_stripe()->cards()->create( $customer_id, $token );

		$card = array(
			'id'        => $response_data['id'],
			'brand'     => $response_data['brand'],
			'exp_month' => $response_data['exp_month'],
			'exp_year'  => $response_data['exp_year'],
			'last4'     => $response_data['last4']
		);

		update_user_meta( $user_id, 'subscription_card', $card );

	   	$final_response = array( 
	   		'status'  => 200, 
	   		'data'    => $response, 
	   		'message' => __( sprintf('Card changed successfull to %1sXXXX-XXXX-XXXX-XXXX-%2s%3s','<em>',
	   			$response_data['last4'],'</em>'),
	   			'organist-review') 
	   	);

	} catch( \Cartalyst\Stripe\Exception\StripeException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\BadRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\UnauthorizedException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\InvalidRequestException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\NotFoundException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\CardErrorException $e ){
		$response = organist_get_exceptions( $e );
	} catch( \Cartalyst\Stripe\Exception\ServerErrorException $e ){
		$response = organist_get_exceptions( $e );
	}

	if( isset( $response[ 'is_exception' ] ) ){

		$final_response = array(
			'status' => $response[ 'code' ],
			'data' => array( 
				'message' => $response[ 'message' ], 
				'code'=> $response[ 'type' ]  
			)
		);			
	}

    return $final_response;
}

function get_manual_subscription_period($plan){

	$periods = array(
		'current_period_start' => null,
		'current_period_end'	=> null
	);
	$interval = '';

	switch($plan){
		case 'daily':
			$interval = ' +1 day';
		break;

		case 'month':
			$interval = '+1 month';
		break;

		case 'year':
		default:
			$interval = '+1 year';			
	}

	$periods['current_period_start'] = time();
	$periods['current_period_end'] = strtotime($interval);

	return $periods;
}

add_filter( 'woocommerce_saved_payment_methods_list', 'wc_get_organists_saved_payment_methods', 10, 2 );
function wc_get_organists_saved_payment_methods( $item, $user_id ) {
	if(!$user_id){
		$user_id = get_current_user_id();
	}

	$user = get_userdata($user_id);
	//$item = array();

	$card = get_user_meta( $user_id, 'subscription_card', true );

	if($card){

		$payment_mode['methods']['method']['last4'] = $card['last4'];
		$payment_mode['methods']['method']['brand'] = $card['brand'];
		$payment_mode['methods']['is_default'] 		=  true;
		$payment_mode['methods']['expires']         = sprintf("%02d", $card['exp_month']).'/'.substr($card['exp_year'],-2);

		$payment_mode['methods']['actions']         = array(
			'edit' => array(
				'url' => wc_get_endpoint_url( 'add-payment-method',$card['id'] ), 
				'name' => __('Update','organist') 
			)
		);

		$item[] = $payment_mode;	
	}

	return $item;
}
 
function or_get_months(){
	$months = array(
		'01' => __('Jan','organist'),
		'02' => __('Feb','organist'),
		'03' => __('Mar','organist'),
		'04' => __('Apr','organist'),
		'05' => __('May','organist'),
		'06' => __('Jun','organist'),
		'07' => __('July','organist'),
		'08' => __('Aug','organist'),
		'09' => __('Sep','organist'),
		'10' => __('Oct','organist'),
		'11' => __('Nov','organist'),
		'12' => __('Dec','organist')
	);

	return $months;
}

function or_get_years(){
	$years = array();
	$year = date('Y');
	for( $i=0; $i<20; $i++ ){
		$years[$year+$i] = $year+$i;
	}

	return $years;
}

add_action( 'woocommerce_save_account_details', 'organists_save_account_details' );
function organists_save_account_details($user_id){

	$account_title = isset( $_POST[ 'account_title' ] ) ? $_POST[ 'account_title' ] : '';
	$account_phone = isset( $_POST[ 'account_phone' ] ) ? $_POST[ 'account_phone' ] : '';
	$account_address_1 = isset( $_POST[ 'account_address_1' ] ) ? $_POST[ 'account_address_1' ] : '';
	$account_address_2 = isset( $_POST[ 'account_address_2' ] ) ? $_POST[ 'account_address_2' ] : '';
	$billing_country = isset( $_POST[ 'billing_country' ] ) ? $_POST[ 'billing_country' ] : '';
	$billing_state = isset( $_POST[ 'billing_state' ] ) ? $_POST[ 'billing_state' ] : '';
	$account_city = isset( $_POST[ 'account_city' ] ) ? $_POST[ 'account_city' ] : '';
	$account_postcode = isset( $_POST[ 'account_postcode' ] ) ? $_POST[ 'account_postcode' ] : '';

	update_user_meta( $user_id, 'title', htmlentities( $account_title ) );
	update_user_meta( $user_id, 'billing_phone', htmlentities( $account_phone ) );
	update_user_meta( $user_id, 'billing_address_1', htmlentities( $account_address_1 ) );
	update_user_meta( $user_id, 'billing_address_2', htmlentities( $account_address_2 ) );
	update_user_meta( $user_id, 'billing_country', htmlentities( $billing_country ) );
	update_user_meta( $user_id, 'billing_state', htmlentities( $billing_state ) );
	update_user_meta( $user_id, 'billing_city', htmlentities( $account_city ) );
	update_user_meta( $user_id, 'billing_postcode', htmlentities( $account_postcode ) );
}

function organists_get_wc_details(){
	$wc_details['title']              = '';
	$wc_details['billing_first_name'] = '';
	$wc_details['billing_last_name']  = '';
	$wc_details['billing_email']      = '';
	$wc_details['billing_confirm_email']      = '';
	$wc_details['billing_phone']      = '';
	$wc_details['billing_address_1']  = '';
	$wc_details['billing_address_2']  = '';
	$wc_details['billing_country']    = '';
	$wc_details['billing_city']      = '';
	$wc_details['billing_postcode']   = '';

	$wc_details['shipping_first_name'] = '';
	$wc_details['shipping_last_name']  = '';
	$wc_details['shipping_email']      = '';
	$wc_details['shipping_phone']      = '';
	$wc_details['shipping_address_1']  = '';
	$wc_details['shipping_address_2']  = '';
	$wc_details['shipping_country']    = '';
	$wc_details['shipping_city']       = '';
	$wc_details['shipping_postcode']   = '';
	$wc_details['gift_aid_donation'] = 'no';


	if(is_user_logged_in()){
	    $user_id = get_current_user_id();
	    
	    $wc_details['title']              = get_user_meta( $user_id, 'title', true );
	    $wc_details['billing_first_name'] = get_user_meta( $user_id, 'billing_first_name', true );
	    $wc_details['billing_last_name']  = get_user_meta( $user_id, 'billing_last_name', true );
	    $wc_details['billing_email']      = get_user_meta( $user_id, 'billing_email', true );
	    $wc_details['confirm_billing_email']      = get_user_meta( $user_id, 'billing_email', true );
	    $wc_details['billing_phone']      = get_user_meta( $user_id, 'billing_phone', true );
	    $wc_details['billing_address_1']  = get_user_meta( $user_id, 'billing_address_1', true );
	    $wc_details['billing_address_2']  = get_user_meta( $user_id, 'billing_address_2', true );
	    $wc_details['billing_country']   = get_user_meta( $user_id, 'billing_country', true );
	    $wc_details['billing_city']    = get_user_meta( $user_id, 'billing_city', true );
	    $wc_details['billing_postcode']  = get_user_meta( $user_id, 'billing_postcode', true );

	    $wc_details['shipping_first_name'] = get_user_meta( $user_id, 'shipping_first_name', true );
	    $wc_details['shipping_last_name '] = get_user_meta( $user_id, 'shipping_last_name', true );
	    $wc_details['shipping_email'] = get_user_meta( $user_id, 'shipping_email', true );
	    $wc_details['shipping_phone'] = get_user_meta( $user_id, 'shipping_phone', true );
	    $wc_details['shipping_address_1'] = get_user_meta( $user_id, 'shipping_address_1', true );
	    $wc_details['shipping_address_2'] = get_user_meta( $user_id, 'shipping_address_2', true );
	    $wc_details['shipping_country'] = get_user_meta( $user_id, 'shipping_country', true );
	    $wc_details['shipping_city'] = get_user_meta( $user_id, 'shipping_city', true );
	    $wc_details['shipping_postcode'] = get_user_meta( $user_id, 'shipping_postcode', true );
	    $wc_details['gift_aid_donation'] = get_user_meta( $user_id, 'gift_aid_donation', true );
	}

	return $wc_details;
}

function is_organists_subscriber(){
	$output =  false;
	
	if( is_user_logged_in() ){
    	$user_id = get_current_user_id();
    	$subscription_id = get_user_meta( $user_id, 'subscription_id', true );
    	
    	if($subscription_id){
    		$output = true;
    	}
  	}
  	
  	return $output;
}

add_filter( 'insert_user_meta', 'add_subscriber_meta',10, 3 );
function add_subscriber_meta($meta, $user, $update){
	if(!$update){
		list($u, $domain) = explode('@', $user->user_email);
	
		$is_paper_renew = false;

		if( $domain == 'or.post' ){
			$is_paper_renew = true;
		}

		update_user_meta( $user->ID, 'paper_renew', $is_paper_renew );	
	}

	return $meta;
}