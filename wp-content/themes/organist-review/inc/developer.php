<?php

add_action( 'phpmailer_init', 'prevent_email_from_sending' );
function prevent_email_from_sending( $phpmailer ) {
	if(defined('WP_DEBUG') && WP_DEBUG){
		return false;
	}
}