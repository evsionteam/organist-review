<?php
/*
	[0] => ﻿CID
    [1] => Email
    [2] => Renewal Date
    [3] => Company Name
    [4] => Title
    [5] => First Name
    [6] => SURNAME
    [7] => ADDRESS1
    [8] => ADDRESS2
    [9] => Town
    [10] => COUNTY
    [11] => POSTCODE
    [12] => COUNTRY
    [13] => Status
    [14] => Association
    [15] => Package
*/

add_action('admin_head', 'import_user_for_org' );
function import_user_for_org(){
	
	if( isset($_GET['import_user']) && isset($_GET['file']) ):
		
		if( $_GET['file'] == 1 ){			
			$filename = __DIR__.'/csv/EuropeanPackage.csv'; // 57 users
		}else if( $_GET['file'] == 2 ){
			$filename = __DIR__.'/csv/UKAssociationMember.csv'; // 861 users => CF - 918 users
		}else if( $_GET['file'] == 3 ){
			$filename = __DIR__.'/csv/UkFullPriceMember.csv'; // 519 users => CF - 1437 users
		}else if( $_GET['file'] == 4 ){
			$filename = __DIR__.'/csv/WorldwidePackage.csv'; // 92 users => CF - 1529 users
		}else if( $_GET['file'] == 5 ){
			$filename = __DIR__.'/csv/ExportForEagle.csv'; //
		}
	
		$file = fopen( $filename, "r" );
		$count = 0;

		$userdata = array();

		while( !feof($file) ){
		
			$data = fgetcsv($file);
			
			if( $count == 0 ){
				$head = get_csv_head($data);
				$count++;
				continue;
			}

			if( array_key_exists ( 'cid', $head ) && isset( $data[intval($head['cid']) ] ) ){
				
				$login = $data[intval($head['cid'])].'@or.post';
				
				$userdata = array(
				    'user_login'  =>  $login,
				    'user_email'  =>  $login,
				    'user_pass'   =>  wp_generate_password( 8, false ),
				    'first_name'  =>  (isset($head['first_name']))?$data[$head['first_name']]:'',
				    'last_name'   =>  (isset($head['surname']))?$data[$head['surname']]:'',
				    'role'		  => 'manual_subscriber'
				);

				$user_id = wp_insert_user( $userdata );
				$user = get_userdata($user_id);

				if ( ! is_wp_error( $user_id ) ) {
					
					$rep_fields = array(
						'email' => $login
					);

					if( array_key_exists( 'first_name', $head ) ){
						$rep_fields['first_name'] = $data[$head['first_name']];
					}

					if( array_key_exists( 'surname', $head ) ){
						$rep_fields['last_name'] = $data[$head['surname']];
					}

					if( array_key_exists( 'address1', $head ) ){
						$rep_fields['address_1'] = $data[$head['address1']];
					}

					if( array_key_exists( 'address2', $head ) ){
						$rep_fields['address_2'] = $data[$head['address2']];
					}

					if( array_key_exists( 'country', $head ) ){
						$rep_fields['country'] = get_wc_country($data[$head['country']]);
					}

					if( array_key_exists( 'county', $head ) ){
						$rep_fields['state'] = $data[$head['county']];
					}

					if( array_key_exists( 'postcode', $head ) ){
						$rep_fields['postcode'] = $data[$head['postcode']];
					}

					if( array_key_exists( 'company_name', $head ) ){
						$rep_fields['company'] = $data[$head['company_name']];
					}

					if( array_key_exists( 'town', $head ) ){
						$rep_fields['city'] = $data[$head['town']];
					}

					if( array_key_exists( 'title', $head ) ){
						update_user_meta( $user_id, 'title', $data[$head['title']] );
					}
					
					if( array_key_exists( 'package', $head ) ){
						$plan = convert_to_db_subscription($data[$head['package']]);
						update_user_meta( $user_id, 'manual_plan', $plan );
					}

					if( array_key_exists( 'association', $head ) ){
						update_user_meta( $user_id, 'local_association', $data[$head['association']] );
					}			

					if( array_key_exists( 'renewal_date', $head ) && array_key_exists( 'start_date', $head ) ){

						$renew_date = get_appropriate_subscription_date( $data[$head['start_date']], $data[$head['renewal_date']] );

						update_user_meta( $user_id, 'current_period_start', $renew_date['current_period_start'] );
						update_user_meta( $user_id, 'current_period_end', $renew_date['current_period_end'] );
						update_user_meta( $user_id, 'ternary_role', 'on' );
						update_user_meta( $user_id, 'paper_renew', 1 );
						
						$user->add_role('active_subscriber');	
					}else{
						$user->add_role('passive_subscriber');
					}				

					foreach( $rep_fields as $meta_key => $meta_value ){
						update_user_meta( $user_id, 'billing_'.$meta_key, $meta_value );	
						update_user_meta( $user_id, 'shipping_'.$meta_key, $meta_value );	
					}
				}
			}

			$count++;
		}

		fclose($file);
	endif;

}

function get_csv_head($data){
	/*
		[0] => ﻿CID
	    [1] => Email
	    [2] => Renewal Date
	    [3] => Company Name
	    [4] => Title
	    [5] => First Name
	    [6] => SURNAME
	    [7] => ADDRESS1
	    [8] => ADDRESS2
	    [9] => Town
	    [10] => COUNTY
	    [11] => POSTCODE
	    [12] => COUNTRY
	    [13] => Status
	    [14] => Association
	    [15] => Package
	*/
	
	/*$heads = array(
		'cid' => -1,		
		'email' => -1,		
		'renewal_date' => -1,		
		'company_name' => -1,		
		'title' => -1,		
		'first_name' => -1,		
		'surname' => -1,		
		'address1' => -1,		
		'address2' => -1,		
		'town' => -1,		
		'county' => -1,		
		'postcode' => -1,		
		'country' => -1,		
		'status' => -1,		
		'association' => -1,		
		'package' => -1	
	);*/


	$heads = array();
		
	foreach($data as $key => $val){
		
		$val = str_replace(' ', '_', strtolower($val));
		$val = convert_string($val);

		$heads[$val] = $key;
	}

	return $heads;
}

function convert_string($str){
	$str = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $str);
	return trim($str);
}


function convert_to_db_subscription($subs){
	$subs = convert_string($subs);

	switch($subs){
		case 'UK Association Member':
			$subscription = 'ukassociationmember';
		break;

		case 'Non Member Uk Sub':
			$subscription = 'ukfullsub';
		break;

		case 'European Sub':
			$subscription = 'eursubs';
		break;

		case 'Worldwide Sub':
			$subscription = 'airmalglobal';
		break;

		default:
			$subscription = '';
	}

	return $subscription;
}



function get_appropriate_subscription_date($start_date, $end_date){

	$start_date = convert_string($start_date);
	$end_date = convert_string($end_date);
	
	$start_date_arr = explode('-', $start_date);
	$end_date_arr = explode('-', $end_date);
	
	$renewal_period = array(
		'current_period_start' 	=> '',
		'current_period_end'	=> ''
	);

	$renewal_period['current_period_start'] = strtotime( $start_date_arr[0].'-'. month_to_digit( $start_date_arr[1] ).'-20'. $start_date_arr[2] );
	$renewal_period['current_period_end']   = strtotime( $end_date_arr[0].  '-'. month_to_digit($end_date_arr[1]).    '-20'. $end_date_arr[2]   );
	
	/*
	if( strtolower($renewal_month) == 'sep' ){
		$renewal_period['current_period_end'] = strtotime('25-'.(month_to_digit($renewal_month)-2).'-20'.$renewal_year );
	}else{
		$renewal_period['current_period_end'] = strtotime('25-'.month_to_digit($renewal_month).'-20'.$renewal_year );
	}*/

	return $renewal_period;
}

function month_to_digit($month){
	$month = convert_string($month);

	switch( strtolower($month) ){
		case 'jan':
			$num = 1;
		break;

		case 'feb':
			$num = 2;
		break;

		case 'mar':
			$num = 3;
		break;

		case 'apr':
			$num = 4;
		break;

		case 'may':
			$num = 5;
		break;

		case 'jun':
			$num = 6;
		break;

		case 'july':
			$num = 7;
		break;

		case 'aug':
			$num = 8;
		break;

		case 'sep':
			$num = 9;
		break;

		case 'oct':
			$num = 10;
		break;

		case 'nov':
			$num = 11;
		break;

		case 'dec':
			$num = 12;
		break;

		default:
			$num = 0;
	}

	return $num;
}

function get_wc_country($country){
	$country = convert_string($country);

	$countries = WC()->countries->get_countries();
	
	$comp_country = '';
	switch(strtolower($country)){
		case 'republic of ireland':
		case 'irish republic':
		case 'ireland':
			$comp_country = 'Ireland';
		break;

		case 'usa':
			$comp_country = 'United States (US)';
		break;

		case 'united kingdom':
			$comp_country = 'United Kingdom (UK)';
		break;

		case 'south atlantic ocean':
		case 'south africa':
			$comp_country = 'Ireland';
		break;

		default:
			$comp_country = $country;
	}
	
	$code = array_search( $comp_country, $countries );

	return $code;
}