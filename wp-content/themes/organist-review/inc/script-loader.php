<?php
add_action( 'wp_enqueue_scripts', 'organist_review_scripts' );
function organist_review_scripts() {
	global $organist_opt;
	//wp_enqueue_script( 'jquery3', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), null, true );
	wp_enqueue_style( 'organist-review-style', get_stylesheet_uri(), array('select2') );

	$assets_url = get_stylesheet_directory_uri().'/assets/build';
	$suffix = '.min';

	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}

	wp_enqueue_script( 'stripe', 'https://js.stripe.com/v2/', array(), null, true );

	if( !wp_style_is( 'select', 'registered') ){
		$assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
		wp_register_style( 'select2', $assets_path . 'css/select2.css' );	
	}

	wp_enqueue_script( 'wc-credit-card-form' );
	
	wp_enqueue_script( 'organist-script', $assets_url . '/js/script'  . $suffix . '.js', array( 'jquery','select2' ), null, true );

	$main_css = $assets_url . '/css/main'.$suffix.'.css';
	$script = "
	   var head  = document.getElementsByTagName('head')[0];
	   var link  = document.createElement('link');
	   link.rel  = 'stylesheet';
	   link.type = 'text/css';
	   link.href = '".$main_css."';
	   link.media = 'all';
	   head.appendChild(link);
	";
    wp_add_inline_script( 'organist-script',$script );

    $webfont = "WebFontConfig = {
			google: { families: [ 'Lato:100,100i,300,300i,400,400i,700,700i', 'Oswald:300,400,700' ] }
		 };
		 (function() {
		   var wf = document.createElement('script');
		   wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		   wf.type = 'text/javascript';
		   wf.async = 'true';
		   var s = document.getElementsByTagName('script')[0];
		   s.parentNode.insertBefore(wf, s);
		 })(); ";
	wp_add_inline_script('organist-script', $webfont );

	$localized_var = array(
		'stripe_pk' => $organist_opt['stripe_publishable_key'],
		'ajax_url'	=> admin_url('admin-ajax.php'),
		'wc_price'	=> wc_price('0'),
		'errors'	=> array(
			'card_details_missing' 	=> __('Credit card details are missing','organist'),
			'invalid_number'		=> __('Invalid credit card number','organist'),
			'invalid_cvc'			=> __('Invalid CVC number','organist'),
			'invalid_period'		=> __('The date is invalid','organist'),
			'invalid_amount'		=> __('The total amount for order is invalid','organist'),
			'no_coupon_provided'	=> __('No coupon code provided','organist'),
			'coupon_gone_wrong'		=> __('Something went wrong while applying coupon','organist'),
			'invalid_confirm_email' => __( 'Email address mismatch, please confirm that you have entered the correct email address', 'organist' ),
		),
		'notice' => array(
			'processing_order' 		=> __('Please wait while we are processing your order','organist'),
			'coupon_applied' 		=> __('%s discount is ready to be applied for your order','organist'),
			'loading'				=> __('Please wait...','organist'),
			'cancel_subscription'	=> array(
				'title' => __('Are you sure?','organist'),
				'text' => ''
			),
			'renew_subscription'	=> array(
				'title' => __('Are you sure?','organist'),
				'text' => __('Once you renew your subscription, you will get charged affected immediately','organist')
			),
			'update_card'	=> array(
				'title' => __('Are you sure?','organist'),
				'text' => __('Once you change your card, you will not be able to switch to old one','organist')
			),
			'success'		=> __('Success','organist'),
			'failed'		=> __('Error','organist'),
		)
	);

	if(is_user_logged_in()){
		$date = get_user_meta(get_current_user_id(),'current_period_end',true);
		if($date){
			$localized_var['notice']['cancel_subscription']['text'] = sprintf( __('Once you cancel your subscription, you will not receive future issues after %s','organist'),date("d-M-Y", $date ) );
		}
	}


	wp_localize_script( 'organist-script', 'orgReview', $localized_var );

    wp_dequeue_style( 'woocommerce-general' );
    wp_dequeue_style( 'woocommerce-smallscreen' );
    wp_dequeue_style( 'woocommerce-layout' );

    if( is_404() ):
        wp_enqueue_script( 'organist-404', $assets_url . '/js/404/404' . $suffix . '.js', array( 'jquery'), null, true );
    endif;

	//wp_enqueue_style( 'organist-style', $assets_url . '/css/main' . $suffix . '.css' );
}


add_action( 'admin_enqueue_scripts', 'organist_admin_script' );
function organist_admin_script(){
	$assets_url = get_stylesheet_directory_uri().'/assets/build';
	$suffix = '.min';

	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}

	$screen = get_current_screen();

	wp_enqueue_script( 'admin', $assets_url.'/js/admin' . $suffix . '.js', array('jquery'), null, true );

	if( 'widgets' == $screen->id ){

		if ( ! wp_script_is( 'jquery', 'done' ) ) {
			wp_enqueue_script( 'jquery' );
	   	}

	   	wp_enqueue_media();
		$media_upload = "function media_upload(button_class) {

		    jQuery('body').on('click', button_class, function(e) {

		            var target = jQuery(this).attr('data-target');
		            var send_attachment_bkp = wp.media.editor.send.attachment;
		            var button = jQuery('#'+jQuery(this).attr('id'));

		            wp.media.editor.send.attachment = function(props, attachment){
		                jQuery(target).val(attachment.url);
		            }
		            wp.media.editor.open(button);
		                return false;

		    });
		} jQuery(document).ready(function(){ media_upload( '.custom_media_button' ); });";

		wp_add_inline_script( 'jquery-migrate', $media_upload );
	}
}
