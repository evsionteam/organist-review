jQuery(document).ready(function() {
   /* jQuery('#slider').lightSlider({
        auto:true,
        item: 4,
        autoWidth: false,
        responsive : [1],
        loop:false,
        
        controls: true,
        pager:false
    });*/


    jQuery('#slider').lightSlider({
        item: 3,
        autoWidth: false,
        mode: "slide",
        auto: false,
        pager: true,
        slideMove:1,
        enableTouch: true,
        enableDrag: true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ],
    });
});


function search(param) {
    this.header = param.header,
        this.toggler = param.toggler,
        this.searchEle = param.searchEle,
        this.enabled = false,
        this.init = function() {
            var that = this;

            $(document).on('click touchstart', this.toggler, function(e) {
                e.stopPropagation();
                that.enabled = true;
                $(that.header).slideUp(100);
                $(that.searchEle).slideDown(200);
            });

            $(document).on('click touchstart', function(e) {
                if ($(e.target).closest(that.searchEle).length === 0 && that.enabled) {
                    that.enabled = false;
                    $(that.header).slideDown(100);
                    $(that.searchEle).slideUp(200);
                    //
                }
            });
        }
}

function classToggler(param) {

    this.animation = param.animation,
        this.toggler = param.toggler,
        this.exceptions = param.exceptions;
    this.init = function() {
        var that = this;

        // for stop propagation
        var stopToggler = this.implode(this.exceptions);
        if (typeof stopToggler !== 'undefined') {
            $(document).on('click touchstart', stopToggler, function(e) {
                e.stopPropagation();
            });
        }

        // for toggle class
        var toggler = this.implode(this.toggler);
        if (typeof toggler !== 'undefined') {

            $(document).on('click touchstart', toggler, function(e) {
                e.stopPropagation();
                e.preventDefault();
                that.toggle();
            });
        }
    }

    // open class toggler
    this.toggle = function() {
        var selectors = this.implode(this.animation);
        if (typeof selectors !== 'undefined') {
            $(selectors).toggleClass('open');
        }
    }

    // array selector maker
    this.implode = function(arr, imploder) {

        // checking arg is array or not
        if (!(arr instanceof Array)) {
            return arr;
        }
        // setting default imploder
        if (typeof imploder == 'undefined') {
            imploder = ',';
        }

        // making selector
        var data = arr;
        var ele = '';
        for (var j = 0; j < arr.length; j++) {
            ele += arr[j];
            if (j !== arr.length - 1) {
                ele += imploder;
            }
        }
        data = ele;
        return data;
    }
}

function animateNum() {

    // var productList = document.getElementById('organist-product-list');
    var productList = document.getElementById('subscription');
    if (productList == null) {
        return;
    }

    var waypoint = new Waypoint({
        element: productList,
        handler: function(direction) {
            var comma_separator_number_step = jQuery.animateNumber.numberStepFactories.separator(',');
            jQuery('.pie-value').animateNumber({
                number: jQuery('.pie-value').attr('data-total'),
                numberStep: comma_separator_number_step
            }, 1000);
        },
        offset: 500
    });
}

/*function productCaroufredsel() {

    var ele = document.getElementById('organist-product-thumbnails');
    if (ele == null) {
        return;
    }

    jQuery('#organist-product-thumbnails').carouFredSel({
        auto: false,
        responsive: true,
        items: {
            visible: {
                max: 5,
                min: 1
            }
        },
        swipe: {
            onTouch: true,
            onMouse: true
        }
    });
}
*/
function preloader(param) {
    this.time = param.time;
    this.init = function () {
        var time = this.time;
        // $(window).load(function () {
        //     jQuery(".preloader").fadeOut();
        //     jQuery(".preloader-wrap").delay(time).fadeOut("slow");
        // })

        window.addEventListener('load', function(){
            jQuery(".preloader").fadeOut();
            jQuery(".preloader-wrap").delay(time).fadeOut("slow");
        })
    }
}

$(window).on('load', function() {


    var $grid = $('.grid').isotope({
        filter: 'li.recent_products'
    });

    // filter items on button click
    $('.filter-button-group').on('click', function() {
        var filterValue = $(this).attr('data-filter');
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        $grid.isotope({
            filter: filterValue
        });
    });

    animateNum();

    //productCaroufredsel();
    var isSafari = /constructor/i.test(window.HTMLElement),
    // var isiOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    isiOS = parseFloat(
    ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1])
    .replace('undefined', '3_2').replace('_', '.').replace('_', ''));

    if(isSafari) {
        console.log('SAFARI DETECTED');
        jQuery('body').addClass('onsafari');
        if(jQuery('body').hasClass('home')){
            jQuery('.filter-button-group:first-child').trigger('click');
        }
        // jQuery('body.home div.woocommerce ul.products.grid').css({'height' : 'auto'});
        // jQuery(document).on('click touchstart', '.onsafari .mobile-menu', function(e){
        //     e.preventDefault();
        //     var wheight = jQuery(window).height();
        //     jQuery('html').addClass('mm-opened mm-blocking mm-background mm-effect-slide-menu mm-pageshadow mm-opening');
        //     jQuery('nav#menu').addClass('mm-current mm-opened');
        //     jQuery('.mm-slideout').css({'height' : wheight });
        // });

        jQuery('body').on('click touchstart', '#mm-blocker, .mm-navbar .mm-title', function(e){
            jQuery('#mm-blocker').trigger('click');
        });
    }

    if(isiOS){
        jQuery('body').addClass('iosdevice');
        if(jQuery('body').hasClass('home')){
            jQuery('.filter-button-group:first-child').trigger('click');
        }
    }

});

// new preloader({
//     time: 500
// }).init();


//document
jQuery(document).ready(function($) {

    var searchConfig = {
        animation: ['#search', '#overlay', 'body'],
        exceptions: ['#search'],
        toggler: ['.search-icon', '#overlay']
    };

    new classToggler(searchConfig).init();

    //jquery ui
    var month = null,
        year = null;

   jQuery('select').select2();

    // ios menu button
    // jQuery(document).on('click touchstart', '.iosdevice .mobile-menu', function(e){
    //     e.preventDefault();
    //     var wheight = jQuery(window).height();
    //     jQuery('html').addClass('mm-opened mm-blocking mm-background mm-effect-slide-menu mm-pageshadow mm-opening');
    //     jQuery('nav#menu').addClass('mm-current mm-opened');
    //     jQuery('.mm-slideout').css({'height' : wheight });
    // });

    // jQuery(document).on('click touchstart', '.iosdevice #mm-blocker', function(e){
    //     jQuery('html').removeClass('mm-opened mm-blocking mm-background mm-effect-slide-menu mm-pageshadow mm-opening');
    //     jQuery('nav#menu').removeClass('mm-current mm-opened');
    // });

    if(jQuery('body').hasClass('onsafari')){
        jQuery(document).on('click touchstart', '#mm-blocker, .mm-navbar .mm-title', function(e){
            jQuery('#mm-blocker').trigger('click');
        });
    }

   /* $("html").niceScroll({
        cursorcolor: "#632637",
        cursorwidth: "7px",
        cursorborderradius: "0px",
        cursorborder: "none",
        smoothscroll: true,
        mousescrollstep: 90,
        zindex: 999999999999,
        autohidemode: false,
        horizrailenabled: false,
    });*/

    // $('#or-subscribe').on('submit', function(){
    //     if($('.sweet-alert').hasClass('visible')) {
    //         $('.sweet-alert').niceScroll({
    //             cursorcolor: "#632637",
    //             cursorwidth: "3px",
    //             cursorborderradius: "0px",
    //             cursorborder: "none",
    //             smoothscroll: true,
    //             mousescrollstep: 90,
    //             zindex: 999999999999,
    //             autohidemode: false,
    //             horizrailenabled: false,
    //         });
    //     }
    // });


    //$('.or-next button').click(function(e){e.preventDefault()});

    jQuery('.qty-increase, .qty-decrease').on('click touchstart', function(e) {

        e.preventDefault();
        if (jQuery(this).hasClass('qty-increase')) {

            var $input = jQuery(this).prev('.input-text');
            var val = parseInt($input.val());

            if (isNaN(val)) {
                $input.val(1);
            } else {
                $input.val(++val);
            }
        } else {

            var $input = jQuery(this).next('.input-text');
            var val = parseInt($input.val());

            if (isNaN(val)) {
                $input.val(1);
            } else {

                if (val <= 1) {
                    $input.val(1);
                } else {
                    $input.val(--val);
                }
            }
        }

        jQuery(this).trigger('change');
    });

    //trigger cart submit
    jQuery('#cart-submit').on('click', function() {
        jQuery('input[name="update_cart"]').trigger('click');
    });


    jQuery('nav#menu').mmenu({
        extensions: ['effect-slide-menu', 'pageshadow'],
        navbar: {
            title: 'Menu'
        },
    });

    jQuery('body').on('click touchstart', '.organist-mini-cart-toggler', function(e) {
        e.preventDefault();
        jQuery('.organist-mini-cart').slideToggle();
    });

    $(document.body).on('added_to_cart', function(event, fragments, cart_hash) {

        var $cartCounter = $('.organist-mini-cart-toggler span.items');
        var count = parseInt($cartCounter.text());
        count = isNaN(count) ? 0 : count;
        $cartCounter.text(++count);
        $('.organist-mini-cart').html(fragments["div.widget_shopping_cart_content"]);
    });

    jQuery(document.body).on('updated_cart_totals', function() {

        var total = 0;

        jQuery('.input-text.qty').each(function(key, val) {

            var val = parseInt(jQuery(this).val());

            if (isNaN(val))
                val = 0;

            total = total + val;

        });

        jQuery('.organist-mini-cart-toggler .items').text(total);
    });

});
