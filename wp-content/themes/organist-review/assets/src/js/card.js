+(function(){
	jQuery(document).ready(function(){
		card.initiate();
	});

	var card = {
		form: null,
		stripePK : null,
		ajaxURL: null,
		submitBtn: null,
		tempVar: null,
		errorMessage: null,
		notice: null,
		notificationContainer: null,

		initiate: function(){
			this.ajaxURL 		= orgReview.ajax_url;
			this.stripePK 		= orgReview.stripe_pk;
			this.form 			= jQuery('#or-change-cc');
			this.submitBtn 		= this.form.find('button[type=submit]');
			this.errorMessage 	= orgReview.errors;
			this.notice 		= orgReview.notice;
			this.notificationContainer = jQuery('#notification');

			this.form.on('submit',this,this.createToken);
		},

		createToken: function(event){
			var self = event.data;
			self.tempVar = self.submitBtn.find('.or-arrow > i').attr('class');
			self.submitBtn.find('i.fa-spinner').removeClass('hidden');
			self.submitBtn.prop('disabled',true);

			var data = self.collectCardDetails();
			
			if(data){
				Stripe.card.createToken(data, stripeResponseHandler);
			}

			return false;
		},

		handleSubmission: function(token){
			var self = this;
			self.submitBtn.find('i.fa-spinner').addClass('hidden');

			this.form.append( jQuery('<input type="hidden" name="card_token" />').val(token) );
			swal({
				title: self.notice.update_card.title,
				text: self.notice.update_card.text,
				type: "warning",
				showCancelButton: true,
			  	closeOnConfirm: false,
			  	showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {
					self.__makeRequest({ action:'organist_update_subcription_card', token: token, user_id:jQuery('input#user_id').val() }, function(){
						self.submitBtn.find('i.fa-spinner').removeClass('hidden');
					}, function(response){
						var swalObj = {
							title: self.notice.success,
							text: response.message,
							type: "success",
							html: true,
							confirmButtonText: 'Ok'
						};

						if(response.status != 200 ){
							swalObj.title = self.notice.failed;
							swalObj.type = "error";
						}

						swal(swalObj);

						self.submitBtn.find('i.fa-spinner').addClass('hidden');
						self.submitBtn.prop('disabled',false);
					}, function(error){
						console.error(error);
					});
				}else{
					self.submitBtn.prop('disabled',false);
				}
			});
			
			return false;
		},

		collectCardDetails: function(){
			var cvc = jQuery('#cvc').val();
			var cvv = jQuery('#cvv').val();

			var exp_month = jQuery('#card-expiry-month').val();
			var exp_year = jQuery('#card-expiry-year').val();

			var error = "";

			if(cvv && cvc && exp_month && exp_year ){
				var cardDetails = {
					number: cvv,
					cvc : cvc,
					exp_month: exp_month,
					exp_year: exp_year,
				};

				if( !Stripe.card.validateExpiry(cardDetails.exp_month, cardDetails.exp_year ) ){
					error += "<li>"+this.errorMessage.invalid_period+"</li>";
				}	

				if(!Stripe.card.validateCardNumber(cvv) ){
					error += "<li>"+this.errorMessage.invalid_number+"</li>";
				}

				if(!Stripe.card.validateCVC(cvc) ){
					error += "<li>"+this.errorMessage.invalid_cvc+"</li>";
				}

				if(error!=""){
					this.submitBtn.find('i.fa-spinner').addClass('hidden');
					this.submitBtn.prop('disabled',false);
					this.handleError({ message: "<ul>"+error+"</ul>", code: "invalid_credit_card"});
					return false;
				}

				return cardDetails;
			}else{
				this.handleError({ message: this.errorMessage.card_details_missing, code: "card_details_missing"});
			}

			return false;
		},

		handleError: function(error){
			swal({
  				title: "Error!",
  				text: error.message,
  				timer: 3000,
  				type: "error",
  				html: true 
			});
			this.submitBtn.find('.or-arrow > i').removeAttr('class').addClass(this.tempVar);
			this.submitBtn.prop('disabled',false);
		},

		__makeRequest: function(allData, beforeSend, onSuccess, onError){
			var self = this;
			jQuery.ajax({
				url: self.ajaxURL,
				data: allData,
				type: 'POST',
				dataType: 'json',
				beforeSend: beforeSend,
				success: onSuccess,
				error: onError
			});
		},
	}


	function stripeResponseHandler(status, response){
		if (response.error) {
		   	card.handleError(response.error);
		} else {
		 	card.handleSubmission(response.id);
		}
	}
})(jQuery);