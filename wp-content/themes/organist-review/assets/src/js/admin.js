jQuery(document).ready(function(){
	mailchimpAdmin.init();
});

var mailchimpAdmin = {
	ajaxUrl: null,
	spinner: null,

	init: function(){
		this.ajaxURL = ajaxurl;
		this.spinner = jQuery('#or-spinner');		
		jQuery('#notify_user').on('click',this, this.handlePassiveNotification );
		jQuery('#notify_user').on('click',this, this.handlePassiveNotification );
		jQuery('#sync-stripe').on('click',this, this.syncWithStripe );
		this.handleDependency();
	},

	handlePassiveNotification: function(event){
		event.preventDefault();
		var self = event.data;
		self.__makeRequest({action:'notify_passive_users'},function(){
			self.spinner.addClass('is-active');
		},function(response){
			if( (response.status == 200) || (response.status == 201) ){
				self.__handleSuccess(response.data);
				console.log(response);
			}else{
				self.__handleError(response.data);
			}
			self.spinner.removeClass('is-active');
		},function(error){
			console.error(error);
			self.spinner.removeClass('is-active');
		});
	},

	handleDependency: function(){
		jQuery('*[data-dependent]').on('change',function(){
			var elems = jQuery(this).data('dependent').split(',');
			jQuery.each(elems, function(index, elem){
				if( jQuery(elem).length > 0 ){
					$prop = true;
					
					if(jQuery(elem).prop('disabled')){
						$prop = false;
					}

					jQuery(elem).prop('disabled', $prop );
				}
			});
		});
	},

	syncWithStripe: function(event){
		event.preventDefault();
		var self = event.data;
		var userID = jQuery('input[name=user_id]').val();
		var stripeKey = jQuery('#subscription_id').val();
		var btn = jQuery(this);
		var btnVal = btn.text();

		self.__makeRequest({action:'sync_with_stripe', subscription_id:stripeKey, user_id:userID },function(){
			btn.text(btn.data('loading'));
		},function(response){
			if( (response.status == 200) || (response.status == 201) ){
				self.__handleSuccess(response.data);
				btn.text(btnVal);
				location.reload();
			}else{
				self.__handleError(response.data);
			}
			
		},function(error){
			console.error(error);
			self.spinner.removeClass('is-active');
		});
	},

	__makeRequest: function(allData, beforeSend, onSuccess, onError){
		var self = this;
		jQuery.ajax({
			url: self.ajaxURL,
			data: allData,
			type: 'POST',
			dataType: 'json',
			beforeSend: beforeSend,
			success: onSuccess,
			error: onError
		});
	},

	__handleError: function(error){		
		var errorHTML = '<div id="message" class="error notice is-dismissible '+error.code+'" ><p>'+error.message+'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		jQuery('div.wrap > h1').after(errorHTML);
	},

	__handleSuccess: function(success){
		var errorHTML = '<div id="message" class="updated notice is-dismissible '+success.code+'" ><p>'+success.message+'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		jQuery('div.wrap > h1').after(errorHTML);
	}
};