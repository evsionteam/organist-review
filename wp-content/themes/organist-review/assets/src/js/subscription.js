+(function(){
	jQuery(document).ready(function(){
		swal.setDefaults({ 
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			customClass: 'or-swal'
		});
		subscription.initiate();
	});

	var subscription = {
		form: null,
		formContainer: null,
		orderData: null,
		stripePK : null,
		ajaxURL: null,
		submitBtn: null,
		tempVar: null,
		errorMessage: null,
		notice: null,
		notificationContainer: null,
		confirmBlock: null,
		rawRow : null,
		rawPrice: null,

		initiate: function(){
			var self = this;

			this.formContainer = jQuery('.form-container');
			this.form = jQuery('#or-subscribe');
			this.stripePK = orgReview.stripe_pk;
			this.ajaxURL = orgReview.ajax_url;
			this.rawPrice = orgReview.wc_price;
			this.submitBtn = this.form.find('button[name=submit_cart]');
			this.errorMessage = orgReview.errors;
			this.notice = orgReview.notice;
			this.notificationContainer = jQuery('#notification');
			this.totalBlock = jQuery(jQuery('#order-template').html());
			this.rawRow = "<tr><td><%thead%></td><td><%tdata%></td></tr>";

			this.form.on('submit', this, this.createToken);
			this.form.find('.apply-coupon').on('click',this,this.applyCoupon );
			this.form.find('select[name=billing_country], select[name=shipping_country]').on('change',this,this.getState);
			
			jQuery('.edit-account').find('select[name=billing_country], select[name=shipping_country]').on('change',this,this.getState);
			jQuery('#renew-subscription').on('click',this,this.renewSubscription);
			jQuery('#cancel-subscription').on('click',this,this.cancelSubscription);
			jQuery('#copy_from_billing').on('change',this,this.copyFromBilling);

			jQuery('input[name=subscription_package]').on('change',function(event){
				self.toggleSocialMembership();
				self.toggleLocationAssoc();
			});

			jQuery('#reveal-coupon').on('click',function(event){
				event.preventDefault();
				jQuery('.coupon-block').slideToggle();
			})

			jQuery(document).on("stateFetched",function(event){
				
				if( event.dataType == "shipping" ){
					var val = jQuery('*[name=billing_state]').val();
					jQuery('*[name=shipping_state]').val(val);
					console.log(val);
				}
			});

			jQuery('input[name=terms]').on('change', function(){
				if(jQuery(this).prop('checked')){
					jQuery('[name=submit_cart]').prop('disabled',false);
				}else{
					jQuery('[name=submit_cart]').prop('disabled',true);
				}
			});

			this.toggleLocationAssoc();
			this.toggleSocialMembership();
			
			var country = jQuery('select[name=billing_country]').val();			
			var type = jQuery('select[name=billing_country]').data('type');
			var stateContainer = jQuery('select[name=billing_country]').parents('.form-fields-container').find('.state-container');
			this.__collectState(country, type, stateContainer,self);

			country = jQuery('select[name=shipping_country]').val();			
			type = jQuery('select[name=shipping_country]').data('type');
			stateContainer = jQuery('select[name=shipping_country]').parents('.form-fields-container').find('.state-container');
			this.__collectState(country, type, stateContainer,self);

			Stripe.setPublishableKey(this.stripePK);
		},

		getState: function(event){
			var self = event.data;
			var country = jQuery(this).val();
			var type = jQuery(this).data('type');
			var stateContainer = jQuery(event.target).parents('.form-fields-container').find('.state-container');
			self.__collectState(country, type, stateContainer,self);
		},

		__collectState: function(country, type, stateContainer,self){
			if( typeof country != "undefined" && country != "" ){
				self.__makeRequest({ action:'or_get_state', country: country, type: type }, function(){
					self.tempVar =  stateContainer.html();
					stateContainer.html('<p>'+self.notice.loading+'</p>');
				},function(response){
					if( response.status == 500 ){
						stateContainer.html(self.tempVar);
						self.tempVar = null;
					}else{
						stateContainer.html(response.data);
						if(response.status != 404 ){
							jQuery('#or-state').select2();
						}
					}

					jQuery(document).trigger({
						type: "stateFetched",
						dataType: type
					});
				});
			}else{
				stateContainer.html(self.tempVar);
				self.tempVar = null;
			}	
		},

		createToken: function(event){

			if(jQuery('input[name=terms]').prop('checked')){
				var self = event.data;
				self.tempVar = self.submitBtn.find('.or-arrow > i').attr('class');
				self.submitBtn.find('.or-arrow > i').removeAttr('class').addClass('fa fa-spinner fa-spin');
				self.submitBtn.prop('disabled',true);

				var data = self.collectCardDetails();
				
				if(data){
					Stripe.card.createToken(data, stripeResponseHandler);
				}	
			}
			
			return false;
		},

		collectCardDetails: function(){
			var cvc = jQuery('#cvc').val();
			var cvv = jQuery('#cvv').val();

			var exp_month = jQuery('#card-expiry-month').val();
			var exp_year = jQuery('#card-expiry-year').val();

			var error = "";

			/* Validate Email */
			var email = jQuery( 'input[name=billing_email]' ).val(),
				confirmEmail = jQuery( 'input[name=billing_confirm_email]' ).val();

			if( email != confirmEmail ){
				this.handleError({ message: orgReview.errors.invalid_confirm_email });
				return false;
			}

			if(cvv && cvc && exp_month && exp_year ){
				var cardDetails = {
					number: cvv,
					cvc : cvc,
					exp_month: exp_month,
					exp_year: exp_year,
				};

				if( !Stripe.card.validateExpiry(cardDetails.exp_month, cardDetails.exp_year ) ){
					error += "<li>"+this.errorMessage.invalid_period+"</li>";
				}	

				if(!Stripe.card.validateCardNumber(cvv) ){
					error += "<li>"+this.errorMessage.invalid_number+"</li>";
				}

				if(!Stripe.card.validateCVC(cvc) ){
					error += "<li>"+this.errorMessage.invalid_cvc+"</li>";
				}

				if(error!=""){
					this.submitBtn.find('.or-arrow > i').removeAttr('class').addClass(this.tempVar);
					this.submitBtn.prop('disabled',false);
					this.handleError({ message: "<ul>"+error+"</ul>", code: "invalid_credit_card"});
					return false;
				}

				return cardDetails;
			}else{
				this.handleError({ message: this.errorMessage.card_details_missing, code: "card_details_missing"});
			}

			return false;
		},

		copyFromBilling: function(event){
			var self = event.data;
			var name = "";

			if(jQuery(event.target).prop('checked')){
				jQuery.each(jQuery('*[name^="billing_"]'), function( key, object ) {
					name = (jQuery(object).attr('name')).replace('billing_','shipping_');
					jQuery( "[name="+name+"]" ).val(jQuery(object).val());
				});

				if(jQuery( "[name=billing_country]").val() != "" ){
					jQuery( "[name=shipping_country]").trigger('change');
				}

				jQuery('select').select2();
			}
		},

		handleSubmission: function(token){
			this.form.append( jQuery('<input type="hidden" name="card_token" />').val(token) );
			
			var formData = this.form.serializeObject();		
			this.orderData = formData;
			this.confirmOrder(formData);

			return false;
		},

		processSubmission: function(formData){
			var self = this;
			
			var coupon_code = jQuery('input[name=coupon_value]').data('coupon-id');
			self.__makeRequest({ action:'handle_organists_subscription', data: formData, coupon: coupon_code }, function(){},
			function(response){
				if( (response.status == 200) || (response.status == 201) ){
					var threshold = 10;
					var message = response.message;
					message = message.replace('@time','<span id="redirect-period">'+threshold+'</span>');

					self.handleInfo({id:'subscription_created',message:message},false);
					
					var interval = setInterval(function(){ 
						if(threshold > 0){
							jQuery('#redirect-period').html(threshold);
							threshold--;	
						}else{
							if( (typeof response.redirect != "undefined" ) || (response.redirect != null)  ){
								clearInterval(interval);
								location.href = response.redirect;
							}
						}

					}, 1200);
				}else{
					self.handleError(response.data);
				}
			});
		},

		toggleLocationAssoc: function(){
			if( jQuery('input[name=subscription_package]:checked').val() == "ukassociationmember" ){				
				jQuery('input[name=local_association]').parent().removeClass('hidden');
			}else{
				jQuery('input[name=local_association]').parent().addClass('hidden');
			}
		},

		toggleSocialMembership: function(){
			jQuery('input[name=society_membership]').removeAttr('checked');
			if( jQuery('input[name=subscription_package]:checked').val() == "ukfullsub" ){
				jQuery('[name=society_membership]').prop('checked',false);
				jQuery('#yes').prop('checked',true);
				jQuery('input[name=society_membership]').parents('div.or-block').removeClass('hidden');
			}else{
				jQuery('[name=society_membership]').prop('checked',false);
				jQuery('#no').prop('checked',true);
				jQuery('input[name=society_membership]').parents('div.or-block').addClass('hidden');
			}
		},

		applyCoupon: function(event){
			var self = event.data;
			var btn = jQuery(this);

			var coupon = jQuery('input[name=coupon_code]').val();

			if(!coupon){
				self.handleError({id:'no_coupon_provided', message: self.errorMessage.no_coupon_provided });
				return;
			}

			self.__makeRequest({ action:'apply_organists_coupon', coupon_code:coupon }, function(){
			/* Before Send*/
				btn.prepend('<i class="fa fa-spinner fa-spin"></i>');
			}, function(response){
			/* Success */
				if( (response.status == 200) || (response.status == 201) ){
					var couponHolder = jQuery('input[name=coupon_value]');

					var amount = '';
					if( response.data.amount_off != null ){
						couponHolder.val((response.data.amount_off/100));
						couponHolder.attr('data-coupon-type','amount');

						amount = self.rawPrice.replace(new RegExp("[0-9]+(\.[0-9][0-9]?)?", "g"), ( response.data.amount_off/100 ) );
					}else if( response.data.percent_off != null ){
						couponHolder.val(response.data.percent_off);
						couponHolder.attr('data-coupon-type','percent');

						amount = response.data.percent_off+'%';
					}else{
						self.handleError({id:'coupon_gone_wrong', message: self.errorMessage.coupon_gone_wrong });
						return;
					}

					couponHolder.attr('data-coupon-id',response.data.id);

					btn.text(btn.attr('data-after-text')).prop('disabled',true);
					jQuery('input[name=coupon_code]').prop('readonly',true);


					var notice = self.notice.coupon_applied.replace('%s',amount);
					self.handleInfo({id: 'coupon_applied', message: notice });
				}else{
					self.formContainer.fadeIn(500,function(){
						self.notificationContainer.html('');
						self.handleError(response.data);
					});
				}

				btn.find('i.fa.fa-spinner.fa-spin').remove();
			});
		},

		handleError: function(error){
			swal({
  				title: "Error!",
  				text: error.message,
  				timer: 3000,
  				type: "error",
  				html: true 
			});
			this.submitBtn.find('.or-arrow > i').removeAttr('class').addClass(this.tempVar);
			this.submitBtn.prop('disabled',false);
		},

		handleInfo: function(info,callback,showConfirm){
			if( typeof callback == "undefined" ){
				var callback = function(isConfirm){};
			}

			if( typeof showConfirm == "undefined" ){
				var showConfirm = true;
			}

			swal({
  				title: this.notice.success,
  				text: info.message,
  				timer: 5000,
  				type: "success",
  				html: true,
  				confirmButtonText: 'Ok',
  				showConfirmButton: showConfirm
			},callback);
		},

		confirmOrder: function(data){
			var ignoreList = ['card_token','coupon_code','special_comment','coupon_value', 'billing_confirm_email', 'billing_address_1','billing_address_2','billing_phone','billing_city','billing_state','billing_country','billing_postcode','issue_number','shipping_title','shipping_first_name','shipping_last_name','shipping_email','shipping_address_1','shipping_address_2','shipping_phone','shipping_city','shipping_state','shipping_country','shipping_postcode'];
			var orderHTML = '';
			console.log( data );
			var priceField = ['additional_donation','society_membership'];

			var table = jQuery('<table></table>').addClass('or-payment-detail').html('<tbody></tbody>');
			var additionalCharge = 0.0, discount = 0.0, subTotal = 0.0, total = 0.0, planCharge = 0.0, temp = null;

			var self = this;
			jQuery.each(data, function( index, value ) {
				if( value && (jQuery.inArray(index,ignoreList) == -1) ){ 
					var label = jQuery('*[name='+index+']').parents('.or-block').find('label.or-title').text();
					if( label != "" ){
						var val = getTextForField(index, unescape(value) );

						if( jQuery.inArray(index,priceField) != -1 ){
							if(!isNaN(val)){
								val = self.__formatPrice(val);
								var tdata = self.rawRow.replace("<%thead%>", label).replace("<%tdata%>", val  );
								table.find('tbody').append(tdata);
							}
						}else{
							var tdata = self.rawRow.replace("<%thead%>", label).replace("<%tdata%>", val  );
							table.find('tbody').append(tdata);
						}
						
					}
					
				}
			});

			/* Total Calculation */
			var tableParent = table.wrap('<div>');
			orderHTML += '<div class="order-body">'+tableParent.parent().html()+'</div>';			

			planCharge = parseFloat(jQuery('input[name=subscription_package]:checked').data('price'));

			if(isNaN(planCharge)){
				planCharge = 0.00;
			}

			subTotal = planCharge;

			this.totalBlock.find('#or-plan-charge').removeClass('hidden').find('td').html(this.__formatPrice(planCharge));

			var coupon_value = jQuery('input[name=coupon_value]');
			if( coupon_value.val() ){
				if( coupon_value.data('coupon-type') == "amount" ){
					discount = parseFloat(coupon_value.val());
				}else{
					discount = (planCharge*(coupon_value.val()))/100;
				}

				if(isNaN(discount)){
					discount = 0;
				}

				this.totalBlock.find('#or-discount').removeClass('hidden').find('td').html(this.__formatPrice(discount));

				subTotal -=  discount;

				this.totalBlock.find('#subtotal').removeClass('hidden').find('td').html(this.__formatPrice(subTotal));
			}


			temp = parseFloat(jQuery('input[name=society_membership]:checked').data('price'));
			if(!isNaN(temp)){
				additionalCharge += temp;
			}

			temp = parseFloat(jQuery('input[name=additional_donation]').val() );
			if(!isNaN(temp)){
				additionalCharge += temp;
			}

			this.totalBlock.find('#or-additional-charge').removeClass('hidden').find('td').html(this.__formatPrice(additionalCharge));
			
			total = subTotal + additionalCharge;			

			if(isNaN(total)){
				self.handleError({code:'invalid_amount',message:self.errorMessage.invalid_amount});
				return false;
			}

			this.totalBlock.find('#or-total').removeClass('hidden').find('td').html(this.__formatPrice(total.toFixed(2)));
	

			tableParent = this.totalBlock.wrap('<div>');
			orderHTML += '<div class="total-block">'+tableParent.parent().html()+'</div>';

			swal({
				title: 'Your order',
				text: orderHTML,
				type: "info",
				showCancelButton: true,
			  	closeOnConfirm: false,
			  	showLoaderOnConfirm: true,
			  	html: true,
			},
			function(isConfirm){
				if (isConfirm) {
					self.processSubmission(self.orderData);
				}else{
					self.submitBtn.find('.or-arrow > i').removeAttr('class').addClass(self.tempVar);
					self.tempVar = null
					self.submitBtn.prop('disabled',false);
				}
			});

			$('.sweet-alert').niceScroll({
			    cursorcolor: "#632637",
			    cursorwidth: "3px",
			    cursorborderradius: "0px",
			    cursorborder: "none",
			    touchbehavior: true,
			    smoothscroll: true,
			    mousescrollstep: 90,
			    zindex: 999999999999,
			    autohidemode: false,
			    horizrailenabled: false,
			});
		},

		renewSubscription: function(event){
			event.preventDefault();
			var btn = jQuery(this);
			var user_id = btn.data('user');
			var self = event.data;
			
			swal({
				title: self.notice.renew_subscription.title,
				text: self.notice.renew_subscription.text,
				type: "warning",
				showCancelButton: true,
			  	closeOnConfirm: false,
			  	showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {
					self.__makeRequest({ action:'or_renew_subscription', user_id:user_id }, function(){},
					function(response){
						if( (response.status == 200) || (response.status == 201) ){
							btn.prop('disabled',true);
							location.reload();
						}else{
							self.handleError(response.data);
						}
					});
				}
			});			
		},


		cancelSubscription: function(event){
			event.preventDefault();
			var btn = jQuery(this);
			var user_id = btn.data('user');
			var self = event.data;
			
			swal({
				title: self.notice.cancel_subscription.title,
				text: self.notice.cancel_subscription.text,
				type: "warning",
				showCancelButton: true,
			  	closeOnConfirm: false,
			  	showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {
					self.__makeRequest({ action:'or_cancel_subscription', user_id:user_id }, function(){},
					function(response){
						if( (response.status == 200) || (response.status == 201) ){
							
							btn.fadeOut('slow',function(){
								this.remove();
							})

							self.handleInfo({id:'subscription_cancelled',message:response.message});
						}else{
							self.handleError(response.data);
						}
					});
				}
			});
		},

		__makeRequest: function(allData, beforeSend, onSuccess){
			var self = this;
			jQuery.ajax({
				url: self.ajaxURL,
				data: allData,
				type: 'POST',
				dataType: 'json',
				beforeSend: beforeSend,
				success: onSuccess,
				error: function(error){
					console.error(error);
				}
			});
		},

		__formatPrice: function(price){
			var formatted = parseFloat(Math.round(parseFloat(price) * 100) / 100).toFixed(2);
			if(!isNaN(price)) {
				formatted = this.rawPrice.replace(new RegExp("[0-9]+(\.[0-9][0-9]?)?", "g"), formatted );
			}
			return formatted;
		}
	}


	function stripeResponseHandler(status, response){
		if (response.error) {
		   	subscription.handleError(response.error);
		} else {
		 	subscription.handleSubmission(response.id);
		}
	}

	function getTextForField(name,value){
		var label;
		if( jQuery('*[name='+name+']').is('select') ){
			/* For select field */
			label = jQuery('*[name='+name+'] option:selected').text();
		}else if( ( jQuery('*[name='+name+']').filter(':radio').length > 0 ) || (jQuery('*[name='+name+']').filter(':checkbox').length > 0) ){
			/* For radio and checkbox */
			labelWrapper = jQuery('label[for='+jQuery('*[name='+name+']:checked').attr('id')+']');
			if( labelWrapper.children().length > 0 ){
				label = labelWrapper.find('span:first').text();
			}else{
				label = jQuery('label[for='+jQuery('*[name='+name+']:checked').attr('id')+']').text();
			}
		}else{
			label = value;
		}
		return label;
	}

	jQuery.fn.serializeObject = function(){
		var formArray = jQuery(this).serializeArray();
		returnArray = {};
		for (var i = 0; i < formArray.length; i++){
			returnArray[formArray[i]['name']] = formArray[i]['value'];
		}
		return returnArray;
	}

})(jQuery);