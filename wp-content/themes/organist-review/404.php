<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Organists_Review
 */
get_header();
?>

<!-- CONNECTED ACCOUNT AND USER PROFILE -->
<div class="page-not-found clearfix">

    <div class="col-md-offset-1 col-sm-8 col-md-6">
        <div class="logo-with-message">
            <div class="message">
                <h1>404</h1>
                <h2>Page not found</h2>
                <h3>the page you requested could not be found</h3>
                <a href="<?php echo esc_url(home_url('/'));?>" class="btn-dashboard">Back to Home</a>
            </div><!-- /.message -->
        </div>
    </div><!-- /.col-md-6 -->

    <div class="col-sm-4 col-md-5">
        <div class="animated-image text-center">
           
            <!-- Generator: Adobe Illustrator 20.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 426.7 500" style="enable-background:new 0 0 426.7 500;" xml:space="preserve">
                <style type="text/css">
                    .st20{fill-rule:evenodd;clip-rule:evenodd;fill:#F9A81A;}
                    .st21{fill-rule:evenodd;clip-rule:evenodd;fill:#DD9427;}
                    .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                    .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#010101;}
                    .st4{fill-rule:evenodd;clip-rule:evenodd;fill:#805823;}
                    .st5{fill-rule:evenodd;clip-rule:evenodd;fill:#ED735A;}
                    .st6{fill-rule:evenodd;clip-rule:evenodd;fill:#245B50;}
                    .st7{fill-rule:evenodd;clip-rule:evenodd;fill:#4785A9;}
                    .st8{fill-rule:evenodd;clip-rule:evenodd;fill:#69A1C2;}
                    .st9{fill-rule:evenodd;clip-rule:evenodd;fill:#D1D3D4;}
                </style>
                <g id="bird">
                    <path id="right-leg" class="st20" d="M240.8,327.1c7.5,9.8,21.2,25,18.3,40c-2.9,15.1-7.8,27.5,1.9,31.6c9.7,4.1,0.6-9,13.6-8.3
                          c13,0.6,16.2,19.8,24.8,10.7c8.6-9.2-9.2-19.3-6.5-28.6c2.6-9.3,14.9-2.4,11.6-11.7c-3.3-9.3-11.3-6-28.3-3.6
                          c-17,2.4-16-42.1-20.3-59C251.8,281.3,223,305.9,240.8,327.1z"/>
                    <path id="left-leg" class="st20" d="M132.8,276.9c18.8-0.9,51.7,59.8,41.4,71.1c-10.4,11.3-26.5,21.4-19.4,27.1
                          c7.1,5.7,9.8-6.5,20.5,0.8c10.8,7.3,10.7,20.5,23.4,16.9c12.7-3.6-5.5-16.1,1.6-22.6c7.1-6.5,14.5,7.4,16.2-2.8
                          c1.8-10.2-6.7-12.1-22.4-18.9c-15.7-6.9-1.1-42.5-16.9-59.9C161.4,271.1,115,255.7,132.8,276.9z"/>
                    <g id="hands">
                        <path id="right-hand" class="st20" d="M306.3,146.3c-4.8-13.3,12.7-64.5,32.5-56.8c19.8,7.7-13.8,27-7.2,31.4
                              c6.6,4.4,37.5-30.3,44.1-6.6c6.6,23.7-30.9,25.4-30.9,33.1c0,7.7,30.9-8.8,27.6,8.3C369.1,172.7,315.3,171.6,306.3,146.3z"/>
                        <path id="left-hand" class="st20" d="M93.4,146.3c4.8-13.3-12.7-64.5-32.5-56.8c-19.8,7.7,13.8,27,7.2,31.4
                              c-6.6,4.4-37.5-30.3-44.1-6.6c-6.6,23.7,30.9,25.4,30.9,33.1c0,7.7-30.9-8.8-27.6,8.3C30.5,172.7,84.4,171.6,93.4,146.3z"/>
                    </g>
                    <path class="st21" d="M199.8,17.3c110.4,0,199.8,305.1,0,305.1C0.1,322.4,89.4,17.3,199.8,17.3z"/>
                    <path class="st20" d="M199.8,24.4c98,0,177.4,250.3,0,250.3C22.5,274.7,101.8,24.4,199.8,24.4z"/>
                    <ellipse transform="matrix(0.138 -0.9904 0.9904 0.138 49.4755 317.7136)" class="st21" cx="207.3" cy="130.4" rx="41.9" ry="41.9"/>
                    <ellipse transform="matrix(0.138 -0.9904 0.9904 0.138 -9.8231 250.2506)" class="st21" cx="138.9" cy="130.8" rx="35.2" ry="35.2"/>
                    <path class="st2" d="M142.7,98.6c15.3,2.1,26,16.3,23.8,31.6c-2.1,15.3-16.3,26-31.6,23.8c-15.3-2.1-26-16.3-23.8-31.6
                          C113.3,107.2,127.4,96.5,142.7,98.6z"/>
                    <g id="left-eye">
                        <path class="st3" d="M146.7,116.2c5.6,0.8,9.5,5.9,8.7,11.5c-0.8,5.6-6,9.5-11.5,8.7c-5.6-0.8-9.5-5.9-8.7-11.5
                              C135.9,119.3,141.1,115.4,146.7,116.2z"/>

                        <ellipse transform="matrix(0.1385 -0.9904 0.9904 0.1385 -0.2589 263.4497)" class="st2" cx="151.3" cy="131.9" rx="3.3" ry="3.3"/>
                    </g>
                    <path class="st2" d="M212,91.7c18.9,2.6,32.1,20.1,29.5,39c-2.6,18.9-20.1,32.1-39,29.5c-18.9-2.6-32.1-20.1-29.5-39
                          C175.6,102.3,193.1,89.1,212,91.7z"/>
                    <g id="right-eye">
                        <path class="st3" d="M217,113.5c6.9,1,11.8,7.4,10.8,14.3c-1,6.9-7.4,11.8-14.3,10.8c-6.9-1-11.8-7.4-10.8-14.3
                              C203.7,117.3,210.1,112.5,217,113.5z"/>

                        <ellipse transform="matrix(0.1381 -0.9904 0.9904 0.1381 60.3258 335.0133)" class="st2" cx="222.6" cy="132.8" rx="4.1" ry="4.1"/>
                    </g>
                    <g>
                        <path class="st4" d="M266.9,164.2c-24.3-10.7-44.3,17.4-89.9,16.9c-40.5-0.4-60.3-14.9-72-9.6c-11.7,5.2-26.9,48.1,65.4,46.4
                              C263.6,216.2,281,170.5,266.9,164.2z"/>
                        <path class="st5" d="M226.5,209.8c-7.3-13.1-17.2-13.2-28.1-17.6c-6.1-2.5-12.4,12.7-17.1,12.8c-4.2,0.1-12.6-17-17.5-14.1
                              c-9.2,5.5-16.5,13.2-22.9,25.7c8.3,1,18.1,1.5,29.6,1.3C193.5,217.5,212,214.4,226.5,209.8z"/>
                        <g>
                            <path class="st2" d="M235.2,166.7c1.2,6,6.7,10.3,13,9.8c6.7-0.5,11.8-6.4,11.2-13.2c0-0.4-0.1-0.9-0.2-1.3
                                  C251.4,161,243.8,163.3,235.2,166.7z"/>
                            <path class="st2" d="M169.9,180.9c0.8,6.5,6.5,11.3,13.1,10.8c6.3-0.5,11.1-5.7,11.3-11.8c-5.4,0.8-11.1,1.3-17.3,1.3
                                  C174.6,181.1,172.2,181,169.9,180.9z"/>
                            <path class="st2" d="M137.7,176.1c-0.1,0.8-0.1,1.6,0,2.5c0.5,6.7,6.4,11.8,13.2,11.2c5.5-0.4,9.8-4.4,11-9.5
                                  C152.6,179.4,144.6,177.8,137.7,176.1z"/>
                            <path class="st2" d="M108.2,170.6c-0.1,0.7-0.1,1.4,0,2.2c0.5,6.7,6.4,11.8,13.2,11.2c5.4-0.4,9.7-4.3,10.9-9.2
                                  C121.7,172,114.1,169.6,108.2,170.6z"/>
                            <path class="st2" d="M110.2,193.8c-4.2,0.3-7.8,2.8-9.7,6.2c3.8,4.9,10.4,9.4,20.9,12.7c1.4-2.2,2.2-4.9,2-7.7
                                  C122.9,198.3,117,193.3,110.2,193.8z"/>
                            <path class="st2" d="M173.7,209.2c-5.2,0.4-9.4,4-10.8,8.7c2.4,0,4.9,0,7.5,0c5.5-0.1,10.7-0.4,15.7-0.7
                                  C184.2,212.2,179.2,208.8,173.7,209.2z"/>
                            <path class="st2" d="M204.5,203.4c-6.7,0.5-11.7,6.4-11.2,13.1c8.9-1,16.9-2.4,24-4.1C215.7,206.8,210.4,203,204.5,203.4z"/>
                            <path class="st2" d="M234.3,194.4c-6.7,0.5-11.8,6.4-11.2,13.2c0.1,1.1,0.3,2.1,0.7,3.1c9-2.6,16.5-5.8,22.7-9.2
                                  C244.3,196.9,239.5,193.9,234.3,194.4z"/>
                            <path class="st2" d="M259.9,179.6c-6.7,0.5-11.8,6.4-11.2,13.2c0.2,2.2,0.9,4.2,2.1,5.9c8.6-5.5,14.2-11.4,17.3-16.8
                                  C265.8,180.3,263,179.4,259.9,179.6z"/>
                        </g>
                    </g>
                    <path class="st6" d="M203,35.3c14.5,0.3,28.2,9.5,20.5,9.5c-7.7,0,3.9,3.4,14,6c10.1,2.6,12.5,9.1,4.8,7.4c-7.7-1.7,4.9,2.8,8.7,9.1
                          c3.7,6.3,5.1,10-2.5,3.7c-7.6-6.3-32.7-24.1-47.3-27.2C186.5,40.6,188.4,34.9,203,35.3z"/>
                    <path class="st6" d="M159.7,62.8c-14.5,0.3-28.2,9.5-20.5,9.5c7.7,0-3.9,3.4-14,6c-10.1,2.6-12.5,9.1-4.8,7.4
                          c7.7-1.7-4.9,2.8-8.7,9.1c-3.7,6.3-5.1,10,2.5,3.7c7.6-6.3,32.7-24.1,47.3-27.2C176.1,68.2,174.2,62.5,159.7,62.8z"/>
                    <path class="st7" d="M348.8,171.4c13.8,13.4,31.8,20.9,55-1.9C394,199.6,358.3,190.5,348.8,171.4z"/>
                    <path class="st7" d="M350.7,191.2c13.8,13.4,31.8,20.9,55-1.9C395.9,219.3,360.1,210.2,350.7,191.2z"/>
                    <path class="st7" d="M329.3,80.1c10.2-14.6,25.3-24.6,50.5-8C365.5,46.5,334.6,61.1,329.3,80.1z"/>
                    <path class="st7" d="M327.5,61.8c10.2-14.6,25.3-24.6,50.5-8C363.7,28.2,332.8,42.8,327.5,61.8z"/>
                    <path class="st8" d="M73.9,175.8c-13.1,12.7-30.3,19.9-52.5-1.8C30.8,202.6,64.9,193.9,73.9,175.8z"/>
                    <path class="st8" d="M72.1,194.5c-13.1,12.7-30.3,19.9-52.5-1.8C29,221.4,63.1,212.7,72.1,194.5z"/>
                    <path class="st8 arrow-up" d="M92.5,88.6C82.8,74.7,68.4,65.1,44.3,81C57.9,56.6,87.5,70.5,92.5,88.6z"/>
                    <path class="st8 arrow-up" d="M94.2,71.2c-9.7-13.9-24.1-23.5-48.1-7.6C59.7,39.1,89.2,53.1,94.2,71.2z"/>
                </g>

                <ellipse class="st9" id="shadow" cx="222.5" cy="467.5" rx="113.1" ry="16.7"/>
            </svg>
        </div><!-- /.animated-image -->
    </div><!-- /.col-md-6 -->

</div><!-- /.connected-n-profile -->

<?php
get_footer();
