
<div class="or-dashboard">

	<div class="row">
		<div class="col-md-8">
			<div class="or-user-info">
				<div class="row">
					<div class="or-form-grounp col-md-2">
						<div class="or-label"><?php _e('Title','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_title']; ?></div>
					</div>
					<div class="col-md-10">
						<div class="row">
							<div class="or-form-grounp col-md-6">
								<div class="or-label"><?php _e('First Name','organist'); ?></div>
								<div class="or-name"><?php echo $userdata['first_name']; ?></div>
							</div>

							<div class="or-form-grounp col-md-6">
								<div class="or-label"><?php _e('Last Name','organist'); ?></div>
								<div class="or-name"><?php echo $userdata['last_name']; ?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Email','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_email']; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Phone','organist'); ?></div>
						<div class="or-name"><?php echo isset($userdata['billing_phone'])?$userdata['billing_phone']:'N/A'; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Address 1','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_address_1']; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Address 2','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_address_2']; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('State/County','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_state']; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('City/Town','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_city']; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Country','organist'); ?></div>
						<div class="or-name"><?php echo $countries[$userdata['billing_country']]; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Zip/Postcode','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_postcode']; ?></div>
					</div>
				</div>
				<?php if(isset($userdata['special_comment'])): ?>
					<div class="row">
						<div class="or-form-grounp col-md-6">
							<div class="or-label"><?php _e('Special Comment','organist'); ?></div>
							<div class="or-name"><?php echo $userdata['special_comment']; ?></div>
						</div>
					</div>
				<?php endif; ?>


			</div>
		</div><!-- /.col-md-8 -->

		<div class="col-md-4">
			<div class="or-package-detail">
				<?php
						
					if( in_array('manual_subscriber', $user->roles) ){
						$plan = organists_get_plans($userdata['manual_plan']);
						$is_manual = true;
					}
					
					if(isset($plan['nickname'])){
				?>

					<div class="or-package-name"><?php echo $plan['nickname']; ?></div>
				
				<?php
					}
				?>
				<table>
					<tr>
						<th><?php _e('Status','organist'); ?></th>
						<td>
							<?php
								if(in_array('active_subscriber', $user->roles) ){
									_e('Active','organist');
									$is_inactive = false;
								}else if ( in_array('passive_subscriber', $user->roles) ){
									_e('Inactive','organist');
								}else{
									_e('Not specified','organist');
								}
							?>
						</td>
					</tr>

					<?php

					if(!$is_manual):
						if(isset($plan['amount'],$plan['interval'])){
					?>
						<tr>
							<th><?php _e('Amount','organist'); ?></th>
							<td><?php _e(sprintf('%s per %s',wc_price($plan['amount']/100),$plan['interval'] ),'organist'); ?></td>
						</tr>
					<?php
						}
					endif;
					?>

					<tr>
						<th><?php _e('Registered Date','organist'); ?></th>
						<td><?php echo date("d-M-Y", strtotime($user->user_registered)); ?></td>
					</tr>

					<tr>
						<th><?php _e('Renew Date','organist'); ?></th>
						<td>
							<?php
								if( isset($userdata['current_period_end']) ){
									echo date("d-M-Y", $userdata['current_period_end']);
								}else{
									echo 'N/A';
								}
							?>
						</td>
					</tr>

					<tr>
						<th><?php _e('Next Issue','organist'); ?></th>
						<td><?php echo get_next_issue(get_current_user_id()); ?></td>
					</tr>
				</table>
			</div><!-- /.or-package-detail -->

			<?php if( !$is_manual && !$cancellation_proceeded ): ?>
				<?php if($is_inactive): ?>
					<button type="button" class="c-btn" id="renew-subscription" data-user="<?php echo get_current_user_id(); ?>"><?php _e('Renew Subscription','organist'); ?></button>
				<?php else: ?>
					<button type="button" class="c-btn" id="cancel-subscription" data-user="<?php echo get_current_user_id(); ?>"><?php _e('Cancel Subscription','organist'); ?></button>
				<?php endif; ?>
			<?php endif; ?>


		</div><!-- /.col-md-4-->
	</div>
</div><!-- /.or-dashboard -->
