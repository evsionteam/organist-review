<?php
  $months     = or_get_months();
  $years      = or_get_years();
?>
<div>
	<div id="notification"></div>

	<form id="or-change-cc" >
		<div class="or-cgap">
            <div class="row">

              	<div class="col-md-12 or-block">
                	<label class=" or-title"><?php _e('Card Number','organist'); ?></label>
                  	<input type="number" id="cvv" class="or-form-control" />
              	</div>

              	<div class="col-md-12 or-block">
                	<label class=" or-title"><?php _e('Expiration Date','organist'); ?></label>
                	<div class="expiry-period">
                    <select class="or-form-control" id="card-expiry-month" required>
                      <?php foreach( $months as $val => $text ): ?>
                        <option value="<?php echo $val; ?>" ><?php echo $text; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <select class="or-form-control" id="card-expiry-year" required>
                      <?php foreach( $years as $val => $text ): ?>
                        <option value="<?php echo $val; ?>" ><?php echo $text; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
              	</div>

              	<div class="col-md-12 or-block">
              	  	<label class=" or-title"><?php _e('CVC Number','organist'); ?></label>
                	<input type="number" id="cvc" class="or-form-control" />
              	</div>
            </div>
        </div>
        <input type="hidden" id="card_id" value="<?php echo $type; ?>" />
        <input type="hidden" id="user_id" value="<?php echo get_current_user_id(); ?>" />

		<button type="submit" class="btn btn-default c-btn"><span class="or-text"><?php _e('Change','organist'); ?></span> <i class="fa fa-spinner fa-spin hidden"></i></button>
	</form>
</div>
