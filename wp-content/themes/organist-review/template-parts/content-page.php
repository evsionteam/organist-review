<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Organists_Review
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="title-breadcrumbs">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
		<?php woocommerce_breadcrumb(); ?>
	</div>
	<div  class="container">
		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'organist-review' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</div><!-- #primary -->
</article><!-- #post-## -->
