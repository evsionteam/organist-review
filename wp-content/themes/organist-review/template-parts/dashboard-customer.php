<div class="or-dashboard">
	<div class="row">
		<div class="col-md-12">
			<div class="or-user-info">
				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('First Name','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['first_name']; ?></div>
					</div>

					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Last Name','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['last_name']; ?></div>
					</div>
						
				</div>
				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Email','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_email']; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Phone','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_phone']; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Address 1','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_address_1']; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Address 2','organist'); ?></div>
						<div class="or-name"><?php echo isset($userdata['billing_address_2'])?$userdata['billing_address_2']:'N/A'; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('State/County','organist'); ?></div>
						<div class="or-name"><?php echo isset($userdata['billing_state'])?$userdata['billing_state']:'N/A'; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('City/Town','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_city']; ?></div>
					</div>
				</div>

				<div class="row">
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Country','organist'); ?></div>
						<div class="or-name"><?php echo $countries[$userdata['billing_country']]; ?></div>
					</div>
					<div class="or-form-grounp col-md-6">
						<div class="or-label"><?php _e('Zip/Postcode','organist'); ?></div>
						<div class="or-name"><?php echo $userdata['billing_postcode']; ?></div>
					</div>
				</div>

			</div>
		</div><!-- /.col-md-8 -->
	</div>
</div><!-- /.or-dashboard -->
