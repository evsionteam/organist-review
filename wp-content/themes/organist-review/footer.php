<?php $org_opt = get_options(); ?>
<footer>
	<section class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<!--  Widget goes here -->
					<?php dynamic_sidebar('footer-big-sidebar'); ?>
				</div>
				
				<div class="col-sm-7">
					<div class="footer-secondary">
						<div class="row">
							<!--  Widgets goes here -->
							<?php dynamic_sidebar('footer-sidebar-1'); ?>
							<?php dynamic_sidebar('footer-sidebar-2'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="copyright-wrapper">
		<p class="text-center"><?php echo esc_attr( $org_opt['copyright_text'] ); ?></p>
	</section>
</footer>
<?php wp_footer(); ?>
</div>
</body>
</html>
