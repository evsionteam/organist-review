<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Organists_Review
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
	<script>
  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  		ga('create', 'UA-37263472-1', 'auto');
 		 ga('send', 'pageview');

	</script>
        <?php wp_head(); ?>
       
    </head>
    <?php $org_opt = get_options(); global $woocommerce;?>
    <body <?php body_class(); ?>>
        <?php /*
        <div class="preloader">
                <div class="preloader-wrap">
                    <?php if ( is_front_page() ) : ?> 
                    <p class="text-center">
                        <img src="<?php echo $org_opt['logo']; ?>" alt="<?php _e('Site Logo', 'organist'); ?>" width='228' />
                    </p>
                    <?php endif; ?>
                    <div class="loader">
                        <span></span>
                    </div>
                </div>
                <?php /* ?>
                <p class="text">Organists'<span>Review</span></p>
                <div class="icon-credit"><img src="<?php echo $org_opt['site-logo']['url']; ?>" alt="Olgashus" width='140'></div>
                
        </div>
        */ ?>
        <div>
        <?php
            $nav_args = array(                
                'theme_location' => 'primary',
                'container' => 'nav',
                'container_class' => 'mm-menu mm-offcanvas',
                'container_id' => 'menu'
            );
            wp_nav_menu($nav_args);
        ?>
        
        <!-- search-wrapper -->
        <div id="search">
            <form role="search" method="get" class="woocommerce-product-search organist-product-search" action="<?php echo esc_url(home_url('/')); ?>">
                <input type="search" id="woocommerce-product-search-field" class="search-field" 
                       placeholder="<?php echo esc_attr_x('Search Products..', 'placeholder', 'woocommerce'); ?>" 
                       value="<?php echo get_search_query(); ?>" 
                       name="s" 
                       title="<?php echo esc_attr_x('Search for:', 'label', 'woocommerce'); ?>" />
                <input type="hidden" name="post_type" value="product" />
                <button type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </form><!-- /form -->
        </div><!-- /.search-wrapper -->

        <div id="overlay"></div>
        <header class="top-header" id="header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <h1 class="logo-wrapper">
                            <a href="<?php echo esc_url(home_url('/')); ?>" alt="<?php _e('Home', 'organist'); ?>">
                                <img src="<?php echo $org_opt['logo']; ?>" alt="<?php _e('Site Logo', 'organist'); ?>" />
                            </a>
                        </h1><!-- /logo-wrapper -->
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="social-icon">
                            <ul class="list-inline">
                                <li>
                                    <a class="mobile-menu" href="#menu">
                                        <i class="fa fa-bars"></i>
                                        <span><?php _e( 'Menu','organist' ); ?></span>
                                    </a>
                                </li>
                                <li class="search-icon"><span class="fa fa-search"></span></li>
                                <li><a href="<?php echo $org_opt['fb_link']; ?>"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="<?php echo $org_opt['twit_link']; ?>"><span class="fa fa-twitter"></span></a></li>
                                <li><a href="<?php echo $org_opt['google_link']; ?>"><span class="fa fa-google"></span></a></li>
                                <li>
                                    <a href="javascript: void(0);" class="organist-mini-cart-toggler">
                                        <span class="fa fa-shopping-cart"></span>
                                        <span class="items"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                                    </a>
                                    <div class="organist-mini-cart">
                                        <?php woocommerce_mini_cart( array( 'list_class' => 'list-unstyled') ); ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <?php
                        $nav_args = array(
                            'menu_class' => 'list-inline',
                            'theme_location' => 'primary'
                        );
                        wp_nav_menu($nav_args);
                        ?>
                    </div>
                </div>
            </div>
        </header>


