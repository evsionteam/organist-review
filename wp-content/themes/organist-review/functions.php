<?php
show_admin_bar( false );
function get_assets_url(){ 
	return  get_stylesheet_directory_uri().'/assets/build';
}

add_action( 'after_setup_theme', 'organist_review_setup' );
function organist_review_setup() {

	load_theme_textdomain( 'organist-review', get_template_directory() . '/languages' );
	
	add_theme_support( 'automatic-feed-links' );
	
	add_theme_support( 'title-tag' );
	
	add_theme_support( 'post-thumbnails' );
	
	add_theme_support( 'woocommerce' );
	
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'organist-review' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption'
	) );

	add_theme_support( 'custom-background', apply_filters( 'organist_review_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_image_size( 'product-image-size', 253, 300, true );
	add_image_size( 'featured-mag', 395, 600 );
}


add_action( 'after_setup_theme', 'organist_review_content_width', 0 );
function organist_review_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'organist_review_content_width', 640 );
}


function get_options(){

	global $organist_opt;

	$org_opt = $organist_opt;

	$placeholdit = 'https://placeholdit.imgix.net/~text?txtsize=18&txt=Logo&w=146&h=70';

	$org_opt[ 'logo' ] = empty( $organist_opt['header_logo']['url'] ) ? $placeholdit : esc_attr( $organist_opt['header_logo']['url'] );


	$placeholdit = 'https://placeholdit.imgix.net/~text?txtsize=38&txt=Banner&w=700&h=200';

	$org_opt[ 'banner_image' ] = empty( $organist_opt['banner_image']['url'] ) ? $placeholdit : esc_attr( $organist_opt['banner_image']['url'] );

	return $org_opt;
}


add_filter( 'post_class', 'add_classes_to_post' );
function organist_maybe_add_category_args( $args, $category, $operator ) {

	if ( ! empty( $category ) ) {

		$args['tax_query'] = array(

			array(

				'taxonomy' => 'product_cat',

				'terms'    => array_map( 'sanitize_title', explode( ',', $category ) ),

				'field'    => 'slug',

				'operator' => $operator

			)

		);

	}



	return $args;
}


function add_classes_to_post($classes){

	global $filter_class;

	global $is_related_product;

	/* add isotopes filter class */

	if( isset( $filter_class ) || is_shop() || is_product_category() ){

		$classes = array_merge($classes,array( $filter_class, 'col-sm-6', 'col-md-3', 'col-xs-12', 'product' ));

	}
	/* modify class in related product */

	if( isset( $is_related_product ) ){

		array_push( $classes, 'col-sm-6','col-md-3','col-xs-12' );

	}

	return $classes;
}

add_filter( 'widget_nav_menu_args', 'alter_widget_nav_classes',10,1 );
function alter_widget_nav_classes($nav_menu_args){

	$nav_menu_args['menu_class'] = 'list-unstyled';

	return $nav_menu_args;

}

function get_total_subscriber(){ return 8000; }

function subscribe_to_mailchimp( $data ){
	if( isset( $_POST[ 'or_subscribe_to_mailchimp' ] ) || isset($_POST['data']['or_subscribe_to_mailchimp']) ){
		ORG_MAILCHIMP()->subscribe($data);
	}
}
add_action( 'woocommerce_review_order_before_submit', 'or_subscribe_to_mailchimp' );
add_action( 'or_mailchimp_subscription', 'or_subscribe_to_mailchimp' );
function or_subscribe_to_mailchimp(){
	?>
	<p class="form-row" >
		<input id="or-subscribe-to-mail" type="checkbox" name="or_subscribe_to_mailchimp" >
		<label for="or-subscribe-to-mail">
			<?php 
				echo esc_html__( 'I would like to subscribe to the IAO newsletter', 'organist-review' ); 
			?>
		</label>
	</p>
	<?php
}

add_action( 'woocommerce_checkout_order_processed', 'or_process_mailchimp_subscription', 11, 3 );
function or_process_mailchimp_subscription( $order_id, $posted_data, $order  ){
	subscribe_to_mailchimp( array(
		'user_email'    => $posted_data[ 'billing_email' ],
		'FNAME' => $posted_data[ 'billing_first_name' ],
		'LNAME'  => $posted_data[ 'billing_last_name' ],
	));
}

add_action( 'wp_head', 'or_stripe_enqueue' );
function or_stripe_enqueue(){
	?>
	<script src="https://js.stripe.com/v3/"></script>	
	<?php
}

require __DIR__ . '/vendor/autoload.php';

/*  Custom template tags for this theme. */
require __DIR__ . '/inc/template-tags.php';

/* Custom functions that act independently of the theme templates.*/
require __DIR__. '/inc/extras.php';

/* Custom Widgets */
require __DIR__. '/inc/widgets.php';

/* Custom Post Types */
require __DIR__. '/inc/post-types.php';


/* Siderbars */
require __DIR__. '/inc/sidebars.php';

/* Woocommerce Functions */
require __DIR__. '/inc/wc-functions.php';



/* Rest endpoints for Cron services and Webhook for subscription module */
require __DIR__. '/inc/rest-endpoints.php';

/* Sub Module that handles all the email for subscription*/
require __DIR__. '/inc/emails.php';

/* API wrapper for Mailchimp */
require __DIR__. '/inc/mailchimp/mailchimp.php';

/* Module to Import users from CSV */
require __DIR__. '/inc/csv-user-import/loader.php';


/* Admin Module For Users */
require __DIR__. '/inc/admin-users.php';

/* Enqueue scripts and styles. */
require __DIR__. '/inc/script-loader.php';

/* Favicons */
require __DIR__. '/inc/favicon.php';

/* Developers */
require __DIR__. '/inc/developer.php';

/* Log data */
require __DIR__. '/inc/log.php';

/* Implement the Redux framework for settings */
require __DIR__ . '/admin/config.php';

/* Subscription module */
require __DIR__. '/inc/subscribe.php';

/*$org = new ORGANIST_MAILCHIMP('01ed7be15923f8e091bc1918b047ed03-us12');

$org->get_campaign();*/