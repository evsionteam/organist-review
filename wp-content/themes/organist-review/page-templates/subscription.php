<?php
/*
  Template Name: Subscription page
 */
  get_header();

?>
  
<div class="title-breadcrumbs">
  <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
  <?php woocommerce_breadcrumb(); ?>
</div>

<?php
  global $organist_opt;
  $users_title = $organist_opt['users_title'];
  $society_membership_price = $organist_opt['society_membership_price'];

  extract(organists_get_wc_details());
  
  $countries  = WC()->countries->get_allowed_countries();
  $months     = or_get_months();
  $years      = or_get_years();

?>

<div class="or-inner-page or-subscribe">
  <div class="container form-container">
    <?php if( is_organists_subscriber() ): ?>
      
      <div class="or-notice">
        <span class="notice-icon"><i class="fa fa-info-circle"></i></span>
        <strong>INFO !</strong><?php _e( sprintf('You are already a subscriber. You can visit your dashboard %1shere%2s','<a href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'">','</a>'),'organist'); ?>
      </div>

    <?php else: ?>
      
      <?php if(!function_exists('WC')): ?>

        <div><?php _e('Looks like shopping feature is not enabled in the site.','organist'); ?></div>

      <?php else: ?>

        <form id="or-subscribe">

            <h1><?php _e('Annual paper subscription','organist'); ?></h1>
            <p><?php _e('Please select your subscription package from the carousel below','organist'); ?></p>

            <div class="or-packages or-block">
              <label class="or-title hidden"><?php _e('Annual Subscription','organist'); ?></label>
              <div class="row">
                <div class="or-package-slider" id="slider">

                <?php
                  $subscription_plans = organists_get_plans();
                  if($subscription_plans){
                    $index = 0;
                    
                    usort($subscription_plans, function($a, $b){
                      
                      if( $a['metadata']['order'] == $b['metadata']['order'] ){
                        return 0;
                      }

                      return ($a['metadata']['order'] < $b['metadata']['order'] )? -1 : 1;

                    });

                    foreach($subscription_plans as $plan):
                      $price = wc_price($plan['amount']/100);
                      $text = $plan['nickname'].' ( '.$price.' )';
                      $id = $plan['id'];

                    ?>
                    <div class="or-package">
                      <div class="or-custom-radio">
                        <input type="radio" id="<?php echo $id ?>" data-price="<?php echo ($plan['amount']/100); ?>" value="<?php echo $id ?>" name="subscription_package" <?php echo ($index==0)?'checked':''; ?> />
                        <label for="<?php echo $id ?>">
                          <span class="or-package-name">
                            <span class="text"><?php echo $text; ?></span>
                          </span>
                          <span class="or-package-price"> <?php echo $price; ?></span>
                        </label>
                      </div>
                    </div>
                    <?php
                      $index++;
                    endforeach;
                  }
                 ?>
               </div>

              </div><!-- /.row -->
            </div> <!-- /.or-cgap -->

            <div class="or-notice">
               <span class="notice-icon"><i class="fa fa-info-circle"></i></span>
               <strong>INFO !</strong> <?php _e(sprintf('Customers who subscribe today will start their subscription with the <strong style="margin-right: 0;">%s</strong> issue',get_next_issue()),'organist');
               ?>
            </div>

            

            <div class="or-coupon">
              <?php _e(sprintf('Have a coupon? %1sClick here to enter your code%2s','<a href="#" id="reveal-coupon" >','</a>'),'woocommerce'); ?>
              <div class="coupon-block" style="display:none;">
               <input type="text" name="coupon_code" class="input-text" placeholder="<?php _e('Coupon code','woocommerce'); ?>" id="coupon_code" value=""><button type="button" class="button c-btn apply-coupon" data-after-text = "<?php _e('Coupon Applied','organist'); ?>" ><?php _e('Apply coupon','woocommerce'); ?></button>
               <input type="hidden" name="coupon_value" value="" data-coupon-id="" />
              </div>
            </div>

            <div class="or-membership-detail">
              <div class="row">
                <div class="col-md-4">
                  <label class="or-title">
                    <a href="<?php echo esc_url( 'http://iao.org.uk/grants/the-iao-benevolent-fund/' ); ?>" target="_blank">
                      <?php _e('Donation To The IAOBF (Optional)','organist'); ?>
                    </a>
                  </label>
                  <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span> 
                  <input type="number" id="additional-donation" min="0" name="additional_donation" class="or-form-control" />
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  
                  <div class="or-add-donation-info">
                    <?php echo $organist_opt[ 'or_addtional_donation_info' ]; ?>
                  </div>
                  <div class="clearfix"></div>
                  <div class="or-title or-radio-default">
                    <?php
                        if( !$gift_aid_donation ){
                            $gift_aid_donation = 'yes';
                        }
                    ?>
                   <label class="or-custom-radio-new">
                     <?php _e( '<b>Yes</b>, treat my donations as a Gift Aid donation.', 'organist' ); ?>
                     <input type="checkbox" id="gift-aid-donation-yes" value="yes" name="gift_aid_donation" 
                     <?php echo $gift_aid_donation == 'yes' ? esc_html__( 'checked', 'organist' ) : ''; ?>  />
                     <span class="or-radio-checkmark"></span>
                   </label>
                  </div>
                </div>
              </div>
              <div class="row">
                
                <div class="col-md-4 or-block hidden">
                  <label class=" or-title"><?php _e('Local association','organist'); ?></label>
                  <input type="text" name="local_association" class="or-form-control" />
                </div>
                <div class="col-md-4 or-block hidden">
                  <div class=" or-multiple-custom-radio or-title">
                    <label class=" or-title"><?php _e('IAO Central society membership','organist'); ?></label>
                    <div class="or-input">
                      <input type="radio" id="yes" value="<?php echo $society_membership_price; ?>" name="society_membership" data-price="<?php echo $society_membership_price; ?>" />
                      <label for="yes"><?php _e( sprintf( 'Yes ( + %s )',wc_price($society_membership_price) ),'organist'); ?></label>
                      <input type="radio" id="no" value="" name="society_membership" checked />
                      <label for="no">no</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="or-membership-detail">
             

              <div class="row billing-wrapper form-fields-container" >
                <div class="col-md-12"><h3>Billing Details</h3></div>
                 
                <div class="col-md-2 or-block">
                  <label class=" or-title"><?php _e('Title','organist'); ?></label>
                  <select name="billing_title" class="or-form-control" style="width: 100% !important;">
                    <?php foreach($users_title as $t): ?>
                      <option value='<?php echo $t; ?>' <?php selected($title,$t); ?> ><?php echo $t; ?></option>
                    <?php endforeach; ?>
                   </select>
                </div>

                <div class="col-md-5 or-block">
                  <label class=" or-title"><?php _e('First Name','organist'); ?></label>
                  <input type="text" name="billing_first_name" class="or-form-control" value="<?php echo $billing_first_name; ?>"/>
                </div>

                <div class="col-md-5 or-block">
                  <label class=" or-title"><?php _e('Last Name','organist'); ?></label>
                  <input type="text" name="billing_last_name" class="or-form-control" value="<?php echo $billing_last_name; ?>" />
                </div>

                <div class="clearfix visible-md visible-lg"></div>
                
                <div class="col-md-6 or-block">
                  <label class=" or-title"><?php _e('Email address','organist'); ?></label>
                  <input type="email" name="billing_email" class="or-form-control" required value="<?php echo $billing_email; ?>" />
                </div>

                <div class="col-md-6 or-block">
                  <label class=" or-title"><?php _e('Confirm Email address','organist'); ?></label>
                  <input type="email" name="billing_confirm_email" class="or-form-control" required value="<?php echo $billing_confirm_email; ?>" />
                </div>

                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Telephone No','organist'); ?></label>
                  <input type="tel" name="billing_phone" class="or-form-control" value="<?php echo $billing_phone; ?>" />
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Address 1','organist'); ?></label>
                  <input type="text" name="billing_address_1" class="or-form-control" value="<?php echo $billing_address_1; ?>" />
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Address 2','organist'); ?></label>
                  <input type="text" name="billing_address_2" class="or-form-control" value="<?php echo $billing_address_2; ?>" />
                </div>

                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Country','organist'); ?></label>
                  <select name="billing_country" class="or-form-control" data-type="billing" >
                    <option value="" ><?php _e( 'Select Country', 'woocommerce' ); ?></option>
                    <?php foreach ( $countries as $ckey => $cvalue ): ?>
                      <option value="<?php echo esc_attr( $ckey ); ?>" <?php selected($billing_country,$ckey); ?> ><?php _e( $cvalue, 'woocommerce' ); ?></option>
                    <?php endforeach;  ?>
                  </select>
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('County/State','organist'); ?></label>
                  <div class="state-container" >
                    <p><?php _e('Select country first','woocommerce'); ?></p>
                  </div>
                </div>

                <div class="col-md-2 or-block">
                  <label class=" or-title"><?php _e('Town / City','organist'); ?></label>
                  <input type="text" name="billing_city" class="or-form-control" value="<?php echo $billing_city; ?>" />
                </div>

                <div class="col-md-2 or-block">
                  <label class=" or-title"><?php _e('Post code','organist'); ?></label>
                  <input type="text" name="billing_postcode" class="or-form-control" value="<?php echo $billing_postcode; ?>" />
                </div>

              </div>  <!-- End .billing-wrappe -->

              <br />
              <div class="row shipping-wrapper form-fields-container" >
                <div class="col-md-12"><h3>Delivery Details</h3></div>

                <div class="or-c-check col-md-4 or-block">
                  <input type="checkbox" id="copy_from_billing" value="" />
                  <label for="copy_from_billing"><?php _e('Copy data from billing details','organist'); ?></label>
                </div>

                <div class="col-md-1 or-block">
                  <label class=" or-title"><?php _e('Title','organist'); ?></label>
                  <select name="shipping_title" class="or-form-control" style="width: 100% !important;" >
                    <?php foreach($users_title as $t): ?>
                      <option value='<?php echo $t; ?>' <?php selected($title,$t); ?> ><?php echo $t; ?></option>
                    <?php endforeach; ?>
                   </select>
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('First Name','organist'); ?></label>
                  <input type="text" name="shipping_first_name" class="or-form-control" value="<?php echo $shipping_first_name; ?>" />
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Last Name','organist'); ?></label>
                  <input type="text" name="shipping_last_name" class="or-form-control" value="<?php echo $shipping_last_name; ?>" />
                </div>
                

                <div class="col-md-3 or-block">
                  <label class=" or-title"><?php _e('Email address','organist'); ?></label>
                  <input type="email" name="shipping_email" class="or-form-control" value="<?php echo $shipping_email; ?>" />
                </div>
                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Telephone No','organist'); ?></label>
                  <input type="tel" name="shipping_phone" class="or-form-control" value="<?php echo $shipping_phone; ?>" />
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Address 1','organist'); ?></label>
                  <input type="text" name="shipping_address_1" class="or-form-control" value="<?php echo $shipping_address_1; ?>" />
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Address 2','organist'); ?></label>
                  <input type="text" name="shipping_address_2" class="or-form-control" value="<?php echo $shipping_address_2; ?>" />
                </div>

                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Country','organist'); ?></label>
                  <select name="shipping_country" class="or-form-control" data-type="shipping" >
                    <option value="" ><?php _e( 'Select Country', 'woocommerce' ); ?></option>
                    <?php foreach ( $countries as $ckey => $cvalue ): ?>
                      <option value="<?php echo esc_attr( $ckey ); ?>" <?php selected($shipping_country,$ckey); ?> ><?php _e( $cvalue, 'woocommerce' ); ?></option>
                    <?php endforeach;  ?>
                  </select>
                </div>

                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('County/State','organist'); ?></label>
                  <div class="state-container" >
                    <p><?php _e('Select country first','woocommerce'); ?></p>
                  </div>
                </div>

                <div class="col-md-2 or-block">
                  <label class=" or-title"><?php _e('Town / City','organist'); ?></label>
                  <input type="text" name="shipping_city" class="or-form-control" value="<?php echo $shipping_city; ?>" />
                </div>

                <div class="col-md-2 or-block">
                  <label class=" or-title"><?php _e('Post code','organist'); ?></label>
                  <input type="text" name="shipping_postcode" class="or-form-control" value="<?php echo $shipping_postcode; ?>" />
                </div>
              </div>
            </div>

            <div class="or-cgap">
              <h3><?php _e('Card Details','organist'); ?></h3>

              <div class="row">
                <div class="col-md-4 or-block">
                    <label class=" or-title"><?php _e('Card Number','organist'); ?></label>
                    <input id="cvv" class="or-form-control input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="•••• •••• •••• ••••">
                </div>
                 <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Expiration Date','organist'); ?></label>
                  <div class="expiry-period">
                    <select class="or-form-control" id="card-expiry-month" required>
                      <?php foreach( $months as $val => $text ): ?>
                        <option value="<?php echo $val; ?>" ><?php echo $text; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <select class="or-form-control" id="card-expiry-year" required>
                      <?php foreach( $years as $val => $text ): ?>
                        <option value="<?php echo $val; ?>" ><?php echo $text; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('CVC Number','organist'); ?></label>
                  <input id="cvc" class="or-form-control input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="<?php _e('CVC','organists'); ?>" required />
                </div>
                <div class="clearfix visible-md visible-lg"></div>
                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Issue Number','organist'); ?></label>
                  <input type="number" name="issue_number" class="or-form-control" />
                </div>
                <div class="col-md-4 or-block">
                  <label class=" or-title"><?php _e('Special Instructions or Order Comments','organist'); ?></label>
                  <textarea type="text" name="special_comment" class="or-form-control"></textarea>
                </div>
              </div>
            </div>

            <?php
              do_action( 'or_mailchimp_subscription' );
              wc_get_template( 'checkout/terms.php' );
            ?>

            <div class="or-next">
              <button type="submit" name="submit_cart" disabled><span class="or-next-text">next step</span><span class="or-arrow"><i class="fa fa-arrow-right"></i></span></button>
            </div>
        </form>

        <!-- Modal -->
        <script type="text/template" id="order-template" >
          <table class="total or-payment-detail" >
            <tr id="or-plan-charge" class="hidden" >
              <th><?php _e('Annual Subscription','organist'); ?></th>
              <td></td>
            </tr>

            <tr id="or-discount" class="hidden" >
              <th><?php _e('Discount','organist'); ?></th>
              <td></td>
            </tr>

            <tr id="or-subtotal" class="hidden" >
              <th><?php _e('Subtotal','organist'); ?></th>
              <td></td>
            </tr>

            <tr id="or-additional-charge" class="hidden">
              <th><?php _e('One-off additional cost(s)','organist'); ?></th>
              <td></td>
            </tr>

            <tr id="or-total" class="or-total-amount hidden">
              <th><?php _e('Total Amount','organist'); ?></th>
              <td></td>
            </tr>
          </table>
        </script>
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>

<?php get_footer(); 
