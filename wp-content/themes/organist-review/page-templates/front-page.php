<?php
/*
  Template Name: Home page
 */
get_header();
$org_opt = get_options();
$assets_url = get_assets_url();

?>

<!-- :::::::::::::::::::::::::::::::::::::::::::::
            BANNER WRAPPER
::::::::::::::::::::::::::::::::::::::::::::::::: -->
<section class="banner-wrapper">
    <div class="container">
        <div class="row">
            <article class="site-banner">
                <div class="col-sm-6">
                    <div class="banner-text-holder">
                        <h3 class="title"><?php echo $org_opt['banner_title']; ?></h3>
                        <p class="desc"><?php echo esc_attr($org_opt['banner_description']); ?></p>
                        <div class="banner-btn-holder">
                            <a class="btn btn-default c-btn" href="<?php echo get_permalink( wc_get_page_id ( 'shop' ) ); ?>" alt="Subscribe Button">
                                <?php _e( 'Shop Now' ); ?> <i class="fa fa-shopping-basket"></i>
                            </a>

                        </div><!-- banner-btn-holder -->
                    </div><!-- banner-text-holder -->
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                    <img class="img-responsive" alt="<?php _e('Site\'s Banner', 'organist'); ?>" src="<?php echo $org_opt['banner_image']; ?>" />
                </div><!-- col-sm-6 -->
            </article><!-- /banner-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.banner-wrapper -->


<!-- :::::::::::::::::::::::::::::::::::::::::::::
            SUBSCRIPTION WRAPPER
::::::::::::::::::::::::::::::::::::::::::::::::: -->
<section class="subscription-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <img src="<?php echo $assets_url . '/img/subscribe.png'; ?>" class="img-responsive" alt="" />
            </div><!-- col-sm-2 -->
            <div class="col-sm-7">
                <div class="subs-title"><?php echo $org_opt['subscribe_title']; ?></div>
                <p class="subs-desc"><?php echo $org_opt['subscribe_description']; ?></p>
            </div><!-- col-sm-7 -->
            <div class="col-sm-3">
                <div class="subscription-btn-holder">
                    <a href="<?php echo $org_opt['subscribe_btn_link']; ?>" class="btn btn-default subs-btn c-btn" alt="<?php _e('Subscribe Button', 'organist'); ?>">
                        <?php echo $org_opt['subscribe_btn_text']; ?>
                    </a>
                </div><!-- subscription-btn-holder -->
            </div><!-- col-sm-3 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- subscription-wrapper -->


<!-- ::::::::::::::::::::::::::::::::::::::::::::
            LATEST ISSUE BLOCK
::::::::::::::::::::::::::::::::::::::::::::::::: -->
<section class="latest-issue-wrapper">
    <div class="container">
        <div class="block-title">
            <h1 class="title"></span><?php echo $org_opt['issue_title']; ?></h1>
            <p class="sub-title"><?php echo $org_opt['issue_sub_title']; ?></p>
        </div><!-- block-title -->
        <?php
            if($org_opt['issue_image']){
                $issue_img = wp_get_attachment_image_src($org_opt['issue_image']['id'], 'featured-mag');
                
                if(!$issue_img){
                    $issue_img = array( 'http://via.placeholder.com/395x550', 395, 550, 1 );
                }
            }else{
                $issue_img = array( 'http://via.placeholder.com/395x550', 395, 550, 1 );
            }
        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="issue-block">
                    <div class="new-image">
                        <img width="<?php echo $issue_img[1]; ?>" height="<?php echo $issue_img[2]; ?>" style="max-height: 600px;" 
                            src="<?php echo $issue_img[0]; ?>" 
                            alt="business" class="img-responsive" />
                    </div><!-- new-image -->
                    <div class="new-tag">
                        <span>New</span>
                    </div><!-- new-tag -->
                </div><!-- issue-block -->
            </div><!-- col-sm-6 -->
            <div class="col-sm-6">
                <div class="latest-issue-content">
                    <h2 class="title hidden">Guest editor Richard Hills</h2>
                    <p class="desc"><?php echo $org_opt['issue_description']; ?></p>
                    <div class="social-share">
                        <span class="price hidden">$30</span>
                        <div class="share-links">
                            <span>Share with</span>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($org_opt['subscribe_btn_link']); ?>"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/home?status=<?php echo urlencode($org_opt['subscribe_btn_link']); ?>"><i class="fa fa-twitter"></i></a>
                            <a href="https://plus.google.com/share?url=<?php echo urlencode($org_opt['subscribe_btn_link']); ?>"><i class="fa fa-google-plus"></i></a>
                            <!-- 
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a> -->
                        </div><!-- share-links -->
                    </div><!-- social-share -->
                    <div class="latest-subscrib">
                        <a href="<?php echo $org_opt['subscribe_btn_link']; ?>" 
                           class="btn btn-default c-btn" 
                           alt="<?php _e('Subscribe Button', 'organist'); ?>">
                               <?php echo $org_opt['subscribe_btn_text']; ?>
                        </a>
                    </div><!-- div -->
                </div><!-- latest-issue-content -->
            </div><!-- col-sm-6 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- latest-issue-wrapper -->


<!-- :::::::::::::::::::::::::::::::::::::::::::::
            PRODUCT LISTING
::::::::::::::::::::::::::::::::::::::::::::::::: -->
<section  id="organist-product-list" class="product-list-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="block-title">
                    <h1 class="title"><?php echo $org_opt['product_title']; ?> </h1>
                    <p class="sub-title"><?php echo $org_opt['product_sub_title']; ?></p>
                </div>
            </div>
            <!-- <div class="block-heading clearfix"> -->
            <div class="col-sm-4">
                <div class="pl-tabs">
                    <button class="filter-button-group active" data-filter="li.recent_products">
                        <?php _e('Latest', 'organist'); ?>
                    </button>
                    <button class="filter-button-group btn-two" data-filter="li.best_selling_products">
                        <?php _e('Popular', 'organist'); ?>
                    </button>
                </div><!-- pl-tabs -->	
            </div><!-- col-sm-4 -->

        </div><!-- row -->
       
        <div class="woocommerce">
            <ul class="products grid row" style="padding: 0;list-style: none;">
                <?php
                //Latest Product
                $atts = array(
                    'per_page' => '12',
                    'columns' => '4',
                    'orderby' => 'date',
                    'order' => 'desc',
                    'category' => '', // Slugs
                    'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                );

                $query_args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => $atts['per_page'],
                    'orderby' => $atts['orderby'],
                    'order' => $atts['order'],
                    'meta_query' => WC()->query->get_meta_query()
                );

                $query_args = organist_maybe_add_category_args($query_args, $atts['category'], $atts['operator']);

                echo organist_product_loop($query_args, $atts, 'recent_products');

                //Popular Product
                $atts = array(
                    'per_page' => '12',
                    'columns' => '4',
                    'category' => '', // Slugs
                    'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                );

                $query_args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => $atts['per_page'],
                    'meta_key' => 'total_sales',
                    'orderby' => 'meta_value_num',
                    'meta_query' => WC()->query->get_meta_query()
                );

                echo organist_product_loop($query_args, $atts, 'best_selling_products');
                ?>
            </ul>
        </div><!-- woocommerce -->
      </div><!-- container -->
</section><!-- /product-list-wrapper -->


<!-- :::::::::::::::::::::::::::::::::::::::::::::
           SUBSCRIBER RIVIEW
::::::::::::::::::::::::::::::::::::::::::::::::: -->

<section  class="overflowed-bg">
    <div id="subscription" class="subscribe-wrap" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/src/img/subscribe-review-.jpg');">
        <div class="container">
            <div class="row equal-height">
                <div class="col-sm-6 equal-col">
                    <div class="advert-wrapper">
                        <div class="review-content">
                            <h3 class="title"><?php echo $org_opt['af-title']; ?></h3>
                            <p class="desc"><?php echo $org_opt['af-desc']; ?></p>
                        </div><!-- review-content -->
                    </div><!-- advert-wrapper -->	
                </div><!-- col-sm-6 equal-col -->
                <?php
                    $total_subscriber = get_total_subscriber();
                ?>
                <div class="col-sm-6 equal-col">
                    <div class="subscribe-wrapper">
                        <div id="happy-subscriber-pie" class="pie-title-center" data-percent="80">
                            <span class="pie-value" data-total="<?php echo $total_subscriber; ?>">0</span>
                        </div><!-- pie-title-center -->
                        <p><?php _e('HAPPY READERS', 'organist'); ?><p>

                    </div><!-- subscribe-wrapper -->
                </div><!-- col-sm-6 equal-col -->
            </div><!-- row equal-height -->
        </div><!-- container -->
    </div><!-- subscribe-wrap -->	
</section><!-- /overflowed-bg -->
<?php
get_footer();
