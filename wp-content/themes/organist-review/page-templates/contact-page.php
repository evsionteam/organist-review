<?php
/*
  Template Name: Contact page
*/
	get_header();
	$org_opt = get_options();
	the_post();
?> 

<div class="title-breadcrumbs">
	<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	<?php woocommerce_breadcrumb(); ?>
</div>

<section class="contact-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<?php the_content(); ?>
			</div>
			<div class="col-sm-6">
				<?php echo do_shortcode( $org_opt['contact_form']); ?>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
