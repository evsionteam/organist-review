<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $organist_opt;
$users_title = $organist_opt['users_title'];

 $current_user = wp_get_current_user();

$user_id = $user->ID;
// print_r( $user ); die;
$title = get_user_meta( $user_id, 'billing_title', true );
$billing_phone = get_user_meta( $user_id, 'billing_phone', true );
$billing_address_1 = get_user_meta( $user_id, 'billing_address_1', true );
$billing_address_2 = get_user_meta( $user_id, 'billing_address_2', true );
$billing_country = get_user_meta( $user_id, 'billing_country', true );
$billing_state = get_user_meta( $user_id, 'billing_state', true );
$billing_city = get_user_meta( $user_id, 'billing_city', true );
$billing_postcode = get_user_meta( $user_id, 'billing_postcode', true );
// $display_name = get_user_meta( $user_id, 'billing_postcode', true );

$countries  = WC()->countries->get_allowed_countries();

do_action( 'woocommerce_before_edit_account_form' ); ?>
<div class="or-notice">
	<span class="notice-icon"><i class="fa fa-info-circle"></i></span>
    <strong>INFO !</strong><?php _e( 'Please note, that if you wish to change the address for your subscription, you need to visit the <em>addresses</em> tab and change your delivery address there','organist'); ?>
</div>
<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	<div class="row form-fields-container">
		<div class="col-md-8">
			<div class="or-user-info">
				<div class="row">
					<div class="or-form-grounp col-md-3">
						<p class="woocommerce-FormRow woocommerce-FormRow--first">
							<label for="account_title"><?php _e( 'Title', 'woocommerce' ); ?></label>
							<select name="account_title" id="account_title" >
								<option><?php _e( 'Choose Title', 'woocommerce' ); ?></option>
								<?php foreach($users_title as $t): ?>
				                	<option value='<?php echo $t; ?>' <?php selected( $title, $t ); ?>><?php echo $t; ?></option>
				                <?php endforeach; ?>
							</select>
						</p>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6">
								<p class="woocommerce-FormRow woocommerce-FormRow--first">
									<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
								</p>
							</div>

							<div class="col-md-6">
								<p class="woocommerce-FormRow woocommerce-FormRow--last">

									<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
									<input type="hidden" name="account_display_name" id="account_display_name" value="<?php echo esc_attr($user->first_name ); ?>" />
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_email"><?php _e( 'Email address', 'organist' ); ?> <span class="required">*</span></label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
						</p>
					</div>

					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_phone"><?php _e( 'Phone Number', 'organist' ); ?></label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_phone" id="account_phone" value="<?php echo esc_attr( $billing_phone ); ?>" />
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_country"><?php _e( 'Country', 'organist' ); ?> </label>
							<select id="account_country" name="billing_country" data-type="billing" class="or-form-control woocommerce-Input" data-type="billing" >
			                	<option value="" ><?php _e( 'Select Country', 'woocommerce' ); ?></option>
			                  	<?php foreach ( $countries as $ckey => $cvalue ): ?>
			                    	<option value="<?php echo esc_attr( $ckey ); ?>" <?php selected( $billing_country, $ckey ); ?> ><?php _e( $cvalue, 'woocommerce' ); ?></option>
			                  	<?php endforeach;  ?>
			                </select>
						</p>
					</div>

					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label class=" or-title"><?php _e('County/State','organist'); ?></label>
							<div class="state-container" >
								<?php echo $billing_state; ?>
							</div>
						</p>
		            </div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_address_1"><?php _e( 'Address 1', 'organist' ); ?> </label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_address_1" id="account_address_1" value="<?php echo esc_attr( $billing_address_1 ); ?>" />
						</p>
					</div>

					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_address_2"><?php _e( 'Address 2', 'organist' ); ?> </label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_address_2" id="account_address_2" value="<?php echo esc_attr( $billing_address_2 ); ?>" />
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_city"><?php _e( 'City/Town', 'organist' ); ?> </label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_city" id="account_city" value="<?php echo esc_attr( $billing_city ); ?>" />
						</p>
					</div>

					<div class="col-md-6">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide">
							<label for="account_postcode"><?php _e( 'Zip/Postcode', 'organist' ); ?> </label>
							<input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_postcode" id="account_postcode" value="<?php echo esc_attr( $billing_postcode ); ?>" />
						</p>
					</div>
				</div>

				<?php do_action( 'woocommerce_edit_account_form' ); ?>
				
			</div>
		</div>

		<div class="col-md-4">
			<div class="or-user-info password-change">
				<h4><?php _e( 'Password Change', 'woocommerce' ); ?></h4>
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					<label for="password_current"><?php _e( 'Current Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />
				</p>
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					<label for="password_1"><?php _e( 'New Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />
				</p>
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					<label for="password_2"><?php _e( 'Confirm New Password', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />
				</p>
			</div>

			<p>
				<?php wp_nonce_field( 'save_account_details' ); ?>

				<input type="submit" class="woocommerce-Button button c-btn" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
				<input type="hidden" name="action" value="save_account_details" />
			</p>
	
		</div>
	</div>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
