<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' => __( 'Billing Address', 'woocommerce' ),
		'shipping' => __( 'Shipping Address', 'woocommerce' )
	), $customer_id );
} else {
	$get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
		'billing' =>  __( 'Billing Address', 'woocommerce' )
	), $customer_id );
}

$oldcol = 1;
$col    = 1;
?>

<p>
	<?php echo apply_filters( 'woocommerce_my_account_my_address_description', __( 'The following addresses will be used for your postal subscription', 'woocommerce' ) ); ?>
</p>
<div class="row">
	<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) echo '<div class="u-columns woocommerce-Addresses col2-set addresses">'; ?>

	<?php foreach ( $get_addresses as $name => $title ) : ?>

		<div class="u-column<?php echo ( ( $col = $col * -1 ) < 0 ) ? 1 : 2; ?> col-<?php echo ( ( $oldcol = $oldcol * -1 ) < 0 ) ? 1 : 2; ?> woocommerce-Address col-md-6">
			<header class="woocommerce-Address-title title">
				<h3><?php echo $title; ?> <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?>" class="edit"><i class="fa fa-pencil-square-o" title="<?php _e( 'Edit', 'woocommerce' ); ?>" ></i></a></h3>
				
			</header>
			<?php
				$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
					'first_name'  => get_user_meta( $customer_id, $name . '_first_name', true ),
					'last_name'   => get_user_meta( $customer_id, $name . '_last_name', true ),
					'company'     => get_user_meta( $customer_id, $name . '_company', true ),
					'address_1'   => get_user_meta( $customer_id, $name . '_address_1', true ),
					'address_2'   => get_user_meta( $customer_id, $name . '_address_2', true ),
					'city'        => get_user_meta( $customer_id, $name . '_city', true ),
					'state'       => get_user_meta( $customer_id, $name . '_state', true ),
					'postcode'    => get_user_meta( $customer_id, $name . '_postcode', true ),
					'country'     => get_user_meta( $customer_id, $name . '_country', true )
				), $customer_id, $name );

				$formatted_address = WC()->countries->get_formatted_address( $address );
			?>

			<div class="woocommerce-address-details">
				<?php  if ( ! $formatted_address ): ?>
					<p class="not-found"><?php _e( 'You have not set up this type of address yet.', 'woocommerce' ); ?></p>
				<?php else: ?>
					<table class="table shop_table shop_table_responsive my_account_orders account-orders-table">
						<tbody>
							<tr>
								<td><?php echo trim($address['first_name'].' '.$address['last_name']);  if($address['company']): ?> ( <?php echo $address['company']; ?> )<?php endif; ?></td>
							</tr>
							<?php
								$temp = array( $address['address_1'], $address['address_2'], $address['city'] );
								if($temp){ ?>
									<tr>
										<td><?php echo implode(', ',$temp); ?></td>
									</tr>
								<?php }
								unset($temp);
							?>

							<tr>
								<td>
									<?php 
										$full_state = ( $address['country'] && $address['state'] && isset( WC()->countries->states[ $address['country'] ][ $address['state'] ] ) ) ? WC()->countries->states[ $address['country'] ][ $address['state'] ] : $address['state'];

										$full_country = ( isset( WC()->countries->countries[ $address['country'] ] ) ) ? WC()->countries->countries[ $address['country'] ] : $address['country'];
										
										echo $full_state.' '.$address['postcode'].', '.$full_country; 
									?>
								</td>
							</tr>

						</tbody>
					</table>
				<?php endif; ?>
			</div>
			
			<p></p>
		</div>

	<?php endforeach; ?>
</div>
<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) echo '</div>'; ?>
