<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user_id = get_current_user_id();
$user = get_userdata( $user_id );

$userdata = get_user_meta( $user_id );

$userdata = array_map(function($val){
	return $val[0];
}, $userdata);

$plan = "";
if(isset($userdata['plan']) ){
	$plan = maybe_unserialize($userdata['plan']);
}
$countries = WC()->countries->get_allowed_countries();

$subscriber = array('active_subscriber','passive_subscriber');
$user_id = get_current_user_id();
$user = get_userdata( $user_id );
$base_path = get_template_directory();

$display_name = $current_user->display_name;
$is_subscriber = false;

if( in_array($subscriber[0], $user->roles) || in_array($subscriber[1], $user->roles) ){
	$is_subscriber = true;
	$display_name = $userdata['billing_title'].' '.$userdata['billing_last_name'];
}

?>

<?php
$is_inactive = true;
$is_manual = false;
$cancellation_proceeded = get_user_meta( $user_id, 'proceeded_cancellation', true );
?>
<?php
	if($cancellation_proceeded){ ?>
	<div class="or-notice">
		<span class="notice-icon"><i class="fa fa-info-circle"></i></span>
		<strong>INFO !</strong>
		<?php
			_e( sprintf('Auto-renew has been disabled, you will continue to receive magazines up until your renewal date (%s), after which you will not receive future issues.',date("d M, Y",$userdata['current_period_end']) ),'organist');
	 	?>
	</div>
<?php
		}
?>

<div class="or-message">
	<?php
		echo sprintf( esc_attr__( 'Hello %s%s%s (not %2$s? %sSign out%s)', 'woocommerce' ), '<strong>', esc_html( $display_name ), '</strong>', '<a href="' . esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) ) . '">', '</a>' );
		echo "<br />";
		echo sprintf( esc_attr__( 'From your account dashboard you can view your %1$srecent orders%2$s, manage your %3$sshipping and billing addresses%2$s and %4$sedit your password and account details%2$s.', 'woocommerce' ), '<a href="' . esc_url( wc_get_endpoint_url( 'orders' ) ) . '">', '</a>', '<a href="' . esc_url( wc_get_endpoint_url( 'edit-address' ) ) . '">', '<a href="' . esc_url( wc_get_endpoint_url( 'edit-account' ) ) . '">' );
	?>
</div>

<?php do_action( 'woocommerce_account_dashboard' ); ?>


<?php do_action( 'woocommerce_before_my_account' ); ?>
<?php

	if( in_array($subscriber[0], $user->roles) || in_array($subscriber[1], $user->roles) ){
		require_once $base_path.'/template-parts/dashboard-subscriber.php';
	}else{
		require_once $base_path.'/template-parts/dashboard-customer.php';
	}

?>
<?php do_action( 'woocommerce_after_my_account' ); ?>
