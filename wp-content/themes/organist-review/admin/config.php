<?php    
    if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxCore/framework.php' ) ) {
        require_once( dirname( __FILE__ ) . '/ReduxCore/framework.php' );
    }

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $option_name = "organist_opt";

    $args = array( 
      'menu_title' => 'Organist Options', 
      'page_title' => 'Organist Options', 
      'save_defaults' => true
    );

    Redux::setArgs ($option_name, $args);
    /* General Settings */

    Redux::setSection( $option_name, array(
        'title'      => __( 'Global Settings', 'organist' ),
        'id'         => 'general',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
            array(
                'id'       => 'site_title',
                'type'     => 'text',
                'title'    => __( 'Site Title', 'organist' ),
                'default'  => 'Organist - Online Shopping Portal'
            ),
            array(
                'id'       => 'header_logo',
                'type'     => 'media',
                'title'    => __( 'Header Logo', 'organist' )
            ),
            array(
                'id'       => 'fb_link',
                'type'     => 'text',
                'title'    => __( 'Facebook Link', 'organist' )
            ),array(
                'id'       => 'twit_link',
                'type'     => 'text',
                'title'    => __( 'Twitter Link', 'organist' ),
            ),array(
                'id'       => 'google_link',
                'type'     => 'text',
                'title'    => __( 'Google plus Link', 'organist' )
            ),array(
                'id'       => 'contact_form',
                'type'     => 'text',
                'title'    => __( 'cf7 ( shortcode )', 'organist' )
            )
        )
    ));

    Redux::setSection( $option_name, array(
           'title'      => __( 'Fav Icons', 'organist' ),
           'id'         => 'opt-favicon-subsection',
           'subsection' => true,
           'fields'     => array(
               array(
                   'id'       => 'favicon-16',
                   'type'     => 'media',
                   'title'    => __( 'Favicon (16x16)', 'organist' ),
               ),
               array(
                   'id'       => 'favicon-32',
                   'type'     => 'media',
                   'title'    => __( 'Favicon (32x32)', 'organist' ),
               ),
               array(
                   'id'       => 'favicon-96',
                   'type'     => 'media',
                   'title'    => __( 'Favicon (96x96)', 'organist' ),
               ),

               array(
                   'id'       => 'apple-icon-57',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (57x57)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-60',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (60x60)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-72',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (72x72)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-76',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (76x76)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-114',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (114x114)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-120',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (120x120)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-144',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (144x144)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-152',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (152x152)', 'organist' ),
               ),
               array(
                   'id'       => 'apple-icon-180',
                   'type'     => 'media',
                   'title'    => __( 'Apple Icon (180x180)', 'organist' ),
               ),
               array(
                   'id'       => 'android-icon-192',
                   'type'     => 'media',
                   'title'    => __( 'Android Icon (192x192)', 'organist' ),
               ),
               array(
                   'id'       => 'favicon-theme-color',
                   'type'     => 'color',
                   'title'    => __('Favicon Background Color', 'organist'),
                   'subtitle' => __('Pick a background color for Windows shortcut', 'organist'),
                   'default'  => '#FFFFFF',
               ),
               array(
                   'id'       => 'favicon-title-color',
                   'type'     => 'color',
                   'title'    => __('Favicon Title Color', 'organist'), 
                   'subtitle' => __('Pick a title color for Windows shortcut', 'organist'),
                   'default'  => '#333333',
               )
           )
    ));

    Redux::setSection( $option_name, array(
        'title'      => __( 'Banner Section', 'organist' ),
        'id'         => 'banner-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                    'id'    => 'banner_title',
                    'type'  => 'text',
                    'title' => __( 'Title', 'organist' )
                ),
                array(
                    'id'    => 'banner_description',
                    'type'  => 'textarea',
                    'title' => __( 'Description', 'organist' )
                ),
                array(
                    'id'    => 'banner_btn_text',
                    'type'  => 'text',
                    'title' => __( 'Button Text', 'organist' )
                ),array(
                    'id'    => 'banner_btn_link',
                    'type'  => 'text',
                    'title' => __( 'Button Link', 'organist' )
                ),array(
                    'id'    => 'banner_image',
                    'type'  => 'media',
                    'title' => __( 'Image', 'organist' )
                ),
            )
    ));

    $lists = array();
    //$campaign_list = array();
    
    $lists_list = ORG_MAILCHIMP()->get_list();
    //$campaigns = ORG_MAILCHIMP()->get_campaign();

    if(!is_wp_error( $lists_list )){
      foreach($lists_list as $list){
       
        $lists[$list['id']] = $list['name'];
      }
    }

    /*if(!is_wp_error( $campaigns )){
      foreach($campaigns as $campaign){
        $campaign_list[$campaign['id']] = $campaign['settings']['title'];
        
      }
    }*/

    Redux::setSection( $option_name, array(
        'title'      => __( 'Subscribe Section', 'organist' ),
        'id'         => 'subscribe-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                  'id'    => 'subscribe_title',
                  'type'  => 'text',
                  'title' => __( 'Title', 'organist' )
                ),
                array(
                  'id'    => 'subscribe_description',
                  'type'  => 'textarea',
                  'title' => __( 'Description', 'organist' )
                ),
                array(
                  'id'    => 'subscribe_btn_text',
                  'type'  => 'text',
                  'title' => __( 'Button Text', 'organist' )
                ),
                array(
                  'id'    => 'subscribe_btn_link',
                  'type'  => 'text',
                  'title' => __( 'Button Link', 'organist' )
                ),
                array(
                  'id'      => 'society_membership_price',
                  'type'    => 'text',
                  'title'   => __( 'Society Membership Price', 'organist' ),
                  'default' => 40
                ),
                array(
                  'id'      => 'stripe_publishable_key',
                  'type'    => 'text',
                  'title'   => __( 'Stripe Publishable Key', 'organist' ),
                  'default' => 'pk_test_yLqms23S5OsGCxv14kIyZfTP'
                ),
                array(
                  'id'      => 'stripe_secret_key',
                  'type'    => 'text',
                  'title'   => __( 'Stripe Secret Key', 'organist' ),
                  'default' => 'sk_test_dTOf3flKTEsHSctQFOngr2z3'
                ),
                array(
                  'id'    => 'or_addtional_donation_info',
                  'type'  => 'editor',
                  'title' => __( 'Additional Information( Donation )', 'organist' ),
                ),
                array(
                  'id'=>'users_title',
                  'type' => 'multi_text',
                  'title' => __('User Title', 'organist'),
                  'desc' => __('Eg: Mr', 'organist')
                ),
                array(
                  'id'       => 'or_inactive_list',
                  'type'     => 'select',
                  'title'    => __('Passive List', 'organist'), 
                  'desc'     => __('List for inactive user', 'organist'),                
                  'options'  => $lists
                ),
                array(
                  'id'       => 'or_active_list',
                  'type'     => 'select',
                  'title'    => __('Active List', 'organist'), 
                  'desc'     => __('List for active user', 'organist'),                
                  'options'  => $lists
                ),
                /*
                array(
                  'id'       => 'or_active_campaign',
                  'type'     => 'select',
                  'title'    => __('Active Campaign', 'organist'), 
                  'desc'     => __('Campaign to send for active user', 'organist'),                
                  'options'  => $campaign_list
                )*/
            )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Emails', 'organist' ),
      'id'         => 'email-section',
      'icon'       => 'el el-envelope',
      'fields'     => array()
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Subscription Created', 'organist' ),
      'id'         => 'payment-created-section',
      'icon'       => 'el el-envelope',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'    => 'payment_created_subject',
          'type'  => 'text',
          'title' => __('Subject', 'organist')
        ),

        array(
          'id'    => 'payment_created_title',
          'type'  => 'text',
          'title' => __('Title', 'organist')
        ),

        array(
          'id'    => 'payment_created_body',
          'type'  => 'editor',
          'title' => __('Body', 'organist'), 
          'args'  => array(
            'teeny'            => true,
            'textarea_rows'    => 10
          )
        )    
      )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Subscription Renewed', 'organist' ),
      'id'         => 'payment-renewed-section',
      'icon'       => 'el el-envelope',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'    => 'payment_renewed_subject',
          'type'  => 'text',
          'title' => __('Subject', 'organist')
        ),

        array(
          'id'    => 'payment_renewed_title',
          'type'  => 'text',
          'title' => __('Title', 'organist')
        ),

        array(
          'id'    => 'payment_renewed_body',
          'type'  => 'editor',
          'title' => __('Body', 'organist'), 
          'args'  => array(
            'teeny'            => true,
            'textarea_rows'    => 10
          )
        )    
      )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Subscription Reminder', 'organist' ),
      'id'         => 'payment-renew-section',
      'icon'       => 'el el-envelope',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'    => 'payment_renew_subject',
          'type'  => 'text',
          'title' => __('Subject', 'organist')
        ),

        array(
          'id'    => 'payment_renew_title',
          'type'  => 'text',
          'title' => __('Title', 'organist')
        ),

        array(
          'id'    => 'payment_renew_body',
          'type'  => 'editor',
          'title' => __('Body', 'organist'), 
          'args'  => array(
            'teeny'            => true,
            'textarea_rows'    => 10
          )
        )    
      )
    ));
    
    Redux::setSection( $option_name, array(
      'title'      => __( 'Payment Failed', 'organist' ),
      'id'         => 'payment-failed-section',
      'icon'       => 'el el-envelope',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'    => 'payment_failed_subject',
          'type'  => 'text',
          'title' => __('Subject', 'organist')
        ),

        array(
          'id'    => 'payment_failed_title',
          'type'  => 'text',
          'title' => __('Title', 'organist')
        ),

        array(
          'id'    => 'payment_failed_body',
          'type'  => 'editor',
          'title' => __('Body', 'organist'), 
          'args'  => array(
            'teeny'            => true,
            'textarea_rows'    => 10
          )
        )    
      )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Passive User Notification', 'organist' ),
      'id'         => 'passive-user-notification',
      'icon'       => 'el el-envelope',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'    => 'passive_user_subject',
          'type'  => 'text',
          'title' => __('Subject', 'organist')
        ),

        array(
          'id'    => 'passive_user_title',
          'type'  => 'text',
          'title' => __('Title', 'organist')
        ),

        array(
          'id'    => 'passive_user_body',
          'type'  => 'editor',
          'title' => __('Body', 'organist'), 
          'args'  => array(
            'teeny'            => true,
            'textarea_rows'    => 10
          )
        )    
      )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Settings', 'organist' ),
      'id'         => 'email-settings',
      'icon'       => 'el el-gear',
      'subsection' => true,
      'fields'     => array(
        array(
          'id'      => 'smtp_host',
          'type'    => 'text',
          'title'   => __('Host', 'organist'),
          'default' => 'smtp.mailgun.org',
        ),

        array(
          'id'      => 'smtp_username',
          'type'    => 'text',
          'title'   => __('Username', 'organist'),
          'default' => 'postmaster@sandboxaa6b698772b5423fb581b46ffeefc239.mailgun.org',
        ),

        array(
          'id'      => 'smtp_password',
          'type'    => 'text',
          'title'   => __('Password', 'organist'),
          'default' => 'sandboxaa6b',
        ),

        array(
          'id'      => 'smtp_port',
          'type'    => 'text',
          'title'   => __('Port', 'organist'),
          'default' => '587',
        ),

        array(
          'id'      => 'smtp_from_email',
          'type'    => 'text',
          'title'   => __('From Email', 'organist'),
          'default' => 'mailgun@sandboxaa6b698772b5423fb581b46ffeefc239.mailgun.org',
        ),

        array(
          'id'      => 'smtp_from_name',
          'type'    => 'text',
          'title'   => __('From Name', 'organist'),
          'default' => 'Organists Review',
        ),

        array(
          'id'      => 'email_threshold_days_automatic',
          'type'    => 'text',
          'title'   => __('Threshold period for automatic payment', 'organist'),
          'subtitle'   => __('In days', 'organist'),
          'default' => '30',
          'description' => __('Number of days prior to send reminder to active subscriber for automatic payment', 'organist')
        ),

        array(
          'id'      => 'email_threshold_days_manual',
          'type'    => 'text',
          'title'   => __('Threshold period for manual payment', 'organist'),
          'subtitle'   => __('In days', 'organist'),
          'default' => '30',
          'description' => __('Number of days prior to send payment reminder for manual User to admin', 'organist')
        ),

        array(
          'id'      => 'email_threshold_days_paper',
          'type'    => 'text',
          'title'   => __('Threshold period for paper renew', 'organist'),
          'subtitle'   => __('In days', 'organist'),
          'default' => '60',
          'description' => __('Number of days prior to send payment reminder for paper renew users to admin', 'organist')
        ),

      )
    ));

    Redux::setSection( $option_name, array(
      'title'      => __( 'Latest Issue Section', 'organist' ),
      'id'         => 'issue-section',
      'icon'       => 'el el-dashboard',
      'fields'     => array(

        array(
          'id'    => 'issue_title',
          'type'  => 'text',
          'title' => __( 'Title', 'organist' )
        ),

        array(
          'id'    => 'issue_sub_title',
          'type'  => 'text',
          'title' => __( 'Sub Title', 'organist' )
        ),

        array(
          'id'    => 'issue_description',
          'type'  => 'textarea',
          'title' => __( 'Description', 'organist' ),
          'default' => 'Following on from December’s Wurlitzer extravaganza, this issue is more eclectic. Nils Henrik Asheim opens the magazine with his feature about Ryde & Berg’s Op.100 installed in Stavanger’s concert hall. Remarkable though the idea is to those who read this magazine, concert organs are rare in Norway and Nils has had an uphill battle to make listening to the instrument acceptable to his audiences – the pull-out is an unusual example of how he has fused many cultural worlds; it was taken during the inaugural recital.'
        ),

        array(
          'id'    => 'issue_image',
          'type'  => 'media',
          'title' => __( 'Featured Image', 'organist' )
        )

      )
    ));

    Redux::setSection( $option_name, array(
        'title'      => __( 'Product Section', 'organist' ),
        'id'         => 'product-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                    'id'    => 'product_title',
                    'type'  => 'text',
                    'title' => __( 'Title', 'organist' )
                ),
                array(
                    'id'    => 'product_sub_title',
                    'type'  => 'text',
                    'title' => __( 'Sub Title', 'organist' )
                )
            )
    ));

    Redux::setSection( $option_name, array(
        'title'      => __( 'Social Section', 'organist' ),
        'id'         => 'social-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                    'id'    => 'fb_link',
                    'type'  => 'text',
                    'title' => __( 'Facebook Link', 'organist' )
                ),array(
                    'id'    => 'twit_link',
                    'type'  => 'text',
                    'title' => __( 'Twitter Link', 'organist' )
                ),array(
                    'id'    => 'google_link',
                    'type'  => 'text',
                    'title' => __( 'Google Link', 'organist' )
                ),
            )
    ));

    Redux::setSection( $option_name, array(
        'title'      => __( 'Above Footer Section', 'organist' ),
        'id'         => 'above-footer-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                    'id'    => 'af-title',
                    'type'  => 'text',
                    'title' => __( 'Title', 'organist' )
                ),
                array(
                    'id'    => 'af-desc',
                    'type'  => 'textarea',
                    'title' => __( 'Description ', 'organist' )
                )
            )
    ));

    Redux::setSection( $option_name, array(
        'title'      => __( 'Footer Section', 'organist' ),
        'id'         => 'footer-section',
        'icon'       => 'el el-dashboard',
        'fields'     => array(
                array(
                    'id'    => 'copyright_text',
                    'type'  => 'text',
                    'title' => __( 'Copyright Text', 'organist' )
                )
            )
    ));