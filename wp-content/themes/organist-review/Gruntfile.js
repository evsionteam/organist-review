module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        makepot: {
            target: {
                options: {
                    mainFile: './style.css',
                    type: 'wp-theme',
                    domainPath: './languages/'
                }
            }
        },
        sass: { // Task
            dist: { // Target
                options: { // Target options
                    style: 'expanded'
                },
                files: { // Dictionary of files
                    'assets/src/css/main.css': './assets/src/sass/main.scss', // 'destination': 'source'
                    'style.css': './assets/src/sass/style.scss'
                }
            }
        },
        concat: {
            options: {
                separator: "\n /*** New File ***/ \n"
            },
            /* Added new JS here*/
            bootstrap: {
                src: [
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js'
                ],
                dest: './assets/build/js/bootstrap.js'
            },
            admin: {
                src: [
                    './assets/src/js/admin.js'
                ],
                dest: './assets/build/js/admin.js'
            },
            js: {
                src: [
                    './assets/src/js/wrapper/start.js',
                    './assets/src/js/jquery.mmenu.all.min.js',
                    './assets/src/js/skip-link-focus-fix.js',
                    './assets/src/js/jquery.carouFredSel-6.2.1.js',
                    './assets/src/js/jquery.touchSwipe.min.js',
                    './assets/src/js/jquery.animateNumber.js',
                    './assets/src/js/jquery.nicescroll.min.js',
                    './assets/build/js/bootstrap.js',
                    './node_modules/isotope-layout/dist/isotope.pkgd.min.js',
                    './node_modules/tablesaw/dist/tablesaw.jquery.js',
                    './node_modules/tablesaw/dist/tablesaw-init.js',
                    './node_modules/waypoints/lib/jquery.waypoints.js',
                    './node_modules/jquery.mmenu/dist/js/jquery.mmenu.all.min.js',
                    './node_modules/sweetalert/dist/sweetalert.min.js',
                    './assets/src/js/subscription.js',
                    './assets/src/js/card.js',
                    './assets/src/js/lightslider.js',
                    './assets/src/js/script.js',
                    './assets/src/js/wrapper/end.js'
                ],
                dest: './assets/build/js/script.js'
            },
            /* Add CSS here*/
            css: {
                src: [
                    './node_modules/font-awesome/css/font-awesome.css',
                    './node_modules/jquery.mmenu/dist/css/jquery.mmenu.all.css',
                    './node_modules/tablesaw/dist/tablesaw.css',
                    './node_modules/sweetalert/dist/sweetalert.css',
                    './assets/src/css/*.css'
                ],
                dest: './assets/build/css/main.css'
            },
            404: {
                src: [
                    './assets/src/js/wrapper/start.js',
                    './assets/src/js/TweenMax.min.js',
                    './assets/src/js/404.js',
                    './assets/src/js/wrapper/end.js'
                ],
                dest: './assets/build/js/404/404.js'
            }
        },
        uglify: {
            options: {
                report: 'gzip'
            },
            main: {
                src: ['./assets/build/js/script.js'],
                dest: './assets/build/js/script.min.js'
            },
            404: {
                src: ['./assets/build/js/404/404.js'],
                dest: './assets/build/js/404/404.min.js'
            },
            admin:{
                src: ['./assets/build/js/admin.js'],
                dest: './assets/build/js/admin.min.js'
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './assets/build/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: './assets/build/css/',
                    ext: '.min.css'
                }]
            }
        },

        copy: {
            main: {

                files: [{
                        expand: true,
                        cwd: './assets/src/img',
                        src: '**',
                        dest: './assets/build/img/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './node_modules/bootstrap/dist/fonts',
                        src: '**',
                        dest: './assets/build/fonts/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: './node_modules/font-awesome/fonts',
                        src: '**',
                        dest: './assets/build/fonts/',
                        filter: 'isFile'
                    },
                ],
            },
        },

        watch: {
            admin:{
                files: ['./assets/src/js/admin.js'],
                tasks: ['admin']
            },
            js: {
                files: ['./assets/src/js/*.js'],
                tasks: ['js']
            },
            css: {
                files: ['./assets/src/sass/**/*.scss'],
                tasks: ['css']
            },
            php: {
                files: ['*.php', '**/*.php'],
                task: ['makepot']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-wp-i18n');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');

    //register grunt default task
    grunt.registerTask('css', ['sass', 'concat', 'cssmin']);
    grunt.registerTask('js', ['concat:js', 'uglify']);
    grunt.registerTask('admin', ['concat:admin', 'uglify:admin']);
    grunt.registerTask('makepot', ['makepot']);

    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'cssmin', 'copy']);
}
